#ifndef __HEADER_CVMTHOOKMANAGER__
#define __HEADER_CVMTHOOKMANAGER__
#pragma once

namespace Hooks
{
	template< size_t nSize >
	class CVMTHookManager;
}

template< size_t nSize > // avoid any allocations
class Hooks::CVMTHookManager 
{
public:
	CVMTHookManager() : m_pppBase(nullptr), m_ppOldVMT(nullptr), m_bHooked(false)
	{
	}

	~CVMTHookManager()
	{
		if (m_bHooked)
		{
			Unhook();
		}
	}

	void Init(void* pBase)
	{
		m_bHooked = false;

		m_pppBase = (void***)pBase;
		m_ppOldVMT = *m_pppBase;

		for (uint32_t i = 0; i < nSize; i++)
		{
			m_NewVMT[i] = m_ppOldVMT[i];
		}
	}

	void HookFunction(uint32_t u32Index, void* pFunction)
	{
		m_NewVMT[u32Index] = pFunction;
	}

	void UnhookFunction(uint32_t u32Index)
	{
		m_NewVMT[u32Index] = m_ppOldVMT[u32Index];
	}

	void Hook()
	{
		m_bHooked = true;

		*m_pppBase = m_NewVMT;
	}

	void Rehook()
	{
		m_bHooked = true;

		*m_pppBase = m_NewVMT;
	}

	void Unhook()
	{
		m_bHooked = false;

		*m_pppBase = m_ppOldVMT;
	}
private:
	void*** m_pppBase;
	void**  m_ppOldVMT;
	void*   m_NewVMT[nSize];
	bool    m_bHooked;
};

#endif