#ifndef __HEADER_HOOKS__
#define __HEADER_HOOKS__
#pragma once

#include "CVMTHookManager.h"

#include "Game/CreateMove.h"
#include "Game/FrameStageNotify.h"
#include "Game/Paint.h"
#include "Game/UpdateButtonState.h"

namespace Hooks
{
	void Initialize();
}

#endif