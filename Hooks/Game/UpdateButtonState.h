#ifndef __HEADER_UPDATEBUTTONSTATE__
#define __HEADER_UPDATEBUTTONSTATE__
#pragma once

#include "Hooks/CVMTHookManager.h"

namespace Hooks
{
	namespace Game
	{
		namespace EngineVGui
		{
			extern CVMTHookManager<47> HookMgr;

			void __fastcall UpdateButtonState(SDK::IEngineVGui* pThis, void*, SDK::InputEvent_t& event);
		}
	}
}

#endif