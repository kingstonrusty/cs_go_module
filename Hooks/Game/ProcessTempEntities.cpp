#include "ProcessTempEntities.h"

namespace Hooks
{
	namespace Game
	{
		namespace SVC_TempEntities
		{
			CVMTHookManager<5> HookMgr;
		}
	}
}

static bool s_bHooked;

void Hooks::Game::SVC_TempEntities::InstallHook()
{
	if (s_bHooked)
	{
		return;
	}

	uint32_t net = (uint32_t)SDK::Interfaces::Engine->GetNetChannelInfo();
	uint32_t v25 = 27/*svc_TempEntities*/;
	uint32_t v26 = *(uint32_t*)(net + 17080);
	if ((v25 >= *(uint32_t*)(v26 + 17044)))
	{
		return;
	}

	uint32_t v27 = *(uint32_t*)(v26 + 17032);
	uint32_t v28 = 5 * v25;
	uint32_t netmsg = **(uint32_t**)(v27 + 4 * v28);
	if (!netmsg)
	{
		return;
	}

	HookMgr.Init((void*)netmsg);
	HookMgr.HookFunction(4, ProcessTempEntities);
	HookMgr.Hook();

	s_bHooked = true;
}

void Hooks::Game::SVC_TempEntities::UninstallHook()
{
	s_bHooked = false;
}

bool __fastcall Hooks::Game::SVC_TempEntities::ProcessTempEntities(SDK::CSVC_TempEntities* pThis, void*, void* msg)
{
	HookMgr.Unhook();
	bool bRet = pThis->Process(msg);
	HookMgr.Rehook();

	return bRet;
}