#ifndef __HEADER_CREATEMOVE__
#define __HEADER_CREATEMOVE__
#pragma once

#include "Hooks/CVMTHookManager.h"

namespace Hooks
{
	namespace Game
	{
		namespace ClientMode
		{
			extern CVMTHookManager<62> HookMgr;

			bool __fastcall CreateMove(SDK::IClientMode* pThis, void*, float sample_time, SDK::CUserCmd* cmd);
		}
	}
}

#endif