#ifndef __HEADER_PROCESS_TEMP_ENTITIES__
#define __HEADER_PROCESS_TEMP_ENTITIES__
#pragma once

#include "Hooks/CVMTHookManager.h"

namespace Hooks
{
	namespace Game
	{
		namespace SVC_TempEntities
		{
			extern CVMTHookManager<5> HookMgr;
			void InstallHook();
			void UninstallHook();

			bool __fastcall ProcessTempEntities(SDK::CSVC_TempEntities* pThis, void*, void* msg);
		}
	}
}

#endif