#include "Paint.h"

#include "Menu/Menu.h"
#include "Hooks/Game/ProcessTempEntities.h"
#include "Util/DrawingHelper.h"

void __fastcall Hooks::Game::EngineVGui::Paint(SDK::IEngineVGui* pThis, void*, SDK::PaintMode_t mode)
{
	HookMgr.Unhook();
	pThis->Paint(mode);
	HookMgr.Rehook();

	if (!(mode & SDK::PAINT_UIPANELS))
	{
		return;
	}

	SDK::Interfaces::Surface->StartDrawing();

	Menu::Draw();

	SDK::Interfaces::Surface->FinishDrawing();

	Util::Input::Update();

	if (SDK::Interfaces::Engine->IsInGame() && SDK::Interfaces::Engine->GetNetChannelInfo())
	{
		Hooks::Game::SVC_TempEntities::InstallHook();
	}
	else
	{
		Hooks::Game::SVC_TempEntities::UninstallHook();
	}
}