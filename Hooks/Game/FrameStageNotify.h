#ifndef __HEADER_FRAMESTAGENOTIFY__
#define __HEADER_FRAMESTAGENOTIFY__
#pragma once

#include "Hooks/CVMTHookManager.h"

namespace Hooks
{
	namespace Game
	{
		namespace Client
		{
			extern CVMTHookManager<124> HookMgr;

			void __fastcall FrameStageNotify(SDK::IBaseClientDLL* pThis, void*, SDK::ClientFrameStage_t stage);
		}
	}
}

#endif