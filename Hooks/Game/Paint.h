#ifndef __HEADER_PAINT__
#define __HEADER_PAINT__
#pragma once

#include "Hooks/CVMTHookManager.h"

namespace Hooks
{
	namespace Game
	{
		namespace EngineVGui
		{
			extern CVMTHookManager<47> HookMgr;

			void __fastcall Paint(SDK::IEngineVGui* pThis, void*, SDK::PaintMode_t mode);
		}
	}
}

#endif