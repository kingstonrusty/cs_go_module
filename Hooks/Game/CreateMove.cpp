#include "CreateMove.h"

#include "Features/Position Adjustment/History.h"
#include "Features/Position Adjustment/Prediction.h"
#include "Features/Ragebot/Autowall.h"

bool __fastcall Hooks::Game::ClientMode::CreateMove(SDK::IClientMode* pThis, void*, float sample_time, SDK::CUserCmd* cmd)
{
	if (cmd->command_number() == 0)
	{
		return true;
	}

	SDK::C_CSPlayer* pLocal = (SDK::C_CSPlayer*)SDK::Interfaces::EntityList->GetClientEntity(SDK::Interfaces::Engine->GetLocalPlayer());
	if (pLocal == nullptr)
	{
		return true;
	}

	if (!pLocal->IsAlive())
	{
		Features::PositionAdjustment::History::UpdateObsoleteSnapshots();

		return true;
	}

	/*Features::PositionAdjustment::History::UpdateObsoleteSnapshots();
	Features::Ragebot::Autowall::FixBreakables();

	Features::PositionAdjustment::Prediction::Run(pLocal, cmd);



	Features::PositionAdjustment::Prediction::Finish(pLocal, cmd);

	Features::Ragebot::Autowall::RestoreBreakable();*/

	SDK::C_WeaponCSBase* pWeapon = (SDK::C_WeaponCSBase*)SDK::Interfaces::EntityList->GetClientEntityFromHandle(pLocal->GetActiveWeapon());
	if (pWeapon)
	{
		typedef void(__thiscall* GetRecoilOffset_t)(SDK::C_WeaponCSBase*, int mode, int seed, float* y, float* x);
		static GetRecoilOffset_t pGetRecoilOffset = (GetRecoilOffset_t)Util::Memory::FindPattern(L"client.dll", "55 8B EC 83 EC 10 8B 4D 08 81 C1");

		SDK::Msg("RecoilTable = { ");

		for (int mode = 0; mode < 2; mode++)
		{
			for (int i = 0; i < 64; i++)
			{
				float x, y;

				__asm
				{
					lea ecx, x
					push ecx
					lea ecx, y
					push ecx
					mov eax, i
					push eax
					push mode
					push pWeapon
					call pGetRecoilOffset
				}

				SDK::Msg("%f, ", y);
			}
		}

		SDK::Msg("}\n");
	}

	SDK::Msg("%d\n", 1 << 6);

	return false;
}