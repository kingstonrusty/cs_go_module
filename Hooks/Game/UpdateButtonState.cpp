#include "UpdateButtonState.h"

#include "Menu/Menu.h"

void __fastcall Hooks::Game::EngineVGui::UpdateButtonState(SDK::IEngineVGui* pThis, void*, SDK::InputEvent_t& event)
{
	if (event.m_nType == SDK::IE_ButtonPressed && event.m_nData == SDK::KEY_INSERT)
	{
		Menu::SetShouldDraw(!Menu::GetShouldDraw());
	}

	if (Menu::GetShouldDraw() && (event.m_nType == SDK::IE_ButtonDoubleClicked || event.m_nType == SDK::IE_ButtonPressed || event.m_nType == SDK::IE_ButtonReleased))
	{
		Util::Input::SetKeyState((SDK::ButtonCode_t)event.m_nData, (event.m_nType == SDK::IE_ButtonReleased) ? 0x0000 : 0x0001);

		event.m_nData = SDK::KEY_APP; // garbage key
	}

	HookMgr.Unhook();
	pThis->UpdateButtonState(event);
	HookMgr.Rehook();
}