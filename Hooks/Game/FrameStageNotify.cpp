#include "FrameStageNotify.h"

#include "Features/Position Adjustment/History.h"
#include "Features/Hack vs Hack/Resolver.h"

void __fastcall Hooks::Game::Client::FrameStageNotify(SDK::IBaseClientDLL* pThis, void*, SDK::ClientFrameStage_t stage)
{
	if (stage == SDK::FRAME_RENDER_START)
	{
		for (int i = 1; i <= SDK::Interfaces::Globals->maxClients(); i++)
		{
			if (i == SDK::Interfaces::Engine->GetLocalPlayer())
			{
				continue;
			}

			SDK::C_CSPlayer* pPlayer = (SDK::C_CSPlayer*)SDK::Interfaces::EntityList->GetClientEntity(i);
			if (pPlayer == nullptr)
			{
				continue;
			}

			pPlayer->RemoveFromInterpolationList();
		}
	}

	HookMgr.Unhook();
	pThis->FrameStageNotify(stage);
	HookMgr.Rehook();

	if (stage == SDK::FRAME_NET_UPDATE_POSTDATAUPDATE_START)
	{
		for (int i = 1; i <= SDK::Interfaces::Globals->maxClients(); i++)
		{
			if (i == SDK::Interfaces::Engine->GetLocalPlayer())
			{
				continue;
			}

			SDK::C_CSPlayer* pPlayer = (SDK::C_CSPlayer*)SDK::Interfaces::EntityList->GetClientEntity(i);
			if (pPlayer == nullptr)
			{
				continue;
			}

			*(SDK::Vector*)((uint32_t)pPlayer->GetAnimState() + 68) = SDK::Vector(0.f, SDK::RandomInt(0, 360), 0.f);
			pPlayer->InvalidateBoneCache();
		}
	}

	if (stage == SDK::FRAME_NET_UPDATE_POSTDATAUPDATE_START)
	{
		Features::PositionAdjustment::History::Record();
		Features::HackvsHack::Resolver::Run();
	}
}