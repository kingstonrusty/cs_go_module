#include "Hooks.h"

namespace Hooks
{
	namespace Game
	{
		namespace Client
		{
			CVMTHookManager<124> HookMgr;
		}

		namespace ClientMode
		{
			CVMTHookManager<62> HookMgr;
		}

		namespace EngineVGui
		{
			CVMTHookManager<47> HookMgr;
		}
	}
}

void Hooks::Initialize()
{
	using namespace Game;

	Client::HookMgr.Init(SDK::Interfaces::Client);
	Client::HookMgr.HookFunction(36, Client::FrameStageNotify);
	Client::HookMgr.Hook();

	ClientMode::HookMgr.Init(SDK::Interfaces::ClientMode);
	ClientMode::HookMgr.HookFunction(24, ClientMode::CreateMove);
	ClientMode::HookMgr.Hook();

	EngineVGui::HookMgr.Init(SDK::Interfaces::EngineVGui);
	EngineVGui::HookMgr.HookFunction(12, EngineVGui::UpdateButtonState);
	EngineVGui::HookMgr.HookFunction(14, EngineVGui::Paint);
	EngineVGui::HookMgr.Hook();
}