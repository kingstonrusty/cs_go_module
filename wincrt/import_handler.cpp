#if !defined(_DEBUG)

#include "import_handler.h"

#include "Wrappers/String.h"
#include "Util/CRC32.h"

HMODULE GetModuleHandle(uint32_t ui32ModuleCRC)
{
	PLIST_ENTRY pEntry = GetPEB()->Ldr->InMemoryOrderModuleList.Flink;
	while (pEntry)
	{
		PLDR_DATA_TABLE_ENTRY pData = (PLDR_DATA_TABLE_ENTRY)pEntry;
		if (pData->FullDllName.Buffer == nullptr || pData->FullDllName.Length == 0)
		{
			break;
		}

		if (Util::CRC32::HashSingleBuffer(pData->FullDllName.Buffer, pData->FullDllName.Length) == ui32ModuleCRC)
		{
			return *(HMODULE*)((uint32_t)pData + 0x10);
		}

		pEntry = pEntry->Flink;
	}

	return nullptr;
}

void* GetExportedFunctionFromModule(uint32_t ui32ModuleCRC, uint32_t ui32FunctionCRC)
{
	HMODULE hModule = GetModuleHandle(ui32ModuleCRC);
	if (hModule == nullptr)
	{
		return nullptr;
	}

	PIMAGE_DOS_HEADER pDOSHeader = (PIMAGE_DOS_HEADER)hModule;
	if (pDOSHeader->e_magic != IMAGE_DOS_SIGNATURE)
	{
		return nullptr;
	}

	PIMAGE_NT_HEADERS32 pNTHeaders = (PIMAGE_NT_HEADERS32)((uint32_t)pDOSHeader + pDOSHeader->e_lfanew);
	if (pNTHeaders->Signature != IMAGE_NT_SIGNATURE)
	{
		return nullptr;
	}

	PIMAGE_OPTIONAL_HEADER32 pOptionalHeader = &pNTHeaders->OptionalHeader;
	if (pOptionalHeader->Magic != IMAGE_NT_OPTIONAL_HDR32_MAGIC)
	{
		return nullptr;
	}

	PIMAGE_EXPORT_DIRECTORY pExportDirectory = (PIMAGE_EXPORT_DIRECTORY)((uint32_t)pDOSHeader + pOptionalHeader->DataDirectory[0].VirtualAddress);
	uint32_t* pAddressOfNames = (uint32_t*)((uint32_t)pDOSHeader + pExportDirectory->AddressOfNames);
	uint16_t* pNameOrdinals = (uint16_t*)((uint32_t)pDOSHeader + pExportDirectory->AddressOfNameOrdinals);
	uint32_t* pAddresses = (uint32_t*)((uint32_t)pDOSHeader + pExportDirectory->AddressOfFunctions);

	for (uint32_t i = 0; i < pExportDirectory->NumberOfNames; i++)
	{
		const char* pszFunctionName = (const char*)((uint32_t)pDOSHeader + pAddressOfNames[i]);

		if (Util::CRC32::HashSingleBuffer(pszFunctionName, StringLength(pszFunctionName)) == ui32FunctionCRC)
		{
			return (void*)((uint32_t)pDOSHeader + pAddresses[pNameOrdinals[i]]);
		}
	}

	return nullptr;
}

#endif