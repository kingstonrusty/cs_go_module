#if !defined(_DEBUG)

#include "kernel32.h"

typedef int(__stdcall* MultiByteToWideChar_t)(unsigned int, DWORD, const char*, int, wchar_t*, int);
static MultiByteToWideChar_t s_pMultiByteToWideChar;

int MultiByteToWideChar(unsigned int CodePage, DWORD dwFlags, const char* lpMultiByteString, int cbMultiByteStr, wchar_t* lpWideCharStr, int cchWideChar)
{
	if (!s_pMultiByteToWideChar)
	{
		s_pMultiByteToWideChar = (MultiByteToWideChar_t)GetExportedFunctionFromModule(KERNEL32_CRC32, 0x72F11E39/*MultiByteToWideChar*/);
	}

	return s_pMultiByteToWideChar(CodePage, dwFlags, lpMultiByteString, cbMultiByteStr, lpWideCharStr, cchWideChar);
}

#endif