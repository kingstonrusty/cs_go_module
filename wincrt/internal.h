#ifndef __HEADER_INTERNAL__
#define __HEADER_INTERNAL__
#pragma once

inline PPEB GetPEB()
{
	PPEB pPEB;

	__asm
	{
		push eax

			mov eax, dword ptr fs : [00000018h]
			mov eax, dword ptr[eax + 30h]
			mov pPEB, eax

		pop eax
	}

	return pPEB;
}

#endif