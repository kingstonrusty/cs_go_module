#if !defined(_DEBUG)

#ifndef __HEADER_NTAPI__
#define __HEADER_NTAPI__
#pragma once

#define NTAPI __stdcall

PVOID RtlCreateHeap(ULONG Flags, PVOID HeapBase, SIZE_T ReserveSize, SIZE_T CommitSize, PVOID Lock, PRTL_HEAP_PARAMETERS Parameters);
PVOID RtlAllocateHeap(PVOID HeapHandle, ULONG Flags, SIZE_T Size);
PVOID RtlReAllocateHeap(PVOID HeapHandle, ULONG Flags, PVOID MemoryPointer, SIZE_T Size);
LOGICAL RtlFreeHeap(PVOID HeapHandle, ULONG Flags, PVOID HeapBase);

int _vsnprintf(char* const _Buffer, size_t const _BufferCount, char const* const _Format, va_list _ArgList);
int vswprintf_s(wchar_t* buffer, size_t numberOfElements, const wchar_t* format, va_list argptr);

#endif

#endif