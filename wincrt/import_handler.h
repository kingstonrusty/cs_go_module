#ifndef __HEADER_IMPORT_HANDLER__
#define __HEADER_IMPORT_HANDLER__
#pragma once

#define NTDLL_CRC32 0x26797E77
#define KERNEL32_CRC32 0xF7784A01
#define USER32_CRC32 0xD51B72AE
#define UCRTBASE_CRC32 0x5CEE33A7

HMODULE GetModuleHandle(uint32_t ui32ModuleCRC);
void* GetExportedFunctionFromModule(uint32_t ui32ModuleCRC, uint32_t ui32FunctionCRC);

#endif