#ifndef __HEADER_STRING__
#define __HEADER_STRING__
#pragma once

#if !defined(_DEBUG)

extern "C" void* memset(void* pBlock, int iValue, size_t nCount);
#pragma intrinsic(memset)

#endif

#endif