#if !defined(_DEBUG)

#include "ntapi.h"

typedef PVOID(NTAPI* RtlCreateHeap_t)(ULONG, PVOID, SIZE_T, SIZE_T, PVOID, PRTL_HEAP_PARAMETERS);
static RtlCreateHeap_t s_pRtlCreateHeap;

PVOID RtlCreateHeap(ULONG Flags, PVOID HeapBase, SIZE_T ReserveSize, SIZE_T CommitSize, PVOID Lock, PRTL_HEAP_PARAMETERS Parameters)
{
	if (!s_pRtlCreateHeap)
	{
		s_pRtlCreateHeap = (RtlCreateHeap_t)GetExportedFunctionFromModule(NTDLL_CRC32, 0xF80C219B/*RtlCreateHeap*/);
	}

	return s_pRtlCreateHeap(Flags, HeapBase, ReserveSize, CommitSize, Lock, Parameters);
}

typedef PVOID(NTAPI* RtlAllocateHeap_t)(PVOID, ULONG, SIZE_T);
static RtlAllocateHeap_t s_pRtlAllocateHeap;

PVOID RtlAllocateHeap(PVOID HeapHandle, ULONG Flags, SIZE_T Size)
{
	if (!s_pRtlAllocateHeap)
	{
		s_pRtlAllocateHeap = (RtlAllocateHeap_t)GetExportedFunctionFromModule(NTDLL_CRC32, 0xA1D45974/*RtlAllocateHeap*/);
	}

	return s_pRtlAllocateHeap(HeapHandle, Flags, Size);
}

typedef PVOID(NTAPI* RtlReAllocateHeap_t)(PVOID, ULONG, PVOID, SIZE_T);
static RtlReAllocateHeap_t s_pRtlReAllocateHeap;

PVOID RtlReAllocateHeap(PVOID HeapHandle, ULONG Flags, PVOID MemoryPointer, SIZE_T Size)
{
	if (!s_pRtlReAllocateHeap)
	{
		s_pRtlReAllocateHeap = (RtlReAllocateHeap_t)GetExportedFunctionFromModule(NTDLL_CRC32, 0xB973B8DC/*RtlReAllocateHeap*/);
	}

	return s_pRtlReAllocateHeap(HeapHandle, Flags, MemoryPointer, Size);
}

typedef LOGICAL(NTAPI* RtlFreeHeap_t)(PVOID, ULONG, PVOID);
static RtlFreeHeap_t s_pRtlFreeHeap;

LOGICAL RtlFreeHeap(PVOID HeapHandle, ULONG Flags, PVOID HeapBase)
{
	if (!s_pRtlFreeHeap)
	{
		s_pRtlFreeHeap = (RtlFreeHeap_t)GetExportedFunctionFromModule(NTDLL_CRC32, 0xAF11BC24/*RtlFreeHeap*/);
	}

	return s_pRtlFreeHeap(HeapHandle, Flags, HeapBase);
}

typedef int(__cdecl* _vsnprintf_t)(char* const, size_t const, char const* const, va_list);
static _vsnprintf_t s_p_vsnprintf;

int _vsnprintf(char* const _Buffer, size_t const _BufferCount, char const* const _Format, va_list _ArgList)
{
	if (!s_p_vsnprintf)
	{
		s_p_vsnprintf = (_vsnprintf_t)GetExportedFunctionFromModule(NTDLL_CRC32, 0xB2AF3675/*_vsnprintf*/);
	}

	return s_p_vsnprintf(_Buffer, _BufferCount, _Format, _ArgList);
}

typedef int(__cdecl* vswprintf_s_t)(wchar_t*, size_t, const wchar_t*, va_list);
static vswprintf_s_t s_pvswprintf_s;

int vswprintf_s(wchar_t* buffer, size_t numberOfElements, const wchar_t* format, va_list argptr)
{
	if (!s_pvswprintf_s)
	{
		s_pvswprintf_s = (vswprintf_s_t)GetExportedFunctionFromModule(NTDLL_CRC32, 0x76DA9FB3/*vswprintf_s*/);
	}

	return s_pvswprintf_s(buffer, numberOfElements, format, argptr);
}

#endif