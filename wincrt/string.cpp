#include "string.h"

#if !defined(_DEBUG)

#pragma function(memset)
extern "C" void* memset(void* pBlock, int iValue, size_t nCount)
{
	SetMemory(pBlock, iValue, nCount);

	return pBlock;
}

#endif