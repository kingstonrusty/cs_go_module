#if !defined(_DEBUG)

#include "math.h"

typedef double(__cdecl* pow_t)(double, double);
static pow_t s_ppow;

double pow(double _X, double _Y)
{
	if (!s_ppow)
	{
		s_ppow = (pow_t)GetExportedFunctionFromModule(UCRTBASE_CRC32, 0x87B422B5/*pow*/);
	}

	return s_ppow(_X, _Y);
}

#endif