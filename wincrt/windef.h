#ifndef __HEADER_WINDEF__
#define __HEADER_WINDEF__
#pragma once

/*number types*/
typedef int32_t INT;
typedef INT BOOL;

typedef uint32_t UINT, ULONG;
typedef ULONG DWORD;

#define SIZE_T size_t

/*other types(handles, etc)*/
typedef void VOID;
typedef VOID *PVOID, *LPVOID;

typedef void* HANDLE;
typedef HANDLE HINSTANCE, HMODULE;

#define WINAPI __stdcall

/*enums etc*/
#define TRUE 1
#define FALSE 0

#define DLL_PROCESS_ATTACH   1    
#define DLL_THREAD_ATTACH    2    
#define DLL_THREAD_DETACH    3    
#define DLL_PROCESS_DETACH   0  

/*structs etc*/

typedef struct _LSA_UNICODE_STRING 
{
	uint16_t Length;
	uint16_t MaximumLength;
	wchar_t* Buffer;
} LSA_UNICODE_STRING, *PLSA_UNICODE_STRING, UNICODE_STRING, *PUNICODE_STRING;

typedef struct _LIST_ENTRY 
{
	struct _LIST_ENTRY *Flink;
	struct _LIST_ENTRY *Blink;
} LIST_ENTRY, *PLIST_ENTRY;

#endif