#if !defined(_DEBUG)

#ifndef __HEADER_KERNEL32__
#define __HEADER_KERNEL32__
#pragma once

int MultiByteToWideChar(unsigned int CodePage, DWORD dwFlags, const char* lpMultiByteString, int cbMultiByteStr, wchar_t* lpWideCharStr, int cchWideChar);

#endif

#endif
