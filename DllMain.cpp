#include "DllMain.h"

#include "Hooks/Hooks.h"
#include "Util/DrawingHelper.h"

BOOL WINAPI DllMain(HINSTANCE hinstDll, DWORD fdwReason, LPVOID lpvReserved)
{
	if (fdwReason == DLL_PROCESS_ATTACH)
	{
#if !defined(_DEBUG)
		//InitializeHeap();
#endif

		SDK::Initialize();
		Util::DrawingHelper::Fonts::Initialize();
		Hooks::Initialize();

		SDK::Msg(XorStr("[%s] Build %s\n"), NAME, XorStr(__DATE__ " " __TIME__));
	}

	return TRUE;
}

#if !defined(_DEBUG)
extern "C" 
{
	int _fltused;
}

int atexit(void(__cdecl *func)(void)) //ghettofix
{
	return 0;
}

int __cdecl _purecall()
{
	return 0;
}
#endif