#include "Input.h"

static short s_ButtonStates[SDK::BUTTON_COUNT + 1]; // button 0 = nothing

void Util::Input::Update()
{
	for (uint32_t i = 1; i <= SDK::BUTTON_COUNT; i++)
	{
		if (i == SDK::MOUSE_WHEEL_DOWN || i == SDK::MOUSE_WHEEL_UP)
		{
			s_ButtonStates[i] = 0x0000i16;
		}
		else if (s_ButtonStates[i] == 0x0001i16)
		{
			s_ButtonStates[i] = 0x8000i16;
		}
	}
}

short Util::Input::GetKeyState(SDK::ButtonCode_t button)
{
	if (button <= 0 || button > SDK::BUTTON_COUNT)
	{
		return 0;
	}

	return s_ButtonStates[button];
}

void Util::Input::SetKeyState(SDK::ButtonCode_t button, short state)
{
	if (button <= 0 || button > SDK::BUTTON_COUNT)
	{
		return;
	}

	if (state == 0 && (button == SDK::MOUSE_WHEEL_DOWN || button == SDK::MOUSE_WHEEL_UP))
	{
		return;
	}

	s_ButtonStates[button] = state;
}