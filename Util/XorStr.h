#ifndef __HEADER_XORSTR__
#define __HEADER_XORSTR__
#pragma once

#if !defined(_DEBUG)
#define XOR_ENABLED
namespace Util 
{
	namespace XOR 
	{
#define SEED ((__TIME__[7] - '0') * 461  + (__TIME__[6] - '0') * 75  + \
              (__TIME__[4] - '0') * 276   + (__TIME__[3] - '0') * 99129 + \
              (__TIME__[1] - '0') * 1312 + (__TIME__[0] - '0') * 3718)

		constexpr __forceinline int LinearCongruentGenerator(int nRounds) 
		{
			return 1013904223 + 1664525 * ((nRounds> 0) ? LinearCongruentGenerator(nRounds - 1) : SEED & 0xFFFFFFFF);
		}
#undef SEED

		template<size_t nLen, int nCounter>
		class CXorStr 
		{
		public:
			constexpr __forceinline CXorStr(const char* pszString) : m_bDecoded(false), m_uiKey(LinearCongruentGenerator(nCounter)), m_szEncrypted{ 0 } 
			{
				EncodeString<nLen>(pszString);
			}

			template<size_t nCur>
			constexpr __forceinline void EncodeString(const char* pszString) const
			{
				m_szEncrypted[nCur] = Encode<nCur>(pszString[nCur]);
				EncodeString<nCur - 1>(pszString);
			}

			template<>
			constexpr __forceinline void EncodeString<size_t(-1)>(const char* pszString) const
			{

			}

			__forceinline const char* GetString()
			{
				if (m_bDecoded) return m_szEncrypted;

				for (uint32_t i = 0; i < nLen; i++)
					m_szEncrypted[i] ^= ((m_uiKey >> (8 * (i % 4))) & 0xFF);

				m_szEncrypted[nLen] = '\0';
				m_bDecoded = true;
				return m_szEncrypted;
			}
		private:
			template<size_t nCur>
			__forceinline constexpr char Encode(char c) const
			{
				return c ^ ((m_uiKey >> (8 * (nCur % 4))) & 0xFF);
			}
			const uint32_t m_uiKey;
			mutable char m_szEncrypted[nLen + 1];
			bool m_bDecoded;
		};

		template<size_t nLen, int nCounter>
		class CWXorStr
		{
		public:
			constexpr __forceinline CWXorStr(const wchar_t* pszString) : m_bDecoded(false), m_uiKey(LinearCongruentGenerator(nCounter)), m_szEncrypted{ 0 }
			{
				EncodeString<nLen>(pszString);
			}

			template<size_t nCur>
			constexpr __forceinline void EncodeString(const wchar_t* pszString) const
			{
				m_szEncrypted[nCur] = Encode<nCur>(pszString[nCur]);
				EncodeString<nCur - 1>(pszString);
			}

			template<>
			constexpr __forceinline void EncodeString<size_t(-1)>(const wchar_t* pszString) const
			{

			}

			__forceinline const wchar_t* GetString()
			{
				if (m_bDecoded) return m_szEncrypted;

				for (uint32_t i = 0; i < nLen; i++)
					m_szEncrypted[i] ^= ((m_uiKey >> (8 * (i % 4))) & 0xFF);

				m_szEncrypted[nLen] = '\0';
				m_bDecoded = true;
				return m_szEncrypted;
			}
		private:
			template<size_t nCur>
			__forceinline constexpr wchar_t Encode(wchar_t c) const
			{
				return c ^ ((m_uiKey >> (8 * (nCur % 4))) & 0xFF);
			}
			const uint32_t m_uiKey;
			mutable wchar_t m_szEncrypted[nLen + 1];
			bool m_bDecoded;
		};
	}
}

#define XorStr_Expand(s) (Util::XOR::CXorStr<sizeof(s) - 1, __COUNTER__>(s).GetString())
#define XorStr(s) XorStr_Expand(s)

#define WXorStr_Expand(s) (Util::XOR::CWXorStr<sizeof(s) / sizeof(wchar_t) - 1, __COUNTER__>(s).GetString())
#define WXorStr(s) WXorStr_Expand(s)

#else

#define XorStr(s) s
#define WXorStr(s) s

#endif

#endif