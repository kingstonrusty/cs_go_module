#ifndef __HEADER_INPUT__
#define __HEADER_INPUT__
#pragma once

namespace Util
{
	namespace Input
	{
		void Update();
		short GetKeyState(SDK::ButtonCode_t button);
		void SetKeyState(SDK::ButtonCode_t button, short state);
	}
}

#endif