#include "DrawingHelper.h"

namespace Util
{
	namespace DrawingHelper
	{
		namespace Fonts
		{
			SDK::HFont SmallFonts12SemiBold;
			SDK::HFont SmallFonts14SemiBold;
			SDK::HFont SmallFonts12Medium;
		}
	}
}

void Util::DrawingHelper::Fonts::Initialize()
{
	SmallFonts12SemiBold = SDK::Interfaces::Surface->CreateFont();
	SDK::Interfaces::Surface->SetFontGlyphSet(SmallFonts12SemiBold, XorStr("Small Fonts"), 12, 600, 0, 0, SDK::FONTFLAG_DROPSHADOW);

	SmallFonts14SemiBold = SDK::Interfaces::Surface->CreateFont();
	SDK::Interfaces::Surface->SetFontGlyphSet(SmallFonts14SemiBold, XorStr("Small Fonts"), 14, 600, 0, 0, SDK::FONTFLAG_DROPSHADOW);

	SmallFonts12Medium = SDK::Interfaces::Surface->CreateFont();
	SDK::Interfaces::Surface->SetFontGlyphSet(SmallFonts12Medium, XorStr("Small Fonts"), 12, 500, 0, 0, SDK::FONTFLAG_DROPSHADOW);
}

void Util::DrawingHelper::DrawFilledRect(int x, int y, int w, int h, SDK::Color clr)
{
	SDK::Interfaces::Surface->DrawSetColor(clr);
	SDK::Interfaces::Surface->DrawFilledRect(x, y, x + w, y + h);
}

void Util::DrawingHelper::DrawOutlinedRect(int x, int y, int w, int h, SDK::Color clr)
{
	SDK::Interfaces::Surface->DrawSetColor(clr);
	SDK::Interfaces::Surface->DrawOutlinedRect(x, y, x + w, y + h);
}

void Util::DrawingHelper::DrawLine(int x1, int y1, int x2, int y2, SDK::Color clr)
{
	SDK::Interfaces::Surface->DrawSetColor(clr);
	SDK::Interfaces::Surface->DrawLine(x1, y1, x2, y2);
}

void Util::DrawingHelper::DrawText(SDK::HFont font, int x, int y, const wchar_t* pwszText, SDK::Color clr)
{
	SDK::Interfaces::Surface->DrawSetTextColor(clr);
	SDK::Interfaces::Surface->DrawSetTextFont(font);
	SDK::Interfaces::Surface->DrawSetTextPos(x, y);
	SDK::Interfaces::Surface->DrawPrintText(pwszText, WStringLength(pwszText));
}

void Util::DrawingHelper::GetTextSize(int& w, int& h, SDK::HFont font, const wchar_t* pwszText)
{
	SDK::Interfaces::Surface->GetTextSize(font, pwszText, w, h);
}