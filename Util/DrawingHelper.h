#ifndef __HEADER_DRAWINGHELPER__
#define __HEADER_DRAWINGHELPER__
#pragma once

namespace Util
{
	namespace DrawingHelper
	{
		void DrawFilledRect(int x, int y, int w, int h, SDK::Color clr);
		void DrawOutlinedRect(int x, int y, int w, int h, SDK::Color clr);
		void DrawLine(int x1, int y1, int x2, int y2, SDK::Color clr);
		void DrawText(SDK::HFont font, int x, int y, const wchar_t* pwszText, SDK::Color clr);
		void GetTextSize(int& w, int& h, SDK::HFont font, const wchar_t* pwszText);

		namespace Fonts
		{
			void Initialize();

			extern SDK::HFont SmallFonts12SemiBold;
			extern SDK::HFont SmallFonts14SemiBold;
			extern SDK::HFont SmallFonts12Medium;
		}
	}
}

#endif