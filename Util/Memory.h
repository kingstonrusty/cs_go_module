#ifndef __HEADER_MEMORYUTIL__
#define __HEADER_MEMORYUTIL__
#pragma once

#include "Wrappers/String.h"

#define GET_VFUNC(FN, X, IDX) \
((FN)(((*(uint32_t**)((uint32_t)(X)))[(IDX)])))

namespace Util
{
	namespace Memory
	{
		inline void* FindByte(void* pAddress, uint8_t u8Byte, size_t nLength, uint32_t u32Add);

		inline bool CompareMemory(void* pAddress, const char* pszPattern, size_t nLen);
		inline void* FindPattern(const wchar_t* pwszModule, const char* pszPattern);
	}
}

inline void* Util::Memory::FindByte(void* pAddress, uint8_t u8Byte, size_t nLength, uint32_t u32Add)
{
	uint8_t* _pAddress = (uint8_t*)pAddress;
	for (uint32_t i = 0; i < nLength; i++, _pAddress++)
	{
		if (*_pAddress == u8Byte)
		{
			return _pAddress + u32Add;
		}
	}

	return nullptr;
}

inline bool Util::Memory::CompareMemory(void* pAddress, const char* pszPattern, size_t nLen)
{
#define TO_HEX(c) \
((c >= '0' && c <= '9') ? (c - '0') : (c - 'A' + 10))

#define TO_BYTE(a, b) \
((TO_HEX(a) << 4) + TO_HEX(b))

	uint8_t* _pAddress = (uint8_t*)pAddress;

	for (uint32_t nCur = 0; nCur < nLen; nCur += 3, _pAddress++)
	{
		if (pszPattern[nCur] == '?')
		{
			continue;
		}

		if (*_pAddress != TO_BYTE(pszPattern[nCur], pszPattern[nCur + 1]))
		{
			return false;
		}
	}

	return true;

#undef TO_HEX
#undef TO_BYTE
}

inline void* Util::Memory::FindPattern(const wchar_t* pwszModule, const char* pszPattern)
{
#if defined(_DEBUG)
	HMODULE hModule = GetModuleHandleW(pwszModule);
#else
	HMODULE hModule = GetModuleHandle(Util::CRC32::HashSingleBuffer(pwszModule, WStringLength(pwszModule) * sizeof(wchar_t)));
#endif
	if (hModule == nullptr)
	{
		return nullptr;
	}

	PIMAGE_DOS_HEADER pDOSHeader = (PIMAGE_DOS_HEADER)hModule;
	if (pDOSHeader->e_magic != IMAGE_DOS_SIGNATURE)
	{
		return nullptr;
	}

	PIMAGE_NT_HEADERS32 pNTHeaders = (PIMAGE_NT_HEADERS32)((uint32_t)pDOSHeader + pDOSHeader->e_lfanew);
	if (pNTHeaders->Signature != IMAGE_NT_SIGNATURE)
	{
		return nullptr;
	}

	PIMAGE_OPTIONAL_HEADER32 pOptionalHeader = &pNTHeaders->OptionalHeader;
	if (pOptionalHeader->Magic != IMAGE_NT_OPTIONAL_HDR32_MAGIC)
	{
		return nullptr;
	}

	int32_t u32Size = pOptionalHeader->SizeOfCode - StringLength(pszPattern) / 3 + 1;
	uint32_t u32Address = (uint32_t)pDOSHeader + pOptionalHeader->BaseOfCode;
	size_t nLen = StringLength(pszPattern);

	while (u32Size > 0)
	{
		if (CompareMemory((void*)u32Address, pszPattern, nLen))
		{
			return (void*)u32Address;
		}

		u32Size--;
		u32Address++;
	}

	return nullptr;
}

#endif