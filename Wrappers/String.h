#ifndef __HEADER_WRAPPER_STRING__
#define __HEADER_WRAPPER_STRING__
#pragma once

size_t StringLength(const char* pszString);
size_t WStringLength(const wchar_t* pwszString);

void FormatString(char* pszBuffer, size_t nBufferLen, const char* pszFormat, ...);
void FormatWString(wchar_t* pwszBuffer, size_t nBufferLen, const wchar_t* pwszFormat, ...);

int WStringToInt(const wchar_t* pwszString);
int StringToInt(const char* pszString);

bool StringEquals(const char* pszString1, const char* pszString2);

#endif