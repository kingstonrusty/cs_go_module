#include "String.h"

size_t StringLength(const char* pszString)
{
#if defined(_DEBUG)
	return strlen(pszString);
#else
	size_t nLen = 0;
	while (*pszString != '\0')
	{
		nLen++;
		pszString++;
	}

	return nLen;
#endif
}

size_t WStringLength(const wchar_t* pwszString)
{
#if defined(_DEBUG)
	return lstrlenW(pwszString);
#else
	size_t nLen = 0;
	while (*pwszString != '\0')
	{
		nLen++;
		pwszString++;
	}

	return nLen;
#endif
}

void FormatString(char* pszBuffer, size_t nBufferLen, const char* pszFormat, ...)
{
	va_list list;
	va_start(list, pszFormat);

	_vsnprintf(pszBuffer, nBufferLen, pszFormat, list);

	va_end(list);
}

void FormatWString(wchar_t* pwszBuffer, size_t nBufferLen, const wchar_t* pwszFormat, ...)
{
	va_list list;
	va_start(list, pwszFormat);

	vswprintf_s(pwszBuffer, nBufferLen, pwszFormat, list);

	va_end(list);
}

int WStringToInt(const wchar_t* pwszString)
{
#if defined(_DEBUG)
	return _wtoi(pwszString);
#else
	int res = 0;
	int sign = 1;
	int i = 0;

	if (pwszString[0] == '-')
	{
		sign = -1;
		i++;
	}

	for (; pwszString[i] != '\0'; ++i)
		res = res * 10 + pwszString[i] - '0';

	return sign*res;
#endif
}

int StringToInt(const char* pszString)
{
#if defined(_DEBUG)
	return atoi(pszString);
#else
	int res = 0;
	int sign = 1;
	int i = 0;

	if (pszString[0] == '-')
	{
		sign = -1;
		i++;
	}

	for (; pszString[i] != '\0'; ++i)
		res = res * 10 + pszString[i] - '0';

	return sign*res;
#endif
}

bool StringEquals(const char* pszString1, const char* pszString2)
{
#if defined(_DEBUG)
	return strcmp(pszString1, pszString2) == 0;
#endif
	while (*pszString1)
	{
		if (*pszString1 != *pszString2)
		{
			return false;
		}

		pszString1++;
		pszString2++;
	}

	return *pszString2 == '\0';
}