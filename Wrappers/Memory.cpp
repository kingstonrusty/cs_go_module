#include "Memory.h"

#if !defined(_DEBUG)
static PVOID s_pHeapHandle;
#endif

#if !defined(_DEBUG)
void InitializeHeap()
{
	s_pHeapHandle = RtlCreateHeap(0x2/*HEAP_GROWABLE*/, nullptr, 0, 0, nullptr, nullptr);
}
#endif

void CopyMemory(void* pDest, const void* pSrc, size_t nLen)
{
#if defined(_DEBUG)
	memcpy(pDest, pSrc, nLen);
#else
	uint8_t* _pDest = (uint8_t*)pDest;
	const uint8_t* _pSrc = (const uint8_t*)pSrc;

	for (size_t i = 0; i < nLen; i++, _pDest++, _pSrc++)
	{
		*_pDest = *_pSrc;
	}
#endif
}

void SetMemory(void* pDest, uint8_t u8Value, size_t nLen)
{
#if defined(_DEBUG)
	memset(pDest, u8Value, nLen);
#else
	uint8_t* _pDest = (uint8_t*)pDest;

	for (size_t i = 0; i < nLen; i++, _pDest++)
	{
		*_pDest = u8Value;
	}
#endif
}

bool CompareMemory(const void* pBlock1, const void* pBlock2, size_t nLen)
{
#if defined(_DEBUG)
	return memcmp(pBlock1, pBlock2, nLen) == 0;
#else
	const uint8_t* _pBlock1 = (const uint8_t*)pBlock1;
	const uint8_t* _pBlock2 = (const uint8_t*)pBlock2;

	for (size_t i = 0; i < nLen; i++, _pBlock1++, _pBlock2++)
	{
		if (*_pBlock1 != *_pBlock2)
		{
			return false;
		}
	}

	return true;
#endif
}

void* AllocateMemory(size_t nSize)
{
#if defined(_DEBUG)
	return malloc(nSize);
#else
	return RtlAllocateHeap(s_pHeapHandle, 0, nSize);
#endif
}

void* ReallocateMemory(void* pMemory, size_t nSize)
{
#if defined(_DEBUG)
	return realloc(pMemory, nSize);
#else
	return RtlReAllocateHeap(s_pHeapHandle, 0, pMemory, nSize);
#endif
}

void FreeMemory(void* pMemory)
{
#if defined(_DEBUG)
	free(pMemory);
#else
	RtlFreeHeap(s_pHeapHandle, 0, pMemory);
#endif
}

void* AllocateMemoryAndSetData(size_t nSize, uint8_t u8Value)
{
	void* pMemory = AllocateMemory(nSize);
	SetMemory(pMemory, u8Value, nSize);
	return pMemory;
}