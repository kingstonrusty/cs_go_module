#if !defined(_DEBUG)

#include "Math.h"

float sqrtf(float f)
{
	float sq;
	__asm
	{
		sqrtss xmm0, f
		movss sq, xmm0
	}

	return sq;
}

#endif