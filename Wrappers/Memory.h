#ifndef __HEADER_WRAPPER_MEMORY__
#define __HEADER_WRAPPER_MEMORY__
#pragma once

#if defined(_DEBUG)
#undef CopyMemory
#endif

#if !defined(_DEBUG)
void InitializeHeap();
#endif

//Copy, Set, etc
void CopyMemory(void* pDest, const void* pSrc, size_t nLen);
void SetMemory(void* pDest, uint8_t u8Value, size_t nLen);
bool CompareMemory(const void* pBlock1, const void* pBlock2, size_t nLen);

//Alloc, Etc
void* AllocateMemory(size_t nSize);
void* ReallocateMemory(void* pMemory, size_t nSize);
void FreeMemory(void* pMemory);
void* AllocateMemoryAndSetData(size_t nSize, uint8_t u8Value);

#endif