#if !defined(_DEBUG)

#ifndef __HEADER_MATH__
#define __HEADER_MATH__
#pragma once

float sqrtf(float f);

#endif

#endif