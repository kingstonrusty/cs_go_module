#include "Autowall.h"

static void UTIL_ClipTraceToPlayers(SDK::Vector& start, SDK::Vector& end, unsigned int mask, SDK::ITraceFilter* filter, SDK::trace_t* trace)
{
	__asm
	{
		mov eax, filter
		lea ecx, trace
		push ecx
		push eax
		push mask
		lea edx, end
		lea ecx, start
		call SDK::Offsets::Other::UTIL_ClipTraceToPlayers
		add esp, 12
	}
}

static SDK::ConVar* sv_clip_penetration_traces_to_players;

static bool TraceToExit(SDK::Vector* exit, SDK::trace_t* enter_trace, SDK::Vector& start, SDK::Vector& dir, SDK::trace_t* exit_trace)
{
	if (!sv_clip_penetration_traces_to_players)
	{
		sv_clip_penetration_traces_to_players = SDK::Interfaces::Cvar->FindVar("sv_clip_penetration_traces_to_players");
	}

	float dist = 0.0f;
	SDK::Ray_t ray;
	int oldcontents = 0;

	do
	{
		dist += 4.0f;
		*exit = start + dir * dist;
		
		if(!oldcontents)
			oldcontents = SDK::Interfaces::EngineTrace->GetPointContents(*exit, 0x4600400B, nullptr);

		int contents = SDK::Interfaces::EngineTrace->GetPointContents(*exit, 0x4600400B, nullptr);
		if (!(contents & 0x600400B) || contents & 0x40000000 && contents != oldcontents)
		{
			ray.Init(*exit, *exit - dir * 4.0f);
			SDK::Interfaces::EngineTrace->TraceRay(ray, 0x4600400B, nullptr, exit_trace);

			if (sv_clip_penetration_traces_to_players->GetInt())
				UTIL_ClipTraceToPlayers(*exit, *exit - dir * 4.0f, 0x4600400B, nullptr, exit_trace);

			if (exit_trace->startsolid && exit_trace->surface.flags & 0x8000)
			{
				SDK::CTraceFilterSimple filter = SDK::CTraceFilterSimple((SDK::C_BaseEntity*)exit_trace->m_pEnt);
				ray.Init(*exit, start);
				SDK::Interfaces::EngineTrace->TraceRay(ray, 0x600400B, &filter, exit_trace);

				if ((exit_trace->fraction < 1.0f || exit_trace->allsolid || exit_trace->startsolid) && !exit_trace->startsolid)
				{
					*exit = exit_trace->endpos;
					return true;
				}

				continue;
			}

			if (!(exit_trace->fraction < 1.0f || exit_trace->allsolid || exit_trace->startsolid) || exit_trace->startsolid)
			{
				if (enter_trace->m_pEnt && enter_trace->m_pEnt->GetNetworkable()->EntIndex() != 0)
				{
					if (((SDK::C_BaseEntity*)enter_trace->m_pEnt)->IsBreakable())
					{
						CopyMemory(exit_trace, enter_trace, sizeof(SDK::trace_t));
						exit_trace->endpos = start + dir;
						return true;
					}
				}

				continue;
			}

			bool v28 = ((enter_trace->surface.flags >> 7) & 1);
			if ((exit_trace->surface.flags >> 7) & 1)
			{
				if (exit_trace->m_pEnt && ((SDK::C_BaseEntity*)exit_trace->m_pEnt)->IsBreakable() && enter_trace->m_pEnt && ((SDK::C_BaseEntity*)enter_trace->m_pEnt)->IsBreakable())
				{
					*exit = exit_trace->endpos;
					return true;
				}

				if (!v28)
					continue;
			}

			if (exit_trace->plane.normal.Dot(dir) <= 1.0f)
			{
				float v30 = exit_trace->fraction * 4.0f;
				*exit = *exit - dir * v30;

				return true;
			}
		}
	} while (dist <= 90.f);

	return false;
}

static SDK::ConVar* sv_penetration_type;
static SDK::ConVar* ff_damage_reduction_bullets;
static SDK::ConVar* ff_damage_bullet_penetration;

static bool HandleBulletPenetration(SDK::C_CSPlayer* pLocal, SDK::trace_t* enter_trace, SDK::surfacedata_t* enter_surface_data, SDK::Vector* src, SDK::Vector* dir, float constpen, float* penetrationpower, int* pencount, float* dmg)
{
	if (!sv_penetration_type)
	{
		sv_penetration_type = SDK::Interfaces::Cvar->FindVar(XorStr("sv_penetration_type"));
		ff_damage_reduction_bullets = SDK::Interfaces::Cvar->FindVar(XorStr("ff_damage_reduction_bullets"));
		ff_damage_bullet_penetration = SDK::Interfaces::Cvar->FindVar(XorStr("ff_damage_bullet_penetration"));
	}

	float v1;
	bool v42;
	float v46;
	bool v68;
	SDK::trace_t exit_trace;
	SDK::Vector exit;
	SDK::surfacedata_t* exit_surface_data;
	int penetration_type;
	float pen_mod;
	float dmg_mod;
	float dmg_lost;

	penetration_type = sv_penetration_type->GetInt();
	v68 = ((enter_trace->surface.flags >> 7) & 1);

	if (*pencount <= 0 || *penetrationpower <= 0.0f)
	{
		return true;
	}

	if (!TraceToExit(&exit, enter_trace, enter_trace->endpos, *dir, &exit_trace))
	{
		if(!(SDK::Interfaces::EngineTrace->GetPointContents(exit_trace.endpos, 0x600400B, nullptr) & 0x600400B))
			return true;
	}

	exit_surface_data = SDK::Interfaces::PhysProps->GetSurfaceData(exit_trace.surface.surfaceProps);

	if (penetration_type != 1) // this is currently broken
	{
		float v47;
		float v48 = 1.0f;

		if (v68)
		{
			v47 = exit_surface_data->game.flDamageModifier;
			v48 = exit_surface_data->game.flPenetrationModifier;
			if (v48 > enter_surface_data->game.flPenetrationModifier)
				v48 = enter_surface_data->game.flPenetrationModifier;
		}
		else
		{
			v47 = 0.99f;
		}

		if (enter_surface_data->game.material == exit_surface_data->game.material && (exit_surface_data->game.material == 87 || exit_surface_data->game.material == 77))
		{
			v48 *= 2.0f;
		}

		if ((exit_trace.endpos - enter_trace->endpos).Length() > (float)(v48 * constpen))
			return true;

		*dmg *= v47;
		*src = exit_trace.endpos;
		*--pencount;

		return false;
	}

	dmg_mod = 0.16f;

	if (v68)
	{
		if (enter_surface_data->game.material == 70)
		{
			if (ff_damage_reduction_bullets->GetFloat() != 0.0f || !enter_trace->m_pEnt)
			{
				pen_mod = (exit_surface_data->game.flPenetrationModifier + enter_surface_data->game.flPenetrationModifier) * 0.5f;
				dmg_mod = 0.16f;
			}
			else
			{
				int idx = enter_trace->m_pEnt->GetNetworkable()->EntIndex();
				if (idx >= 1 && idx <= SDK::Interfaces::Globals->maxClients() && ((SDK::C_CSPlayer*)enter_trace->m_pEnt)->IsAlive())
				{
					SDK::C_CSPlayer* pPlayer = (SDK::C_CSPlayer*)enter_trace->m_pEnt;
					if (pPlayer->IsAlive() && pPlayer->GetTeamNum() == pLocal->GetTeamNum())
					{
						if (ff_damage_bullet_penetration->GetFloat() == 0.0f)
						{
							return true;
						}

						pen_mod = ff_damage_bullet_penetration->GetFloat();
						dmg_mod = 0.16f;
					}
					else
					{
						pen_mod = (exit_surface_data->game.flPenetrationModifier + enter_surface_data->game.flPenetrationModifier) * 0.5f;
						dmg_mod = 0.16f;
					}
				}
				else
				{
					pen_mod = (exit_surface_data->game.flPenetrationModifier + enter_surface_data->game.flPenetrationModifier) * 0.5f;
					dmg_mod = 0.16f;
				}
			}
		}
		else
		{
			pen_mod = 3.0f;
			dmg_mod = 0.05f;

			if (enter_surface_data->game.material != 71 && enter_surface_data->game.material != 89)
			{
				pen_mod = 1.0f;
				dmg_mod = 0.16f;
			}
		}
	}
	else
	{
		pen_mod = 3.0f;
		dmg_mod = 0.05f;

		if (enter_surface_data->game.material != 71 && enter_surface_data->game.material != 89)
		{
			pen_mod = (exit_surface_data->game.flPenetrationModifier + enter_surface_data->game.flPenetrationModifier) * 0.5f;
			dmg_mod = 0.16f;
		}
	}

	if (enter_surface_data->game.material == exit_surface_data->game.material)
	{
		if (exit_surface_data->game.material == 85 || exit_surface_data->game.material == 87)
		{
			pen_mod = 3.0f;
		}
		else if (exit_surface_data->game.material == 76)
		{
			pen_mod = 2.0f;
		}
	}

	float v56 = 1.0f / pen_mod;
	if (v56 < 0.0f) v56 = 0.0f;

	float v58 = (3.0f / constpen) * 1.25f;
	if (v58 < 0.0f) v58 = 0.0f;

	float v57 = v56 * 3.0f * v58 + (dmg_mod * *dmg);

	float v36 = (exit_trace.endpos - enter_trace->endpos).Length();
	v36 = (((v36 * v36) * v56) / 24.0f) + v57;

	dmg_lost = v36;

	*dmg -= dmg_lost;

	if (*dmg < 1.0f) 
		return true;

	*src = exit_trace.endpos;
	*--pencount;

	return false;
}

static void UTIL_TraceLine(SDK::Vector& start, SDK::Vector& endpos, unsigned int mask, SDK::ITraceFilter* filter, SDK::trace_t* trace)
{
	SDK::Ray_t ray;
	ray.Init(start, endpos);

	SDK::Interfaces::EngineTrace->TraceRay(ray, mask, filter, trace);
}

static SDK::ConVar* mp_damage_scale_ct_body;
static SDK::ConVar* mp_damage_scale_t_body;
static SDK::ConVar* mp_damage_scale_ct_head;
static SDK::ConVar* mp_damage_scale_t_head;

static float GetDamageMultiplier(int hitgroup, bool isct, SDK::C_CSPlayer* pPlayer)
{
	if (!mp_damage_scale_ct_body)
	{
		mp_damage_scale_ct_body = SDK::Interfaces::Cvar->FindVar(XorStr("mp_damage_scale_ct_body"));
		mp_damage_scale_t_body = SDK::Interfaces::Cvar->FindVar(XorStr("mp_damage_scale_t_body"));
		mp_damage_scale_ct_head = SDK::Interfaces::Cvar->FindVar(XorStr("mp_damage_scale_ct_head"));
		mp_damage_scale_t_head = SDK::Interfaces::Cvar->FindVar(XorStr("mp_damage_scale_t_head"));
	}

	switch (hitgroup)
	{
	case 6: // left leg
	case 7: // right leg
		return 0.75f;

	case 4: // left arm
	case 5: // right arm
		return (isct) ? mp_damage_scale_ct_body->GetFloat() : mp_damage_scale_t_body->GetFloat();

	case 1: //head
		return (4.0f * ((isct) ? mp_damage_scale_ct_head->GetFloat() : mp_damage_scale_t_head->GetFloat())) * (pPlayer->HasHeavyArmor() ? 0.5f : 1.0f);

	case 3: //stomach
		return 1.25f;

	case 2: //chest
		return 1.25f * (isct) ? mp_damage_scale_ct_body->GetFloat() : mp_damage_scale_t_body->GetFloat();

	default:
		break;
	}

	return 1.0f;
}

static void ScaleDamage(float& damage, int hitgroup, SDK::C_CSPlayer* pTarget, float armor_ratio)
{
	int armor_value = pTarget->GetArmorValue();
	damage *= GetDamageMultiplier(hitgroup, pTarget->GetTeamNum() == 3, pTarget);

	if (pTarget->HasHeavyArmor() || (armor_value > 0 && ((hitgroup == 1 && pTarget->HasHelmet()) || hitgroup != 1)))
	{
		float v38 = armor_ratio * 0.5f;
		float v153 = damage * v38;
		float v39 = (damage - v153) * 0.5f;

		if (v39 <= (float)armor_value)
		{
			damage = v153;
		}
		else
		{
			damage -= armor_value * 2.0f;
		}
	}
}

float Features::Ragebot::Autowall::GetDamage(SDK::Vector& _src, SDK::Vector& _dir, SDK::C_CSPlayer* pLocal, SDK::C_CSPlayer* pTarget, SDK::C_WeaponCSBase* pWeapon)
{
	SDK::CCSWeaponInfo* weapon_info = pWeapon->GetCSWpnData();
	SDK::trace_t enter_trace;
	float penetrationpower = weapon_info->GetPenetration();
	int penetrationcount = 4;
	float damage = (float)weapon_info->GetDamage();
	float range = weapon_info->GetRange();
	SDK::Vector src = _src;
	SDK::Vector dir = _dir;
	SDK::C_BaseEntity* last = pLocal;
	float v103;
	float v144;
	float v153 = 0.0f;
	float v158 = 0.0f;

	while (true)
	{
		SDK::CTraceFilterSkipTwoEntities filter = SDK::CTraceFilterSkipTwoEntities(pLocal, last);
		UTIL_TraceLine(src, src + dir * range, 0x4600400B, &filter, &enter_trace);
		UTIL_ClipTraceToPlayers(src, src + (dir * range) * 40.f, 0x4600400B, &filter, &enter_trace);

		if (enter_trace.fraction == 1.0f)
			break;

		last = (SDK::C_BaseEntity*)enter_trace.m_pEnt;

		range -= (src - enter_trace.endpos).Length();
		if (range < 0.0f)
			break;
		
		v158 += (weapon_info->GetRange() - v158) * enter_trace.fraction;
		v103 = pow(weapon_info->GetRangeMod(), v158 / 500.0f);
		damage *= v103;

		if (damage < 1.0f)
			break;

		if (enter_trace.m_pEnt)
		{
			if(enter_trace.m_pEnt == pTarget)
			{
				ScaleDamage(damage, enter_trace.hitgroup, pTarget, weapon_info->GetArmorRatio());
				if (damage < 1.0f)
					break;

				return damage;
			}
			else
			{
				int idx = enter_trace.m_pEnt->GetNetworkable()->EntIndex();
				if (idx >= 1 && idx <= SDK::Interfaces::Globals->maxClients())
				{
					SDK::C_CSPlayer* pPlayer = (SDK::C_CSPlayer*)enter_trace.m_pEnt;

					if (pPlayer->GetTeamNum() == pLocal->GetTeamNum() && !Vars.Aimbot.Rage.PenetrateTeammates)
					{
						break;
					}
				}
			}
		}

		SDK::surfacedata_t* enter_surface_date = SDK::Interfaces::PhysProps->GetSurfaceData(enter_trace.surface.surfaceProps);
		if (HandleBulletPenetration(pLocal, &enter_trace, enter_surface_date, &src, &dir, weapon_info->GetPenetration(), &penetrationpower, &penetrationcount, &damage))
		{
			break;
		}
	}

	return -1.0f;
}

static uint8_t s_Backup[4096];

void Features::Ragebot::Autowall::FixBreakables()
{
	/*for (int i = SDK::Interfaces::Globals->maxClients() + 1; i <= SDK::Interfaces::EntityList->GetHighestEntityIndex(); i++)
	{
		SDK::C_BaseEntity* pEntity = (SDK::C_BaseEntity*)SDK::Interfaces::EntityList->GetClientEntity(i);
		if (!pEntity)
		{
			continue;
		}

		s_Backup[i] = *(uint8_t*)((uint32_t)pEntity + 636);
		*(uint8_t*)((uint32_t)pEntity + 636) = 2;//DAMAGE_YES
	}*/
}

void Features::Ragebot::Autowall::RestoreBreakable()
{
	/*for (int i = SDK::Interfaces::Globals->maxClients() + 1; i <= SDK::Interfaces::EntityList->GetHighestEntityIndex(); i++)
	{
		SDK::C_BaseEntity* pEntity = (SDK::C_BaseEntity*)SDK::Interfaces::EntityList->GetClientEntity(i);
		if (!pEntity)
		{
			continue;
		}

		*(uint8_t*)((uint32_t)pEntity + 636) = s_Backup[i];
	}*/
}

/*
	--[[local dist = 0.0;
	local oldcontents = 0;
	local exit;

	repeat

		dist = dist + 4.0;
		exit = start + dir * dist;

		if(oldcontents == 0)
		then

			oldcontents = UTIL_PointContents(exit);

		end

		local contents = UTIL_PointContents(exit);
		if(bit.band(contents, 0x600400B) == 0 or (bit.band(contents, 0x40000000) != 0 and contents != oldcontents))
		then

			table.CopyFromTo(UTIL_TraceLine(exit, exit - dir * 4.0, 0x4600400B, nil), exit_trace);

			if(exit_trace.StartSolid and exit_trace.Entity:IsValid() and exit_trace.HitBox != 0 and exit_trace.HitGroup != 0) -- very ghetto fix
			then -- hit entity, re-trace with filter

				if(not exit_trace.Entity:IsPlayer() or exit_trace.Entity:Alive())
				then

					table.CopyFromTo(UTIL_TraceLine(exit, start, 0x600400B, exit_trace.Entity), exit_trace);

					if((exit_trace.Fraction < 1.0 or exit_trace.AllSolid or exit_trace.StartSolid) and not exit_trace.StartSolid)
					then

						return true;

					end

					continue;

				end

			end

			if(not (exit_trace.Fraction < 1.0 or exit_trace.AllSolid or exit_trace.StartSolid) or exit_trace.StartSolid)
			then

				if(enter_trace.Entity:IsValid() and enter_trace.Entity:EntIndex() != 0)
				then

					if(IsBreakableEntity(enter_trace.Entity))
					then

						table.CopyFromTo(enter_trace, exit_trace);
						exit_trace.HitPos = start + dir;
						return true;

					end

				end

				continue;

			end

			local enter_hitgrate = false;
			local hitgrate = false; -- thanks again
			if(hitgrate)
			then

				if(exit_trace.Entity:IsValid() and exit_trace.Entity:EntIndex() != 0 and enter_trace.Entity:IsValid() and enter_trace.Entity:IsValid())
				then

					if(IsBreakableEntity(enter_trace.Entity) and IsBreakableEntity(exit_trace.Entity))
					then

					end

				end

				if(not enter_hitgrate)
				then

					continue;

				end

			end

			if(exit_trace.HitNormal:Dot(dir) <= 1.0)
			then

				return true;

			end

		end

	until (dist > 90.0)]]
*/