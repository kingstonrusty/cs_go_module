#ifndef __HEADER_AUTOWALL__
#define __HEADER_AUTOWALL__
#pragma once

namespace Features
{
	namespace Ragebot
	{
		namespace Autowall
		{
			float GetDamage(SDK::Vector& _src, SDK::Vector& _dir, SDK::C_CSPlayer* pLocal, SDK::C_CSPlayer* pTarget, SDK::C_WeaponCSBase* pWeapon);
			void FixBreakables();
			void RestoreBreakable();
		}
	}
}

#endif