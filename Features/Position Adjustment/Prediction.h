#ifndef __HEADER_PREDICTION__
#define __HEADER_PREDICTION__
#pragma once

namespace Features
{
	namespace PositionAdjustment
	{
		namespace Prediction
		{
			void Run(SDK::C_CSPlayer* pPlayer, SDK::CUserCmd* cmd);
			void Finish(SDK::C_CSPlayer* pPlayer, SDK::CUserCmd* cmd);
		}
	}
}

#endif