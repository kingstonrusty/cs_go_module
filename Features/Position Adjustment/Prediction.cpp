#include "Prediction.h"

struct
{
	int						m_nOldTickBase;
	float					m_flOldCurTime;
	float					m_flOldFrameTime;
	int						m_nPredictionRandomSeed;
	SDK::C_BasePlayer		*m_pPredictionPlayer;
	SDK::CUserCmd			*m_pCurrentCommand;
	int						m_nOldCmdRandomSeed;
} static s_Backup;

static int s_nOldTickBase;
static int s_nTickBaseAdjust;

static void DoWeaponSelection(SDK::C_CSPlayer* pPlayer, SDK::CUserCmd* cmd)
{
	uint32_t* dword_4A87B1C = (uint32_t*)SDK::Offsets::Other::Prediction::dword_4A87B1C;

	uint32_t weaponselect = cmd->weaponselect();
	if (weaponselect)
	{
		if (weaponselect >= 0)
		{
			uint32_t v9 = dword_4A87B1C[4 * weaponselect];    // C_BaseEntity::Instance
			if (v9)
			{
				int v10 = (*(int(__thiscall**)(uint32_t))(*(uint32_t*)v9 + 28))(v9);
				if (v10)
				{
					int v11 = (*(int(__thiscall **)(int))(*(uint32_t*)v10 + 644))(v10);
					int v12 = v11;
					if (v11)
					{
						if ((*(int(__thiscall **)(int))(*(uint32_t *)v11 + 1096))(v11) == cmd->weaponsubtype())// ? == weaponsubtype
							(*(void(__thiscall **)(int, int))(*(uint32_t *)pPlayer + 1272))((int)pPlayer, v12);// player->SelectItem
					}
				}
			}
		}
	}
}

static void UpdateButtonState(SDK::C_CSPlayer* pPlayer, SDK::CUserCmd* cmd)
{
	static uint32_t dwButtonOffset = SDK::Offsets::C_BasePlayer::ButtonOffset;
	static uint32_t dwDisabledButtonOffset = SDK::Offsets::C_BasePlayer::DisabledButtonOffset;
	uint32_t a2 = (uint32_t)pPlayer;

	cmd->buttons() |= *(uint32_t*)(a2 + dwDisabledButtonOffset);

	int buttons = cmd->buttons();
	int v17 = buttons ^ *(int*)(a2 + dwButtonOffset + 0xC);
	*(int*)(a2 + dwButtonOffset) = *(int*)(a2 + dwButtonOffset + 0xC);
	*(int*)(a2 + dwButtonOffset + 0xC) = buttons;
	*(int*)(a2 + dwButtonOffset + 0x4) = buttons & v17;
	*(int*)(a2 + dwButtonOffset + 0x8) = v17 & ~buttons;
}

void Features::PositionAdjustment::Prediction::Run(SDK::C_CSPlayer* pPlayer, SDK::CUserCmd* cmd)
{
	s_Backup.m_nOldTickBase = pPlayer->GetTickBase();
	s_Backup.m_flOldCurTime = SDK::Interfaces::Globals->curtime();
	s_Backup.m_flOldFrameTime = SDK::Interfaces::Globals->frametime();
	s_Backup.m_nPredictionRandomSeed = SDK::C_BasePlayer::GetPredictionRandomSeed();
	s_Backup.m_pPredictionPlayer = SDK::C_BasePlayer::GetPredictionPlayer();
	s_Backup.m_pCurrentCommand = pPlayer->GetCurrentCommand();
	s_Backup.m_nOldCmdRandomSeed = cmd->random_seed();

	if (s_nOldTickBase != pPlayer->GetTickBase())
	{
		s_nOldTickBase = pPlayer->GetTickBase();
		s_nTickBaseAdjust = 0;
	}
	else
	{
		s_nTickBaseAdjust++;

		pPlayer->SetTickBase(s_nOldTickBase + s_nTickBaseAdjust);
	}

	SDK::Interfaces::Globals->curtime() = SDK::Interfaces::Globals->interval_per_tick() * (float)pPlayer->GetTickBase();
	SDK::Interfaces::Globals->frametime() = SDK::Interfaces::Globals->interval_per_tick();

	cmd->random_seed() = SDK::MD5_PseudoRandom(cmd->command_number()) & 0x7FFFFFFF;
	SDK::C_BasePlayer::SetPredictionRandomSeed(cmd->random_seed());
	SDK::C_BasePlayer::SetPredictionPlayer(pPlayer);
	pPlayer->SetCurrentCommand(cmd);

	DoWeaponSelection(pPlayer, cmd);
	UpdateButtonState(pPlayer, cmd);

	if (pPlayer->PhysicsRunThink(SDK::THINK_FIRE_ALL_FUNCTIONS))
	{
		pPlayer->PreThink();
	}

	int nNextThinkTick = pPlayer->GetNextThinkTick();
	if (nNextThinkTick != -1 && nNextThinkTick > 0 && nNextThinkTick <= pPlayer->GetTickBase())
	{
		pPlayer->SetNextThinkTick(-1);

		pPlayer->Think_NoIdea1(0);
		pPlayer->Think();
	}

	SDK::IMoveHelper::GetPtr()->SetHost(pPlayer);

	SDK::Interfaces::Prediction->SetupMove(pPlayer, cmd, SDK::IMoveHelper::GetPtr(), SDK::Interfaces::GameMovement->GetMoveData());
	SDK::Interfaces::GameMovement->ProcessMovement(pPlayer, SDK::Interfaces::GameMovement->GetMoveData());
	SDK::Interfaces::Prediction->FinishMove(pPlayer, cmd, SDK::Interfaces::GameMovement->GetMoveData());

	if (pPlayer->GetLifeState() == 0/*LIFE_ALIVE*/)
	{
		DWORD v2 = (DWORD)pPlayer;

		pPlayer->PostThink_NoIdea1();

		if (pPlayer->GetFlags() & 1/*FL_ONGROUND*/) // if ( GetFlags() & FL_ONGROUND )
			pPlayer->SetFallVelocity(0.f); // m_Local.m_flFallVelocity = 0;

		if (pPlayer->GetSequence() == -1)         // if ( GetSequence() == -1 )
			(*(void(__thiscall **)(void *, DWORD))(*(DWORD *)v2 + 852))((void*)v2, 0);// SetSequence( 0 );

		(*(void(__thiscall **)(void *))(*(DWORD *)v2 + 856))((void*)v2);// StudioFrameAdvance();

		pPlayer->PostThinkVPhysics();
	}

	pPlayer->SimulatePlayerSimulatedEntities();
}

void Features::PositionAdjustment::Prediction::Finish(SDK::C_CSPlayer* pPlayer, SDK::CUserCmd* cmd)
{
	pPlayer->SetTickBase(s_Backup.m_nOldTickBase);

	SDK::Interfaces::Globals->curtime() = s_Backup.m_flOldCurTime;
	SDK::Interfaces::Globals->frametime() = s_Backup.m_flOldFrameTime;

	SDK::C_BasePlayer::SetPredictionRandomSeed(s_Backup.m_nPredictionRandomSeed);
	SDK::C_BasePlayer::SetPredictionPlayer(s_Backup.m_pPredictionPlayer);
	pPlayer->SetCurrentCommand(s_Backup.m_pCurrentCommand);
	cmd->random_seed() = s_Backup.m_nOldCmdRandomSeed;

	SDK::IMoveHelper::GetPtr()->SetHost(nullptr);
}