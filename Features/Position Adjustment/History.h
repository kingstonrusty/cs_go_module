#ifndef __HEADER_HISTORY__
#define __HEADER_HISTORY__
#pragma once

namespace Features
{
	namespace PositionAdjustment
	{
		namespace History
		{
			struct SSnapshot
			{
				int m_nServerTick;
				float m_flSimulationTime;
				bool m_bObsolete;
				SDK::matrix3x4 m_BoneCache[3][128];
			};

			void Record();
			void SaveBoneCache(SDK::C_CSPlayer* pPlayer, float flSimulationTime, int nSlot);
			void UpdateObsoleteSnapshots();
			void MarkAllSnapshotsAsObsoleteForIndex(int idx);
			void MarkAllSnapshotsAsObsolete();
			bool CanUseSnapshot(SSnapshot* pSnapshot);
		}
	}
}

#endif