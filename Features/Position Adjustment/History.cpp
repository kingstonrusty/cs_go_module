#include "History.h"

#include "Features/Other/BoneCache.h"

static Features::PositionAdjustment::History::SSnapshot s_SnapshotData[MAX_PLAYERS][25];  // 25 ticks on 128tickrate, 12 on 64tickrate
																			      // floor(0.2/(1/tickrate)) 

static Features::PositionAdjustment::History::SSnapshot* s_SnapshotPointers[MAX_PLAYERS][25];

static bool s_bOnce;

void Features::PositionAdjustment::History::Record()
{
	if (!s_bOnce)
	{
		for (int i = 1; i <= MAX_PLAYERS; i++)
		{
			for (int j = 0; j < 25; j++)
			{
				s_SnapshotData[i - 1][j].m_nServerTick = -1;
				s_SnapshotData[i - 1][j].m_flSimulationTime = -1.f;
				s_SnapshotData[i - 1][j].m_bObsolete = true;
				s_SnapshotPointers[i - 1][j] = &s_SnapshotData[i - 1][j];
			}
		}

		s_bOnce = true;
	}

	for (int i = 1; i <= SDK::Interfaces::Globals->maxClients(); i++)
	{
		if (i == SDK::Interfaces::Engine->GetLocalPlayer())
		{
			MarkAllSnapshotsAsObsoleteForIndex(i - 1);
			continue;
		}

		SDK::C_CSPlayer* pPlayer = (SDK::C_CSPlayer*)SDK::Interfaces::EntityList->GetClientEntity(i);
		if (pPlayer == nullptr || !pPlayer->IsAlive())
		{
			MarkAllSnapshotsAsObsoleteForIndex(i - 1);
			continue;
		}

		if (pPlayer->GetSimulationTime() <= s_SnapshotPointers[i - 1][0]->m_flSimulationTime)
		{
			continue;
		}

		for (int j = 24; j > 0; j--) // shift all by 1
		{
			s_SnapshotPointers[i - 1][j] = s_SnapshotPointers[i - 1][j - 1];
		}

		s_SnapshotPointers[i - 1][0]->m_nServerTick = SDK::Interfaces::Engine->GetServerTick();
		s_SnapshotPointers[i - 1][0]->m_flSimulationTime = pPlayer->GetSimulationTime();
		s_SnapshotPointers[i - 1][0]->m_bObsolete = false;

		// we don't save the bone matrix here, we do that in the resolver
	}
}

void Features::PositionAdjustment::History::SaveBoneCache(SDK::C_CSPlayer* pPlayer, float flSimulationTime, int nSlot)
{
	int idx = pPlayer->GetNetworkable()->EntIndex();
	SSnapshot* pFound = nullptr;

	for (int i = 0; i < 25; i++)
	{
		if (s_SnapshotPointers[idx - 1][i]->m_flSimulationTime == flSimulationTime)
		{
			pFound = s_SnapshotPointers[idx - 1][i];

			break;
		}
	}

	if (!pFound)
	{
		return;
	}

	pPlayer->GetRenderable()->SetupBones(pFound->m_BoneCache[nSlot], 128, 0x100, flSimulationTime);
}

void Features::PositionAdjustment::History::MarkAllSnapshotsAsObsoleteForIndex(int idx)
{
	for (int i = 0; i < 25; i++)
	{
		s_SnapshotData[idx][i].m_bObsolete = true;
	}
}

void Features::PositionAdjustment::History::MarkAllSnapshotsAsObsolete()
{
	for (int i = 1; i <= MAX_PLAYERS; i++)
	{
		MarkAllSnapshotsAsObsoleteForIndex(i - 1);
	}
}

void Features::PositionAdjustment::History::UpdateObsoleteSnapshots()
{
	for (int i = 1; i <= SDK::Interfaces::Globals->maxClients(); i++)
	{
		for (int j = 0; j < 25; j++)
		{
			if (s_SnapshotPointers[i - 1][j]->m_bObsolete)
			{
				break;
			}

			if (!CanUseSnapshot(s_SnapshotPointers[i - 1][j]))
			{
				s_SnapshotPointers[i - 1][j]->m_bObsolete = true;
			}
		}
	}
}

bool Features::PositionAdjustment::History::CanUseSnapshot(SSnapshot* pSnapshot)
{
	return false;
}