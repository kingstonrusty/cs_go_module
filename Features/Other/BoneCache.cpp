#include "BoneCache.h"

struct
{
	SDK::matrix3x4 m_Bones[128];
	int m_iMostRecentModelBoneCounter;
	int m_fReadableBones;
	int m_fWriteableBones;
	int m_iPrevBoneMask;
	int m_iAccumulatedBoneMask;
	float m_flLastBoneSetupTime;
} s_Backup;

void Features::Util::BoneCacheHelper::BackupBoneCache(SDK::C_CSPlayer* pPlayer)
{
	s_Backup.m_iMostRecentModelBoneCounter = pPlayer->GetMostRecentBoneCounter();
	s_Backup.m_fReadableBones = pPlayer->GetReadableBones();
	s_Backup.m_fWriteableBones = pPlayer->GetWriteableBones();
	s_Backup.m_flLastBoneSetupTime = pPlayer->GetLastBoneSetupTime();
	s_Backup.m_iPrevBoneMask = pPlayer->GetPreviousBoneMask();
	s_Backup.m_iAccumulatedBoneMask = pPlayer->GetAccumulatedBoneMask();

	for (int i = 0; i < pPlayer->GetCachedBoneCount(); i++)
	{
		CopyMemory(&s_Backup.m_Bones[i], pPlayer->GetCachedBone(i), sizeof(SDK::matrix3x4));
	}
}

void Features::Util::BoneCacheHelper::RestoreBoneCache(SDK::C_CSPlayer* pPlayer)
{
	pPlayer->SetMostRecentBoneCounter(s_Backup.m_iMostRecentModelBoneCounter);
	pPlayer->SetReadableBones(s_Backup.m_fReadableBones);
	pPlayer->SetWriteableBones(s_Backup.m_fWriteableBones);
	pPlayer->SetLastBoneSetupTime(s_Backup.m_flLastBoneSetupTime);
	pPlayer->SetPreviousBoneMask(s_Backup.m_iPrevBoneMask);
	pPlayer->SetAccumulatedBoneMask(s_Backup.m_iAccumulatedBoneMask);

	for (int i = 0; i < pPlayer->GetCachedBoneCount(); i++)
	{
		CopyMemory(pPlayer->GetCachedBone(i), &s_Backup.m_Bones[i], sizeof(SDK::matrix3x4));
	}
}