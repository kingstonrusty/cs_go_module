#ifndef __HEADER_BONECACHE__
#define __HEADER_BONECACHE__
#pragma once

namespace Features
{
	namespace Util
	{
		namespace BoneCacheHelper 
		{
			void BackupBoneCache(SDK::C_CSPlayer* pPlayer);
			void RestoreBoneCache(SDK::C_CSPlayer* pPlayer);
		}
	}
}

#endif