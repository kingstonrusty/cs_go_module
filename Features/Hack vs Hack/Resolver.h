#ifndef __HEADER_RESOLVER__
#define __HEADER_RESOLVER__
#pragma once

namespace Features
{
	namespace HackvsHack
	{
		namespace Resolver
		{
			void Run();
		}
	}
}

#endif