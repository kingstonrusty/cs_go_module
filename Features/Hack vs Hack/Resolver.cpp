#include "Resolver.h"

struct SResolverData
{
	float m_flSimulationTime;
	float m_flOldSimulationTime;
	bool m_bDeadOrDormant;

	struct
	{
		float m_flLowerBodyYaw;
		float m_flOldLowerBodyYaw;
		
		float m_flLowerBodyUpdateTime;
		float m_flOldLowerBodyUpdateTime;

		bool m_bUpdatedThisSnapshot;

		struct
		{
			bool m_bReliable;
			float m_flLowerBodyFlickTime;
		} m_Flick;

		struct
		{
			bool m_bPredictedThisSnapshot;
			int m_nNextUpdateServerTick;
			int m_nPredictedTick;
		} m_Predicted;
	} m_LowerBody;

	struct
	{
		SDK::Vector m_vecOldEyePosition;
		float m_flLowestFractionYaw;
		float m_flLowestFraction;
	} m_WallDetection;

	struct
	{
	} m_Fake;

	struct
	{
		int m_nCertainty;
		int m_nChokedPackets;
	} m_Fakelag;

	struct
	{
		float m_flFinalAngle;
	} m_Corrected;

	bool m_bCleared;
} static s_Data[MAX_PLAYERS];

static int EstimateServerTick();
static void UpdateSimulationTimes(SResolverData& Data, SDK::C_CSPlayer* pPlayer);
static void UpdateLowerBodyYaw(SResolverData& Data, SDK::C_CSPlayer* pPlayer);
static void UpdateLowerBodyYawFlick(SResolverData& Data, SDK::C_CSPlayer* pPlayer);
static void UpdateWallDetection(SResolverData& Data, SDK::C_CSPlayer* pPlayer);
static void UpdateFakelagDetection(SResolverData& Data, SDK::C_CSPlayer* pPlayer);
static void UpdateLowerBodyPrediction(SResolverData& Data, SDK::C_CSPlayer* pPlayer);
static void EstimateRealAngle(SResolverData& Data, SDK::C_CSPlayer* pPlayer);

static bool s_bOnce;

void Features::HackvsHack::Resolver::Run()
{
	Vars.Resolver.Enabled = 1;
	Vars.Resolver.LBYPrediction = 1;
	Vars.Resolver.WallDetection = 1;

	if (!s_bOnce || !Vars.Resolver.Enabled)
	{
		for (int i = 1; i <= MAX_PLAYERS; i++)
		{
			SetMemory(&s_Data[i - 1], 0, sizeof(SResolverData));
			s_Data[i - 1].m_bCleared = true;
		}

		s_bOnce = true;
	}

	SDK::C_CSPlayer* pLocal = (SDK::C_CSPlayer*)SDK::Interfaces::EntityList->GetClientEntity(SDK::Interfaces::Engine->GetLocalPlayer());

	for (int i = 1; i <= SDK::Interfaces::Globals->maxClients(); i++)
	{
		SResolverData& Data = s_Data[i - 1];
		if (i == SDK::Interfaces::Engine->GetLocalPlayer())
		{
			if (!Data.m_bCleared)
			{
				SetMemory(&Data, 0, sizeof(Data));
				Data.m_flSimulationTime = -1.f;
				Data.m_flOldSimulationTime = -1.f;
				Data.m_bCleared = true;
			}

			continue;
		}

		SDK::C_CSPlayer* pPlayer = (SDK::C_CSPlayer*)SDK::Interfaces::EntityList->GetClientEntity(i);
		if (pPlayer == nullptr || pPlayer->GetTeamNum() == pLocal->GetTeamNum())
		{
			if (!Data.m_bCleared)
			{
				SetMemory(&Data, 0, sizeof(Data));
				Data.m_flSimulationTime = -1.f;
				Data.m_flOldSimulationTime = -1.f;
				Data.m_bCleared = true;
			}

			continue;
		}

		if (pPlayer->GetSimulationTime() < Data.m_flSimulationTime)
		{
			SetMemory(&Data, 0, sizeof(Data));
			Data.m_flSimulationTime = -1.f;
			Data.m_flOldSimulationTime = -1.f;
		}

		Data.m_bCleared = false;

		if (!pPlayer->IsAlive() || pPlayer->GetNetworkable()->IsDormant())
		{
			Data.m_bDeadOrDormant = true;

			continue;
		}

		if (Data.m_flSimulationTime == pPlayer->GetSimulationTime())
		{
			continue;
		}

		UpdateSimulationTimes(Data, pPlayer);
		UpdateLowerBodyYaw(Data, pPlayer);
		UpdateLowerBodyYawFlick(Data, pPlayer);
		UpdateWallDetection(Data, pPlayer);
		UpdateFakelagDetection(Data, pPlayer);
		UpdateLowerBodyPrediction(Data, pPlayer);

		EstimateRealAngle(Data, pPlayer);

		pPlayer->GetEyeAngles().y = Data.m_Corrected.m_flFinalAngle;

		Data.m_bDeadOrDormant = false;
	}
}

void UpdateSimulationTimes(SResolverData& Data, SDK::C_CSPlayer* pPlayer)
{
	Data.m_flOldSimulationTime = Data.m_flSimulationTime;
	Data.m_flSimulationTime = pPlayer->GetSimulationTime();
}

void UpdateLowerBodyYaw(SResolverData& Data, SDK::C_CSPlayer* pPlayer)
{
	Data.m_LowerBody.m_bUpdatedThisSnapshot = false;

	if (Data.m_LowerBody.m_flLowerBodyYaw != pPlayer->GetLowerBodyYaw())
	{
		Data.m_LowerBody.m_flOldLowerBodyYaw = Data.m_LowerBody.m_flLowerBodyYaw;
		Data.m_LowerBody.m_flLowerBodyYaw = pPlayer->GetLowerBodyYaw();

		Data.m_LowerBody.m_flOldLowerBodyUpdateTime = Data.m_LowerBody.m_flLowerBodyUpdateTime;
		Data.m_LowerBody.m_flLowerBodyUpdateTime = Data.m_flSimulationTime;

		Data.m_LowerBody.m_bUpdatedThisSnapshot = true;
	}
}

void UpdateLowerBodyYawFlick(SResolverData& Data, SDK::C_CSPlayer* pPlayer)
{
	if (Data.m_LowerBody.m_Flick.m_flLowerBodyFlickTime == Data.m_flSimulationTime)
	{
		return;
	}

	SDK::CAnimationLayer* pLayer = pPlayer->GetAnimOverlay(3);
	if (pLayer == nullptr)
	{
		return;
	}

	if (pPlayer->GetSequenceActivity(pLayer->m_nSequence()) != 979)
	{
		Data.m_LowerBody.m_Flick.m_bReliable = false;
		return;
	}

	if (pLayer->m_flCycle() != 0.0f || pLayer->m_flWeight() != 0.0f || (Data.m_flSimulationTime - Data.m_LowerBody.m_Flick.m_flLowerBodyFlickTime) <= 0.22f)
	{
		return;
	}

	Data.m_LowerBody.m_Flick.m_flLowerBodyFlickTime = Data.m_flSimulationTime;
	Data.m_LowerBody.m_Flick.m_bReliable = true;
}

void UpdateWallDetection(SResolverData& Data, SDK::C_CSPlayer* pPlayer)
{
	if (!Vars.Resolver.WallDetection)
	{
		return;
	}

	SDK::Vector vecOffset = SDK::Vector(0.f, 0.f, (pPlayer->GetFlags() & 2) ? 46.f : 64.f);

	if ((pPlayer->GetLocalOrigin() + vecOffset) == Data.m_WallDetection.m_vecOldEyePosition) // save s0me fps here
	{
		return;
	}

	Data.m_WallDetection.m_flLowestFraction = 1.f;

	SDK::Ray_t ray;
	SDK::trace_t trace;
	SDK::CTraceFilterSkipPlayers filter = SDK::CTraceFilterSkipPlayers();
	SDK::Vector vAng = SDK::Vector(10.f, 0.f, 0.f);
	SDK::Vector vEyePos = pPlayer->GetLocalOrigin() + vecOffset;
	float flStep = (Vars.Resolver.WallDetection == 1) ? 15.f : (Vars.Resolver.WallDetection == 2 ? 10.f : 5.f);

	for (float flYaw = 0.f; flYaw < 360.f; flYaw += flStep)
	{
		vAng.y = flYaw;

		ray.Init(vEyePos, vEyePos + vAng.Forward() * 35.f);
		SDK::Interfaces::EngineTrace->TraceRay(ray, SDK::MASK_SOLID, &filter, &trace);
	
		if (trace.fraction < Data.m_WallDetection.m_flLowestFraction)
		{
			Data.m_WallDetection.m_flLowestFraction = trace.fraction;
			Data.m_WallDetection.m_flLowestFractionYaw = flYaw;
		}
	}

	Data.m_WallDetection.m_vecOldEyePosition = vEyePos;
}

void UpdateFakelagDetection(SResolverData& Data, SDK::C_CSPlayer* pPlayer)
{
	if (Data.m_bDeadOrDormant)
	{
		Data.m_Fakelag.m_nCertainty = 0;
		return;
	}

	Data.m_Fakelag.m_nChokedPackets = SDK::TIME_TO_TICKS(Data.m_flSimulationTime - Data.m_flOldSimulationTime) - 1;
	if (Data.m_Fakelag.m_nChokedPackets <= 1)
	{
		Data.m_Fakelag.m_nCertainty = 0;
		return;
	}

	Data.m_Fakelag.m_nCertainty = 30;

	if (Data.m_Fakelag.m_nChokedPackets >= 12)
	{
		Data.m_Fakelag.m_nChokedPackets = 12;
		Data.m_Fakelag.m_nCertainty = 100;
	}
	else if (Data.m_Fakelag.m_nChokedPackets >= 8)
	{
		Data.m_Fakelag.m_nCertainty = 100;
	}
	else if (Data.m_Fakelag.m_nChokedPackets >= 6)
	{
		Data.m_Fakelag.m_nCertainty = 80;
	}
	else if (Data.m_Fakelag.m_nChokedPackets >= 4)
	{
		Data.m_Fakelag.m_nCertainty = 60;
	}
}

int EstimateServerTick()
{
	int nServerTick = SDK::Interfaces::Engine->GetServerTick();

	if (SDK::Interfaces::Engine->GetNetChannelInfo())
	{
		nServerTick += SDK::TIME_TO_TICKS(SDK::Interfaces::Engine->GetNetChannelInfo()->GetLatency(SDK::FLOW_OUTGOING) + SDK::Interfaces::Engine->GetNetChannelInfo()->GetLatency(SDK::FLOW_INCOMING));
	}

	return nServerTick;
}

void UpdateLowerBodyPrediction(SResolverData& Data, SDK::C_CSPlayer* pPlayer)
{
	if (!Vars.Resolver.LBYPrediction)
	{
		return;
	}

	if (Data.m_bDeadOrDormant)
	{
		return;
	}

	if (Data.m_LowerBody.m_bUpdatedThisSnapshot)
	{
		if(pPlayer->GetFlags() & 1)
		{ 
			if (pPlayer->GetVelocity().Length() < 35.f)
			{
				Data.m_LowerBody.m_Predicted.m_nNextUpdateServerTick = SDK::TIME_TO_TICKS(Data.m_flSimulationTime) + SDK::TIME_TO_TICKS(1.1f) - Data.m_Fakelag.m_nChokedPackets;
			}
			else
			{
				Data.m_LowerBody.m_Predicted.m_nNextUpdateServerTick = SDK::TIME_TO_TICKS(Data.m_flSimulationTime) + SDK::TIME_TO_TICKS(0.22f) - Data.m_Fakelag.m_nChokedPackets;
			}
		}
		else
		{
			Data.m_LowerBody.m_Predicted.m_nNextUpdateServerTick = SDK::TIME_TO_TICKS(Data.m_flSimulationTime) + SDK::TIME_TO_TICKS(1000.f);
		}
	}

	if (EstimateServerTick() >= Data.m_LowerBody.m_Predicted.m_nNextUpdateServerTick)
	{
		Data.m_LowerBody.m_Predicted.m_bPredictedThisSnapshot = true;
		Data.m_LowerBody.m_Predicted.m_nPredictedTick = EstimateServerTick();

		if (pPlayer->GetFlags() & 1)
		{
			if (pPlayer->GetVelocity().Length() < 35.f)
			{
				Data.m_LowerBody.m_Predicted.m_nNextUpdateServerTick = SDK::TIME_TO_TICKS(Data.m_flSimulationTime) + SDK::TIME_TO_TICKS(1.1f) - Data.m_Fakelag.m_nChokedPackets;
			}
			else
			{
				Data.m_LowerBody.m_Predicted.m_nNextUpdateServerTick = SDK::TIME_TO_TICKS(Data.m_flSimulationTime) + SDK::TIME_TO_TICKS(0.22f) - Data.m_Fakelag.m_nChokedPackets;
			}
		}
		else
		{
			Data.m_LowerBody.m_Predicted.m_nNextUpdateServerTick = SDK::TIME_TO_TICKS(Data.m_flSimulationTime) + SDK::TIME_TO_TICKS(1000.f);
		}
	}
}

void EstimateRealAngle(SResolverData& Data, SDK::C_CSPlayer* pPlayer)
{
	if (Data.m_Fakelag.m_nChokedPackets == 0)
	{
		Data.m_Corrected.m_flFinalAngle = pPlayer->GetEyeAngles().y;

		return;
	}

	if (Data.m_LowerBody.m_Flick.m_bReliable)
	{
		Data.m_Corrected.m_flFinalAngle = Data.m_LowerBody.m_flLowerBodyYaw - 150.f + (float)SDK::RandomInt(-2, 2) * 15.f;
	}
	/*else if (Data.m_LowerBody.m_Predicted.m_bPredictedThisSnapshot)
	{
		Data.m_Corrected.m_flFinalAngle = Data.m_LowerBody.m_flLowerBodyYaw;
	}*/
	else if (Data.m_WallDetection.m_flLowestFraction != 1.f)
	{
		Data.m_Corrected.m_flFinalAngle = Data.m_WallDetection.m_flLowestFractionYaw + (float)SDK::RandomInt(-1, 1) * 90.f;
	}
	else
	{
		Data.m_Corrected.m_flFinalAngle = Data.m_LowerBody.m_flLowerBodyYaw - 180.f + (float)SDK::RandomInt(-4, 4) * 20.f;
	}
}