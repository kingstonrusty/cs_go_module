#ifndef __HEADER_DRAWTABSELECTORMAIN__
#define __HEADER_DRAWTABSELECTORMAIN__
#pragma once

#include "Colors.h"
#include "Menu.h"

namespace Menu
{
	void DrawTabSelectorMain(int x, int y, int h, int& tab, int nTabs, ...);
}

#endif