#ifndef __HEADER_MENU__
#define __HEADER_MENU__
#pragma once

#include "Frame.h"
#include "TabSelectorMain.h"
#include "Area.h"
#include "Checkbox.h"
#include "Dropdown.h"
#include "IncrDecrBox.h"
#include "TabSelector.h"
#include "TextBox.h"
#include "Button.h"
#include "PlayerList.h"

#define DrawDropdown(x, y, w, val, n, ...) \
{ \
static bool s_bSelected; \
Menu::_DrawDropdown(x, y, w, val, s_bSelected, n, __VA_ARGS__); \
} \

namespace Menu
{
	void Draw();
	bool GetShouldDraw();
	void SetShouldDraw(bool bDrawMenu);
	bool CursorInBox(int x, int y, int w, int h);
	void DrawText(SDK::Color col, SDK::HFont font, int x, int y, bool bCenter, const wchar_t* pwszText);
}

#endif