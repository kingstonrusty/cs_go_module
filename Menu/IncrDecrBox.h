#ifndef __HEADER_INCRDECRBOX__
#define __HEADER_INCRDECRBOX__
#pragma once

#include "Colors.h"
#include "Menu.h"

namespace Menu
{
	void _DrawIncrDecrBox(int x, int y, int w, INCRDECR& value, int min, int max, bool& bTyping, wchar_t* pwszBuf);
}

#define DrawIncrDecrBox(x, y, w, value, min, max) \
{ \
static wchar_t s_wszBuf[12]; \
static bool s_bTyping; \
Menu::_DrawIncrDecrBox(x, y, w, value, min, max, s_bTyping, s_wszBuf); \
}

#endif