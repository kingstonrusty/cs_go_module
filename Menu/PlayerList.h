#ifndef __HEADER_PLAYERLIST__
#define __HEADER_PLAYERLIST__
#pragma once

#include "Colors.h"
#include "Menu.h"

namespace Menu
{
	struct SPlayerListData
	{
		CHECKBOX Whitelisted;
		DROPDOWN PitchOverride;
		DROPDOWN YawOverride;
		DROPDOWN AimspotOverride;
	};

	SPlayerListData& GetDataForEntity(int idx);
	void DrawPlayerList(int x, int y);
}

#endif