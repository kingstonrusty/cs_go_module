#ifndef __HEADER_MENUCOLORS__
#define __HEADER_MENUCOLORS__
#pragma once

namespace Menu
{
	static SDK::Color FrameBackground = SDK::Color(60, 60, 60);
	static SDK::Color FrameOutline = SDK::Color(0, 0, 0);
	static SDK::Color FrameSeperationLine = SDK::Color(45, 45, 45);
	static SDK::Color FrameTitle = SDK::Color(180, 0, 0);

	static SDK::Color TabSelectorMainOutline = SDK::Color(110, 110, 110);
	static SDK::Color TabSelectorMainOutlineHovered = SDK::Color(180, 0, 0);
	static SDK::Color TabSelectorMainFill = SDK::Color(40, 40, 40);
	static SDK::Color TabSelectorMainFillSelected = SDK::Color(45, 45, 45);
	static SDK::Color TabSelectorMainText = SDK::Color(180, 0, 0);

	static SDK::Color AreaOutline = SDK::Color(110, 110, 110);
	static SDK::Color AreaFill = SDK::Color(70, 70, 70);
	static SDK::Color AreaText = SDK::Color(255, 255, 255);

	static SDK::Color CheckboxOutline = SDK::Color(110, 110, 110);
	static SDK::Color CheckboxOutlineHovered = SDK::Color(180, 0, 0);
	static SDK::Color CheckboxChecked = SDK::Color(175, 0, 0);
	static SDK::Color CheckboxCheckedOuter = SDK::Color(150, 0, 0);
	static SDK::Color CheckboxFilled = SDK::Color(40, 40, 40);

	static SDK::Color DropdownOutline = SDK::Color(110, 110, 110);
	static SDK::Color DropdownOutlineHovered = SDK::Color(180, 0, 0);
	static SDK::Color DropdownFill = SDK::Color(40, 40, 40);
	static SDK::Color DropdownText = SDK::Color(255, 255, 255);

	static SDK::Color IncrDecrOutline = SDK::Color(110, 110, 110);
	static SDK::Color IncrDecrOutlineHovered = SDK::Color(180, 0, 0);
	static SDK::Color IncrDecrFill = SDK::Color(40, 40, 40);
	static SDK::Color IncrDecrText = SDK::Color(255, 255, 255);

	static SDK::Color TabSelectorOutline = SDK::Color(110, 110, 110);
	static SDK::Color TabSelectorOutlineHovered = SDK::Color(180, 0, 0);
	static SDK::Color TabSelectorFill = SDK::Color(40, 40, 40);
	static SDK::Color TabSelectorFillSelected = SDK::Color(45, 45, 45);
	static SDK::Color TabSelectorText = SDK::Color(180, 0, 0);
	static SDK::Color TabSelectorAreaFill = SDK::Color(70, 70, 70);

	static SDK::Color TextBoxOutline = SDK::Color(110, 110, 110);
	static SDK::Color TextBoxHovered = SDK::Color(180, 0, 0);
	static SDK::Color TextBoxFill = SDK::Color(40, 40, 40);
	static SDK::Color TextBoxText = SDK::Color(255, 255, 255);

	static SDK::Color ButtonOutline = SDK::Color(110, 110, 110);
	static SDK::Color ButtonOutlineHovered = SDK::Color(180, 0, 0);
	static SDK::Color ButtonFill = SDK::Color(40, 40, 40);
	static SDK::Color ButtonText = SDK::Color(255, 255, 255);

	static SDK::Color PlayerListOutline = SDK::Color(110, 110, 110);
	static SDK::Color PlayerListFill1 = SDK::Color(70, 70, 70);
	static SDK::Color PlayerListFill2 = SDK::Color(50, 50, 50);
	static SDK::Color PlayerListText = SDK::Color(255, 255, 255);
	static SDK::Color PlayerListScrollbarFill = SDK::Color(60, 60, 60);
	static SDK::Color PlayerListScrollbarFill2 = SDK::Color(100, 100, 100);

	static SDK::Color Cursor = SDK::Color(210, 0, 0);
	static SDK::Color CursorOutline = SDK::Color(0, 0, 0);

	static SDK::Color LabelColor = SDK::Color(255, 255, 255);
}

#endif