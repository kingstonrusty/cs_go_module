#include "Area.h"

#include "Util/DrawingHelper.h"

void Menu::DrawArea(int x, int y, int w, int h, const wchar_t* pwszName)
{
	int tw, th;

	Util::DrawingHelper::GetTextSize(tw, th, Util::DrawingHelper::Fonts::SmallFonts12SemiBold, pwszName);

	SDK::Interfaces::Surface->DrawSetColor(AreaOutline);

	SDK::Interfaces::Surface->DrawFilledRect(x + 1, y + 1, x + 1 + 1, y + 1 + 1);
	SDK::Interfaces::Surface->DrawFilledRect(x + w - 2, y + 1, x + w - 2 + 1, y + 1 + 1);
	SDK::Interfaces::Surface->DrawFilledRect(x + 2, y, x + 7, y + 1);
	SDK::Interfaces::Surface->DrawFilledRect(x + 7 + 5 + tw + 5, y, x + w - 2, y + 1);

	SDK::Interfaces::Surface->DrawFilledRect(x, y + 2, x + 1, y + h - 2);
	SDK::Interfaces::Surface->DrawFilledRect(x + w - 1, y + 2, x + w - 1 + 1, y + h - 2);

	SDK::Interfaces::Surface->DrawFilledRect(x, y + 2, x + 1, y + h - 2);

	SDK::Interfaces::Surface->DrawFilledRect(x + 1, y + h - 2, x + 1 + 1, y + h - 2 + 1);
	SDK::Interfaces::Surface->DrawFilledRect(x + w - 2, y + h - 2, x + w - 2 + 1, y + h - 2 + 1);
	SDK::Interfaces::Surface->DrawFilledRect(x + 2, y + h - 1, x + w - 2, y + h - 1 + 1);

	SDK::Interfaces::Surface->DrawSetColor(AreaFill);

	SDK::Interfaces::Surface->DrawFilledRect(x + 2, y + 1, x + w - 2, y + 1 + 1);
	SDK::Interfaces::Surface->DrawFilledRect(x + 1, y + 2, x + w - 1, y + h - 3 + 1);
	SDK::Interfaces::Surface->DrawFilledRect(x + 2, y + h - 2, x + w - 2, y + h - 2 + 1);

	SDK::Interfaces::Surface->DrawSetTextFont(Util::DrawingHelper::Fonts::SmallFonts12SemiBold);
	SDK::Interfaces::Surface->DrawSetTextPos(x + 7 + 5, y - th / 2);
	SDK::Interfaces::Surface->DrawSetTextColor(AreaText);
	SDK::Interfaces::Surface->DrawPrintText(pwszName, WStringLength(pwszName));
}