#include "PlayerList.h"

#include "Util/DrawingHelper.h"

static Menu::SPlayerListData s_PlayerData[MAX_PLAYERS];

Menu::SPlayerListData& Menu::GetDataForEntity(int idx)
{
	return s_PlayerData[idx];
}

static SDK::player_info_s info[MAX_PLAYERS];

void Menu::DrawPlayerList(int x, int y)
{
	int tw, th;
	static int s_Pos;
	static unsigned int s_Selected;
	SPlayerListData Dummy;
	SPlayerListData* pSelected = &Dummy;
	SDK::player_info_s* pSelectedInfo = nullptr;
	int iPlayers[MAX_PLAYERS];
	SetMemory(iPlayers, 0, sizeof(iPlayers));
	int nCount = 0;

	if (SDK::Interfaces::Engine->IsInGame())
	{
		for (int i = 1; i <= SDK::Interfaces::Globals->maxClients(); i++)
		{
			if (i == SDK::Interfaces::Engine->GetLocalPlayer())
			{
				continue;
			}

			if (!SDK::Interfaces::Engine->GetPlayerInfo(i, &info[nCount]))
			{
				continue;
			}

			iPlayers[nCount] = i;
			nCount++;
		}
	}

	if (Menu::CursorInBox(x, y, 535, 180) && Util::Input::GetKeyState(SDK::MOUSE_WHEEL_UP) & 1)
	{
		s_Pos -= 1;
	}

	if (Menu::CursorInBox(x, y, 535, 180) && Util::Input::GetKeyState(SDK::MOUSE_WHEEL_DOWN) & 1)
	{
		s_Pos += 1;
	}

	if (nCount == 0)
	{
		s_Pos = -1;
	}

	if (nCount > 0 && s_Pos == -1)
	{
		s_Pos = 0;
	}

	if (nCount <= 9)
	{
		s_Pos = 0;
	}

#define max(a,b)            (((a) > (b)) ? (a) : (b))
	if ((s_Pos + 9) > nCount)
	{
		s_Pos = max(nCount - 9, 0);
	}
#undef max

	if (nCount != 0 && s_Pos < 0)
	{
		s_Pos = 0;
	}

	SDK::Interfaces::Surface->DrawSetColor(PlayerListScrollbarFill);
	SDK::Interfaces::Surface->DrawFilledRect(x + 535 + 1, y, x + 535 + 1 + 5, y + 180);

	int scrollh = (int)(180.f / (((float)(nCount - 9 + 1) / 4.0f) + 0.75f));

	if ((nCount - 9) <= 0)
	{
		scrollh = 180;
	}

	if (scrollh > 180)
	{
		scrollh = 180;
	}

	if (scrollh < 10)
	{
		scrollh = 10;
	}

	int scrolly = 0;
	if ((nCount - 9) > 0 && s_Pos != 0)
	{
		int hleft = 180 - scrollh;
		int scrollper = (int)((float)hleft / (float)(nCount - 9));
		scrolly = scrollper * (s_Pos);
	}

	SDK::Interfaces::Surface->DrawSetColor(PlayerListScrollbarFill2);
	SDK::Interfaces::Surface->DrawFilledRect(x + 535 + 1, y + scrolly, x + 535 + 1 + 5, y + (((s_Pos + 9) == nCount) ? 180 : (scrolly + scrollh)));

	SDK::Interfaces::Surface->DrawSetColor(PlayerListOutline);
	SDK::Interfaces::Surface->DrawOutlinedRect(x + 535 + 1, y, x + 535 + 1 + 5, y + 180);

	for (int i = 0; i < 9; i++)
	{
		SDK::Interfaces::Surface->DrawSetColor(((i % 2) == 1) ? PlayerListFill1 : PlayerListFill2);
		SDK::Interfaces::Surface->DrawFilledRect(x, y + i * 20, x + 535, y + i * 20 + 20);
	}

	SDK::Interfaces::Surface->DrawSetColor(PlayerListOutline);
	SDK::Interfaces::Surface->DrawOutlinedRect(x, y, x + 535, y + 180);

	bool bFound = false;

	if (nCount != 0)
	{
		SDK::Interfaces::Surface->DrawSetTextFont(Util::DrawingHelper::Fonts::SmallFonts12SemiBold);
		SDK::Interfaces::Surface->DrawSetTextColor(PlayerListText);

		#define min(a,b)            (((a) < (b)) ? (a) : (b))
		for (int i = 0; i < min(nCount, 9); i++)
		#undef min
		{
			if (!iPlayers[s_Pos + i])
			{
				break;
			}

			if (Menu::CursorInBox(x, y + i * 20 + 1, 535, 18) && Util::Input::GetKeyState(SDK::MOUSE_LEFT) & 1)
			{
				s_Selected = Util::CRC32::HashSingleBuffer(info[s_Pos + i].guid, StringLength(info[s_Pos + i].guid));
				Util::Input::SetKeyState(SDK::MOUSE_LEFT, 0);
			}

			if (s_Selected == Util::CRC32::HashSingleBuffer(info[s_Pos + i].guid, StringLength(info[s_Pos + i].guid)))
			{
				SDK::Interfaces::Surface->DrawSetColor(SDK::Color(150, 0, 0));
				SDK::Interfaces::Surface->DrawFilledRect(x, y + i * 20, x + 535, y + i * 20 + 20);
			}
			else if (Menu::CursorInBox(x, y + i * 20 + 1, 535, 18))
			{
				SDK::Interfaces::Surface->DrawSetColor(SDK::Color(100, 0, 0));
				SDK::Interfaces::Surface->DrawFilledRect(x, y + i * 20, x + 535, y + i * 20 + 20);
			}

			wchar_t wszName[128];
			MultiByteToWideChar(65001/*CP_UTF8*/, 0, info[s_Pos + i].name, -1, wszName, 128);

			SDK::Interfaces::Surface->GetTextSize(Util::DrawingHelper::Fonts::SmallFonts12SemiBold, wszName, tw, th);
			SDK::Interfaces::Surface->DrawSetTextPos(x + 5, y + i * 20 + 20 / 2 - th / 2);
			SDK::Interfaces::Surface->DrawPrintText(wszName, WStringLength(wszName));
		}

		SDK::Interfaces::Surface->DrawSetColor(PlayerListOutline);
		SDK::Interfaces::Surface->DrawOutlinedRect(x, y, x + 535, y + 180);

		if (!bFound)
		{
			for (int i = 0; i < nCount; i++)
			{
				if (s_Selected == Util::CRC32::HashSingleBuffer(info[i].guid, StringLength(info[i].guid)))
				{
					bFound = true;
					pSelectedInfo = &info[i];
					pSelected = &s_PlayerData[iPlayers[i]];
					break;
				}
			}
		}
	}

	wchar_t wszBuf[128 + 33 + 12];
	CopyMemory(wszBuf, WXorStr(L"Options"), 16);

	if (pSelectedInfo)
	{
		wchar_t wszName[128];
		MultiByteToWideChar(65001/*CP_UTF8*/, 0, pSelectedInfo->name, -1, wszName, 128);

		wchar_t wszSteamID[32 + 1];
		MultiByteToWideChar(0/*CP_ACP*/, 0, pSelectedInfo->guid, -1, wszSteamID, 33);

		if (WStringLength(wszName) > 24)
		{
			FormatWString(wszBuf, 128 + 33 + 12, WXorStr(L"Options - %.24s... (%s)"), wszName, wszSteamID);
		}
		else
		{
			FormatWString(wszBuf, 128 + 33 + 12, WXorStr(L"Options - %s (%s)"), wszName, wszSteamID);
		}
	}

	DrawArea(x, y + 180 + 10, 541, 182, wszBuf);
	{
		int _x = x + 5;
		int _y = y + 180 + 10 + 10;

		DrawText(LabelColor, Util::DrawingHelper::Fonts::SmallFonts12Medium, _x, _y + 15 / 2, true, WXorStr(L"Whitelist"));
		DrawCheckbox(_x + 95, _y + 2, pSelected->Whitelisted);

		_y += 20;

		DrawText(LabelColor, Util::DrawingHelper::Fonts::SmallFonts12Medium, _x, _y + 15 / 2, true, WXorStr(L"Aimspot Override"));
		DrawDropdown(_x + 95, _y, 165, pSelected->AimspotOverride, 8, WXorStr(L"Off"), WXorStr(L"Head"), WXorStr(L"Chest"), WXorStr(L"Stomach"), WXorStr(L"Right Arm"), WXorStr(L"Left Arm"), WXorStr(L"Left Leg"), WXorStr(L"Right Leg"));

		_y += 20;

		DrawText(LabelColor, Util::DrawingHelper::Fonts::SmallFonts12Medium, _x, _y + 15 / 2, true, WXorStr(L"Pitch Override"));
		DrawDropdown(_x + 95, _y, 165, pSelected->PitchOverride, 4, WXorStr(L"Off"), WXorStr(L"Down"), WXorStr(L"Up"), WXorStr(L"0"));

		_y += 20;

		DrawText(LabelColor, Util::DrawingHelper::Fonts::SmallFonts12Medium, _x, _y + 15 / 2, true, WXorStr(L"Yaw Override"));
		DrawDropdown(_x + 95, _y, 165, pSelected->YawOverride, 4, WXorStr(L"Off"), WXorStr(L"Left"), WXorStr(L"Right"), WXorStr(L"Inverse"));

		_x = x + 5 + 260 + 10;
		_y = y + 180 + 10 + 10 + 20;

		DrawButton(_x, _y, 260, WXorStr(L"Steal Clantag"), (pSelected == &Dummy) ? nullptr : nullptr);

		_y += 20;

		DrawButton(_x, _y, 260, WXorStr(L"Steal Name"), (pSelected == &Dummy) ? nullptr : nullptr);

		_y += 20;

		DrawButton(_x, _y, 260, WXorStr(L"Copy SteamID"), nullptr);
	}

	SetMemory(info, 0, sizeof(info));
}