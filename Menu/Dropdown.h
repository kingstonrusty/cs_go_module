#ifndef __HEADER_DROPDOWN__
#define __HEADER_DROPDOWN__
#pragma once

#include "Colors.h"
#include "Menu.h"

namespace Menu
{
	void _DrawDropdown(int x, int y, int w, DROPDOWN& value, bool& bSelecting, int nOptions, ...);
	bool IsDropdownActive();
	void AddDropdownCallback(int x, int y, int w, int nOptions, va_list list);
	void DrawDropdownCallback();
}

#endif