#ifndef __HEADER_BUTTON__
#define __HEADER_BUTTON__
#pragma once

#include "Colors.h"
#include "Menu.h"

namespace Menu
{
	void DrawButton(int x, int y, int w, const wchar_t* pszTitle, void* pCallback);
}

#endif