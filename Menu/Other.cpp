#include "Menu/Menu.h"

#include "Util/DrawingHelper.h"

void DrawOther(int x, int y)
{
	using namespace Menu;

	static int s_iTab;
	DrawTabSelector(x, y, 541, 372, s_iTab, 5, WXorStr(L"Configs"), WXorStr(L"Binds"), WXorStr(L"Clan"), WXorStr(L"Anti-Aim"), WXorStr(L"Resolver"));

	int _x = x + 5;
	int _y = y + 30;

	if (s_iTab == 0) // Configs
	{
		DrawText(LabelColor, Util::DrawingHelper::Fonts::SmallFonts12Medium, _x, _y + 15 / 2, true, WXorStr(L"Config File"));
		DrawTextBox(_x + 95, _y, 150, Vars.Temp.ConfigFile, 16, false);

		_y += 35;

		DrawButton(_x, _y, 120, WXorStr(L"Load"), nullptr);
		DrawButton(_x + 120 + 5, _y, 120, WXorStr(L"Save"), nullptr);

		_y += 20;

		DrawButton(_x, _y, 245, WXorStr(L"Reset"), nullptr);
	}
	else if (s_iTab == 2) // Clans
	{
		DrawText(LabelColor, Util::DrawingHelper::Fonts::SmallFonts12Medium, _x, _y + 15 / 2, true, WXorStr(L"Enabled"));
		DrawCheckbox(_x + 95, _y + 2, Vars.Misc.ClantagChanger.Enabled);

		_y += 20;

		DrawText(LabelColor, Util::DrawingHelper::Fonts::SmallFonts12Medium, _x, _y + 15 / 2, true, WXorStr(L"Delay (ms)"));
		DrawIncrDecrBox(_x + 95, _y, 150, Vars.Misc.ClantagChanger.Delay, 0, 2000);

		_y += 20;

		DrawText(LabelColor, Util::DrawingHelper::Fonts::SmallFonts12Medium, _x, _y + 15 / 2 + 1, true, WXorStr(L"Clantag 1"));
		DrawTextBox(_x + 95, _y, 150, Vars.Misc.ClantagChanger.Clantag1, 12, false);

		_y += 20;

		DrawText(LabelColor, Util::DrawingHelper::Fonts::SmallFonts12Medium, _x, _y + 15 / 2 + 1, true, WXorStr(L"Clantag 2"));
		DrawTextBox(_x + 95, _y, 150, Vars.Misc.ClantagChanger.Clantag2, 12, false);

		_y += 20;

		DrawText(LabelColor, Util::DrawingHelper::Fonts::SmallFonts12Medium, _x, _y + 15 / 2, true, WXorStr(L"Clantag 3"));
		DrawTextBox(_x + 95, _y, 150, Vars.Misc.ClantagChanger.Clantag3, 12, false);

		_y += 20;

		DrawText(LabelColor, Util::DrawingHelper::Fonts::SmallFonts12Medium, _x, _y + 15 / 2, true, WXorStr(L"Clantag 4"));
		DrawTextBox(_x + 95, _y, 150, Vars.Misc.ClantagChanger.Clantag4, 12, false);

		_y += 20;

		DrawText(LabelColor, Util::DrawingHelper::Fonts::SmallFonts12Medium, _x, _y + 15 / 2, true, WXorStr(L"Clantag 5"));
		DrawTextBox(_x + 95, _y, 150, Vars.Misc.ClantagChanger.Clantag5, 12, false);

		_y += 20;

		DrawText(LabelColor, Util::DrawingHelper::Fonts::SmallFonts12Medium, _x, _y + 15 / 2, true, WXorStr(L"Clantag 6"));
		DrawTextBox(_x + 95, _y + 2, 150, Vars.Misc.ClantagChanger.Clantag6, 12, false);
	}
}