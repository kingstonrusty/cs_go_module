#include "Frame.h"

#include "Util/DrawingHelper.h"

struct SFrameData
{
	bool m_Dragging;
	bool m_SavePos;
	int m_SaveX;
	int m_SaveY;
};

static SFrameData s_Frames[Menu::MENU_MAXFRAMES];

static void DragFrame(int& x, int& y, int w, int frameindex)
{
	SFrameData& Data = s_Frames[frameindex];

	if (!Util::Input::GetKeyState(SDK::MOUSE_LEFT))
	{
		Data.m_Dragging = false;
		Data.m_SavePos = false;
		return;
	}

	if (!Menu::CursorInBox(x, y, w, 20) && !Data.m_Dragging)
	{
		Data.m_SavePos = false;
		return;
	}

	int mx, my;
	SDK::Interfaces::Surface->SurfaceGetCursorPos(mx, my);

	if (!Data.m_SavePos)
	{
		Data.m_SavePos = true;
		Data.m_Dragging = true;
		Data.m_SaveX = mx - x;
		Data.m_SaveY = my - y;
	}

	x = mx - Data.m_SaveX;
	y = my - Data.m_SaveY;
}

void Menu::DrawFrame(int& x, int& y, int w, int h, int frameindex, const wchar_t* pwszTitle)
{
	if (Menu::GetShouldDraw())
	{
		DragFrame(x, y, w, frameindex);
	}

	SFrameData& Data = s_Frames[frameindex];

	Util::DrawingHelper::DrawFilledRect(x, y + 20, w, h - 20, FrameBackground);
	Util::DrawingHelper::DrawOutlinedRect(x, y + 20, w, h - 20, FrameOutline);
	Util::DrawingHelper::DrawFilledRect(x, y, w - 20, 20, FrameBackground);

	Util::DrawingHelper::DrawLine(x, y, x, y + 21, FrameOutline);
	Util::DrawingHelper::DrawLine(x, y, x + w - 20, y, FrameOutline);

	for (int i = 0; i < 21; i++)
	{
		Util::DrawingHelper::DrawLine(x + w - 20, y + i, x + w - 20 + i, y + i, FrameBackground);
	}

	for (int i = 0; i < 21; i++)
	{
		Util::DrawingHelper::DrawFilledRect(x + i + w - 21, y + i, 1, 1, FrameOutline);
	}

	Util::DrawingHelper::DrawLine(x + 1, y + 20, x + w - 1, y + 20, FrameSeperationLine);

	int tw, th;
	Util::DrawingHelper::GetTextSize(tw, th, Util::DrawingHelper::Fonts::SmallFonts14SemiBold, pwszTitle);
	Util::DrawingHelper::DrawText(Util::DrawingHelper::Fonts::SmallFonts14SemiBold, x + w / 2 - tw / 2, y + 10 - th / 2, pwszTitle, FrameTitle);
}