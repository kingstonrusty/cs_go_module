#include "TextBox.h"

#include "Util/DrawingHelper.h"

#define CHECKKEY(key, c) \
if((int)WStringLength(pwszBuf) < maxlen && Util::Input::GetKeyState(key) & 1) \
{ \
pwszBuf[WStringLength(pwszBuf)] = c; \
}

static void ProcessTyping(wchar_t* pwszBuf, bool bInBox, bool bClick, int maxlen, bool bForceTyping, wchar_t* pwszBackup, bool& bTyping)
{
	if (bForceTyping)
	{
		bTyping = true;
	}

	if (bClick && bInBox)
	{
		bTyping = true;
		Util::Input::SetKeyState(SDK::MOUSE_LEFT, 0);

		for (int i = 0; i < maxlen; i++)
		{
			pwszBackup[i] = pwszBuf[i];
			pwszBuf[i] = '\0';
		}
	}

	if (!bTyping)
	{
		return;
	}

	if (!bInBox && bClick)
	{
		bTyping = false;
		Util::Input::SetKeyState(SDK::MOUSE_LEFT, 0);

		for (int i = 0; i < maxlen; i++)
		{
			pwszBuf[i] = pwszBackup[i];
		}

		return;
	}

	if (Util::Input::GetKeyState(SDK::KEY_BACKSPACE) & 1 && WStringLength(pwszBuf) > 0)
	{
		pwszBuf[WStringLength(pwszBuf) - 1] = '\0';
	}

	CHECKKEY(SDK::KEY_SPACE, ' ');

	CHECKKEY(SDK::KEY_0, '0');
	CHECKKEY(SDK::KEY_1, '1');
	CHECKKEY(SDK::KEY_2, '2');
	CHECKKEY(SDK::KEY_3, '3');
	CHECKKEY(SDK::KEY_4, '4');
	CHECKKEY(SDK::KEY_5, '5');
	CHECKKEY(SDK::KEY_6, '6');
	CHECKKEY(SDK::KEY_7, '7');
	CHECKKEY(SDK::KEY_8, '8');
	CHECKKEY(SDK::KEY_9, '9');

	CHECKKEY(SDK::KEY_PAD_0, '0');
	CHECKKEY(SDK::KEY_PAD_1, '1');
	CHECKKEY(SDK::KEY_PAD_2, '2');
	CHECKKEY(SDK::KEY_PAD_3, '3');
	CHECKKEY(SDK::KEY_PAD_4, '4');
	CHECKKEY(SDK::KEY_PAD_5, '5');
	CHECKKEY(SDK::KEY_PAD_6, '6');
	CHECKKEY(SDK::KEY_PAD_7, '7');
	CHECKKEY(SDK::KEY_PAD_8, '8');
	CHECKKEY(SDK::KEY_PAD_9, '9');

	CHECKKEY(SDK::KEY_COMMA, ',');
	CHECKKEY(SDK::KEY_PERIOD, '.');

	CHECKKEY(SDK::KEY_MINUS, '-');
	CHECKKEY(SDK::KEY_PAD_MINUS, '-');

	if (Util::Input::GetKeyState(SDK::KEY_LSHIFT) || Util::Input::GetKeyState(SDK::KEY_RSHIFT))
	{
		CHECKKEY(SDK::KEY_A, 'A');
		CHECKKEY(SDK::KEY_B, 'B');
		CHECKKEY(SDK::KEY_C, 'C');
		CHECKKEY(SDK::KEY_D, 'D');
		CHECKKEY(SDK::KEY_E, 'E');
		CHECKKEY(SDK::KEY_F, 'F');
		CHECKKEY(SDK::KEY_G, 'G');
		CHECKKEY(SDK::KEY_H, 'H');
		CHECKKEY(SDK::KEY_I, 'I');
		CHECKKEY(SDK::KEY_J, 'J');
		CHECKKEY(SDK::KEY_K, 'K');
		CHECKKEY(SDK::KEY_L, 'L');
		CHECKKEY(SDK::KEY_M, 'M');
		CHECKKEY(SDK::KEY_N, 'N');
		CHECKKEY(SDK::KEY_O, 'O');
		CHECKKEY(SDK::KEY_P, 'P');
		CHECKKEY(SDK::KEY_Q, 'Q');
		CHECKKEY(SDK::KEY_R, 'R');
		CHECKKEY(SDK::KEY_S, 'S');
		CHECKKEY(SDK::KEY_T, 'T');
		CHECKKEY(SDK::KEY_U, 'U');
		CHECKKEY(SDK::KEY_V, 'V');
		CHECKKEY(SDK::KEY_W, 'W');
		CHECKKEY(SDK::KEY_X, 'X');
		CHECKKEY(SDK::KEY_Y, 'Y');
		CHECKKEY(SDK::KEY_Z, 'Z');
	}
	else
	{
		CHECKKEY(SDK::KEY_A, 'a');
		CHECKKEY(SDK::KEY_B, 'b');
		CHECKKEY(SDK::KEY_C, 'c');
		CHECKKEY(SDK::KEY_D, 'd');
		CHECKKEY(SDK::KEY_E, 'e');
		CHECKKEY(SDK::KEY_F, 'f');
		CHECKKEY(SDK::KEY_G, 'g');
		CHECKKEY(SDK::KEY_H, 'h');
		CHECKKEY(SDK::KEY_I, 'i');
		CHECKKEY(SDK::KEY_J, 'j');
		CHECKKEY(SDK::KEY_K, 'k');
		CHECKKEY(SDK::KEY_L, 'l');
		CHECKKEY(SDK::KEY_M, 'm');
		CHECKKEY(SDK::KEY_N, 'n');
		CHECKKEY(SDK::KEY_O, 'o');
		CHECKKEY(SDK::KEY_P, 'p');
		CHECKKEY(SDK::KEY_Q, 'q');
		CHECKKEY(SDK::KEY_R, 'r');
		CHECKKEY(SDK::KEY_S, 's');
		CHECKKEY(SDK::KEY_T, 't');
		CHECKKEY(SDK::KEY_U, 'u');
		CHECKKEY(SDK::KEY_V, 'v');
		CHECKKEY(SDK::KEY_W, 'w');
		CHECKKEY(SDK::KEY_X, 'x');
		CHECKKEY(SDK::KEY_Y, 'y');
		CHECKKEY(SDK::KEY_Z, 'z');
	}

	pwszBuf[WStringLength(pwszBuf)] = '\0';

	if (Util::Input::GetKeyState(SDK::KEY_ENTER) & 1 || Util::Input::GetKeyState(SDK::KEY_PAD_ENTER) & 1)
	{
		bTyping = false;
	}
}

void Menu::_DrawTextBox(int x, int y, int w, wchar_t* pwszBuffer, int maxlen, bool& bTyping, wchar_t* pwszBackup, bool bForceTyping)
{
	int tw, th;

	ProcessTyping(pwszBuffer, Menu::CursorInBox(x, y, w, 15), Util::Input::GetKeyState(SDK::MOUSE_LEFT) & 1, maxlen, bForceTyping, pwszBackup, bTyping);

	SDK::Interfaces::Surface->DrawSetColor((Menu::CursorInBox(x, y, w, 15) || bTyping) ? TextBoxHovered : TextBoxOutline);

	SDK::Interfaces::Surface->DrawFilledRect(x + 1, y + 1, x + 1 + 1, y + 1 + 1);
	SDK::Interfaces::Surface->DrawFilledRect(x + 2, y, x + w - 2, y + 1);
	SDK::Interfaces::Surface->DrawFilledRect(x + w - 2, y + 1, x + w - 2 + 1, y + 1 + 1);

	SDK::Interfaces::Surface->DrawFilledRect(x, y + 2, x + 1, y + 13);
	SDK::Interfaces::Surface->DrawFilledRect(x + w - 1, y + 2, x + w - 1 + 1, y + 13);

	SDK::Interfaces::Surface->DrawFilledRect(x, y + 2, x + 1, y + 13);

	SDK::Interfaces::Surface->DrawFilledRect(x + 1, y + 13, x + 1 + 1, y + 13 + 1);
	SDK::Interfaces::Surface->DrawFilledRect(x + w - 2, y + 13, x + w - 2 + 1, y + 13 + 1);
	SDK::Interfaces::Surface->DrawFilledRect(x + 2, y + 14, x + w - 2, y + 14 + 1);

	SDK::Interfaces::Surface->DrawSetColor(TextBoxFill);

	SDK::Interfaces::Surface->DrawFilledRect(x + 2, y + 1, x + w - 2, y + 1 + 1);
	SDK::Interfaces::Surface->DrawFilledRect(x + 1, y + 2, x + w - 1, y + 12 + 1);
	SDK::Interfaces::Surface->DrawFilledRect(x + 2, y + 13, x + w - 2, y + 13 + 1);

	if (WStringLength(pwszBuffer) > 0)
	{
		SDK::Interfaces::Surface->DrawSetTextFont(Util::DrawingHelper::Fonts::SmallFonts12Medium);
		SDK::Interfaces::Surface->GetTextSize(Util::DrawingHelper::Fonts::SmallFonts12Medium, pwszBuffer, tw, th);
		SDK::Interfaces::Surface->DrawSetTextPos(x + 5, y + 15 / 2 - th / 2);
		SDK::Interfaces::Surface->DrawSetTextColor(TextBoxText);
		SDK::Interfaces::Surface->DrawPrintText(pwszBuffer, WStringLength(pwszBuffer));
	}

	if (bTyping)
	{
		SDK::Interfaces::Surface->DrawSetColor(TextBoxOutline);
		SDK::Interfaces::Surface->DrawFilledRect(x + 5 + tw + 1, y + 3, x + 5 + tw + 1 + 1, y + 12);
	}
}