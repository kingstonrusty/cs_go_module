#include "Dropdown.h"

#include "Util/DrawingHelper.h"

extern short g_OriginalMouseState;

struct SDropdownCallback
{
	int m_X;
	int m_Y;
	int m_W;
	wchar_t m_pwszOptions[12][60];
	int m_nOptions;
} static s_Callback;

void Menu::_DrawDropdown(int x, int y, int w, DROPDOWN& value, bool& bSelecting, int nOptions, ...)
{
	if (Util::Input::GetKeyState(SDK::MOUSE_LEFT) & 1 && Menu::CursorInBox(x, y, w, 15))
	{
		bSelecting = true;
		Util::Input::SetKeyState(SDK::MOUSE_LEFT, 0);
	}

	if (bSelecting && g_OriginalMouseState & 1 && !Menu::CursorInBox(x, y, w, 15 + 15 * nOptions))
	{
		bSelecting = false;
		s_Callback.m_nOptions = 0;
	}

	if (bSelecting)
	{
		for (int i = 0; i < nOptions; i++)
		{
			if (Menu::CursorInBox(x, y + 15 + i * 15, w, 15) && g_OriginalMouseState & 1)
			{
				value = i;
				bSelecting = false;
				s_Callback.m_nOptions = 0;

				break;
			}
		}
	}

	SDK::Interfaces::Surface->DrawSetColor((Menu::CursorInBox(x, y, w, 15) || bSelecting) ? DropdownOutlineHovered : DropdownOutline);

	SDK::Interfaces::Surface->DrawFilledRect(x + 1, y + 1, x + 1 + 1, y + 1 + 1);
	SDK::Interfaces::Surface->DrawFilledRect(x + 2, y, x + w - 2, y + 1);
	SDK::Interfaces::Surface->DrawFilledRect(x + w - 2, y + 1, x + w - 2 + 1, y + 1 + 1);

	SDK::Interfaces::Surface->DrawFilledRect(x, y + 2, x + 1, y + 13);
	SDK::Interfaces::Surface->DrawFilledRect(x + w - 1, y + 2, x + w - 1 + 1, y + 13);

	SDK::Interfaces::Surface->DrawFilledRect(x, y + 2, x + 1, y + 13);

	SDK::Interfaces::Surface->DrawFilledRect(x + 1, y + 13, x + 1 + 1, y + 13 + 1);
	SDK::Interfaces::Surface->DrawFilledRect(x + w - 2, y + 13, x + w - 2 + 1, y + 13 + 1);
	SDK::Interfaces::Surface->DrawFilledRect(x + 2, y + 14, x + w - 2, y + 14 + 1);

	SDK::Interfaces::Surface->DrawSetColor(DropdownFill);

	SDK::Interfaces::Surface->DrawFilledRect(x + 2, y + 1, x + w - 2, y + 1 + 1);
	SDK::Interfaces::Surface->DrawFilledRect(x + 1, y + 2, x + w - 1, y + 12 + 1);
	SDK::Interfaces::Surface->DrawFilledRect(x + 2, y + 13, x + w - 2, y + 13 + 1);

	va_list list;
	va_start(list, nOptions);
	if (value >= nOptions || value < 0)
	{
		value = 0;
	}

	const wchar_t* pwszString = nullptr;
	for (int i = 0; i < (value + 1); i++) pwszString = va_arg(list, const wchar_t*);
	va_end(list);

	if (pwszString == nullptr)
	{
		return;
	}

	SDK::Interfaces::Surface->DrawSetTextFont(Util::DrawingHelper::Fonts::SmallFonts12Medium);

	int tw, th;
	SDK::Interfaces::Surface->GetTextSize(Util::DrawingHelper::Fonts::SmallFonts12Medium, pwszString, tw, th);

	SDK::Interfaces::Surface->DrawSetTextPos(x + 5, y + 15 / 2 - th / 2);
	SDK::Interfaces::Surface->DrawSetTextColor(DropdownText);
	SDK::Interfaces::Surface->DrawPrintText(pwszString, WStringLength(pwszString));

	if (bSelecting)
	{
		va_list list;
		va_start(list, nOptions);
		AddDropdownCallback(x, y, w, nOptions, list);
		va_end(list);
	}
}

bool Menu::IsDropdownActive()
{
	return s_Callback.m_nOptions != 0;
}

void Menu::AddDropdownCallback(int x, int y, int w, int nOptions, va_list list)
{
	s_Callback.m_X = x;
	s_Callback.m_Y = y;
	s_Callback.m_W = w;
	s_Callback.m_nOptions = nOptions;

	for (int i = 0; i < nOptions; i++)
	{
		const wchar_t* pwszString = va_arg(list, const wchar_t*);

		CopyMemory(s_Callback.m_pwszOptions[i], pwszString, (WStringLength(pwszString) + 1) * sizeof(wchar_t));
	}
}

void Menu::DrawDropdownCallback()
{
	int tw, th;
	int x = s_Callback.m_X;
	int y = s_Callback.m_Y;
	int w = s_Callback.m_W;
	int amount = s_Callback.m_nOptions;

	SDK::Interfaces::Surface->DrawSetColor(DropdownFill);
	SDK::Interfaces::Surface->DrawFilledRect(x, y + 13, x + w, y + 15 + 15 * amount - 2);
	SDK::Interfaces::Surface->DrawFilledRect(x + 2, y + 13 + amount * 15 + 1 - 1, x + w - 2, y + 13 + amount * 15 + 1 - 1 + 1);

	SDK::Interfaces::Surface->DrawSetColor(DropdownOutlineHovered);
	SDK::Interfaces::Surface->DrawFilledRect(x, y + 13, x + 1, y + 13 + amount * 15);
	SDK::Interfaces::Surface->DrawFilledRect(x + w - 1, y + 13, x + w, y + 13 + amount * 15);

	SDK::Interfaces::Surface->DrawFilledRect(x + 1, y + 13 + amount * 15, x + 1 + 1, y + 13 + amount * 15 + 1);
	SDK::Interfaces::Surface->DrawFilledRect(x + 2, y + 13 + amount * 15 + 1, x + 2 + 1, y + 13 + amount * 15 + 1 + 1);

	SDK::Interfaces::Surface->DrawFilledRect(x + w - 2, y + 13 + amount * 15, x + w - 2 + 1, y + 13 + amount * 15 + 1);
	SDK::Interfaces::Surface->DrawFilledRect(x + w - 3, y + 13 + amount * 15 + 1, x + w - 3 + 1, y + 13 + amount * 15 + 1 + 1);

	SDK::Interfaces::Surface->DrawFilledRect(x + 2, y + 13 + amount * 15 + 1, x + w - 2, y + 13 + amount * 15 + 1 + 1);

	SDK::Interfaces::Surface->DrawSetTextFont(Util::DrawingHelper::Fonts::SmallFonts12Medium);

	for (int i = 0; i < amount; i++)
	{
		SDK::Interfaces::Surface->GetTextSize(Util::DrawingHelper::Fonts::SmallFonts12Medium, s_Callback.m_pwszOptions[i], tw, th);

		SDK::Interfaces::Surface->DrawSetTextColor(Menu::CursorInBox(x, y + 15 + i * 15 + 1, w, 15 - 2) ? SDK::Color(180, 0, 0) : SDK::Color(255, 255, 255));
		SDK::Interfaces::Surface->DrawSetTextPos(x + 5, y + 15 + i * 15 + 15 / 2 - th / 2);
		SDK::Interfaces::Surface->DrawPrintText(s_Callback.m_pwszOptions[i], WStringLength(s_Callback.m_pwszOptions[i]));

	#if defined(XOR_ENABLED)
		unsigned int _esp;
		__asm
		{
			mov _esp, esp
		}

		for (int j = 0; j < 60; j++)
		{
			s_Callback.m_pwszOptions[i][j] = Util::XOR::LinearCongruentGenerator((s_Callback.m_pwszOptions[i][j] & 0xFF) | (_esp & 0xFF)) & 0xFFFF;
		}
	#endif
	}
}