#include "Checkbox.h"

void Menu::DrawCheckbox(int x, int y, CHECKBOX& value)
{
	if (Menu::CursorInBox(x, y, 12, 12) && Util::Input::GetKeyState(SDK::MOUSE_LEFT) & 1)
	{
		value = !value;

		Util::Input::SetKeyState(SDK::MOUSE_LEFT, 0);
	}

	SDK::Interfaces::Surface->DrawSetColor(Menu::CursorInBox(x, y, 12, 12) ? CheckboxOutlineHovered : CheckboxOutline);

	SDK::Interfaces::Surface->DrawFilledRect(x + 1, y + 1, x + 1 + 1, y + 1 + 1);
	SDK::Interfaces::Surface->DrawFilledRect(x + 2, y, x + 10, y + 1);
	SDK::Interfaces::Surface->DrawFilledRect(x + 10, y + 1, x + 10 + 1, y + 1 + 1);

	SDK::Interfaces::Surface->DrawFilledRect(x, y + 2, x, y + 10);
	SDK::Interfaces::Surface->DrawFilledRect(x + 11, y + 2, x + 11 + 1, y + 10);

	SDK::Interfaces::Surface->DrawFilledRect(x, y + 2, x + 1, y + 10);

	SDK::Interfaces::Surface->DrawFilledRect(x + 1, y + 10, x + 1 + 1, y + 10 + 1);
	SDK::Interfaces::Surface->DrawFilledRect(x + 10, y + 10, x + 10 + 1, y + 10 + 1);
	SDK::Interfaces::Surface->DrawFilledRect(x + 2, y + 11, x + 10, y + 11 + 1);

	SDK::Interfaces::Surface->DrawSetColor(CheckboxFilled);
	SDK::Interfaces::Surface->DrawLine(x + 1, y + 2, x + 1, y + 10);
	SDK::Interfaces::Surface->DrawLine(x + 2, y + 1, x + 10, y + 1);
	SDK::Interfaces::Surface->DrawLine(x + 10, y + 2, x + 10, y + 10);
	SDK::Interfaces::Surface->DrawLine(x + 2, y + 10, x + 10, y + 10);
	SDK::Interfaces::Surface->DrawFilledRect(x + 2, y + 2, x + 10, y + 10);

	if (value)
	{
		SDK::Interfaces::Surface->DrawSetColor(CheckboxCheckedOuter);

		SDK::Interfaces::Surface->DrawLine(x + 2, y + 3, x + 2, y + 9);
		SDK::Interfaces::Surface->DrawLine(x + 3, y + 2, x + 9, y + 2);
		SDK::Interfaces::Surface->DrawLine(x + 9, y + 3, x + 9, y + 9);
		SDK::Interfaces::Surface->DrawLine(x + 3, y + 9, x + 9, y + 9);

		SDK::Interfaces::Surface->DrawSetColor(CheckboxChecked);

		SDK::Interfaces::Surface->DrawFilledRect(x + 3, y + 3, x + 9, y + 9);
	}
}