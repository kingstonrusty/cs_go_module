#ifndef __HEADER_CHECKBOX__
#define __HEADER_CHECKBOX__
#pragma once

#include "Colors.h"
#include "Menu.h"

namespace Menu
{
	void DrawCheckbox(int x, int y, CHECKBOX& value);
}

#endif