#ifndef __HEADER_VARS__
#define __HEADER_VARS__
#pragma once

namespace Menu
{
	typedef float SLIDER;
	typedef int32_t CHECKBOX, DROPDOWN, INCRDECR, COORD;
	typedef SDK::Color COLOR;
	typedef wchar_t CHAR;

	struct __SVars
	{
		struct
		{
			struct
			{
				DROPDOWN Enabled;
				DROPDOWN Aimspot;
				DROPDOWN Selection;
				DROPDOWN Autowall;
				DROPDOWN PenetrateTeammates;
				INCRDECR AutowallMinDmg;
				DROPDOWN Hitscan;
				DROPDOWN Multipoint;
				CHECKBOX Backtrack;
				INCRDECR BacktrackAmount;
				DROPDOWN Silent;
				CHECKBOX FriendlyFire;
				CHECKBOX IgnoreBabygod;
				DROPDOWN HitChance;
				INCRDECR HitChanceAmount;
			} Rage;

			struct
			{
				CHECKBOX Enabled;
				CHECKBOX Backtrack;
				INCRDECR BacktrackAmount;
			} Legit;
		} Aimbot;

		struct
		{
			CHECKBOX Enabled;
			DROPDOWN WallDetection;
			CHECKBOX LBYPrediction;
		} Resolver;

		struct
		{
			struct
			{
				CHECKBOX Enabled;
				DROPDOWN Pitch;
				DROPDOWN FakeYaw;
				DROPDOWN RealYaw;
				CHECKBOX BreakLBY = 1;
				SLIDER   MinDeltaFromFakeYaw; // example: if its set to 90 and abs(fakeyaw - realyaw) < 90 it will do fakeyaw += deltaleft /*simplified*/

				struct
				{
					INCRDECR Seed;
					INCRDECR Angles = 2;
				} JitterEngine;
			} AntiAim;
		} HvH;

		struct
		{
			struct
			{
				CHECKBOX Enabled;
				INCRDECR Delay;
				CHAR Clantag1[12 + 1];
				CHAR Clantag2[12 + 1];
				CHAR Clantag3[12 + 1];
				CHAR Clantag4[12 + 1];
				CHAR Clantag5[12 + 1];
				CHAR Clantag6[12 + 1];
			} ClantagChanger;
		} Misc;

		struct
		{
			struct
			{
				CHECKBOX Enabled;
				DROPDOWN Players; // 0 = off, 1 = enabled, 2 = enemy only
				CHECKBOX DrawLocalPlayer;
				CHECKBOX Dormant;
				DROPDOWN Box;
				CHECKBOX OffScreen;
				CHECKBOX Snaplines;
				DROPDOWN Name;
				DROPDOWN Health;
			} ESP;

			struct
			{
				CHECKBOX Enabled;
				CHECKBOX DrawFrame;
				COORD SavedX = -1;
				COORD SavedY = -1;
			} Radar;

			struct
			{
				CHECKBOX Enabled;
				COORD SavedX = -1;
				COORD SavedY = -1;
			} SpectatorList;
		} Visuals;

		struct
		{
			CHAR ConfigFile[16 + 1];
			CHAR ConsoleCommand[128 + 1];
		} Temp;
	};

	extern __SVars Vars;
}

using Menu::Vars;

#endif