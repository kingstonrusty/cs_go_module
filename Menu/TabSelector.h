#ifndef __HEADER_TABSELECTOR__
#define __HEADER_TABSELECTOR__
#pragma once

#include "Colors.h"
#include "Menu.h"

namespace Menu
{
	void DrawTabSelector(int x, int y, int w, int h, int& value, int nTabs, ...);
}

#endif