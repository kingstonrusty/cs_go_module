#include "Button.h"

#include "Util/DrawingHelper.h"

void Menu::DrawButton(int x, int y, int w, const wchar_t* pszTitle, void* pCallback)
{
	int tw, th;

	if (Menu::CursorInBox(x, y, w, 15) && Util::Input::GetKeyState(SDK::MOUSE_LEFT) & 1)
	{
		Util::Input::SetKeyState(SDK::MOUSE_LEFT, 0);

		if (pCallback)
		{
			((void(*)())pCallback)();
		}
	}

	SDK::Interfaces::Surface->DrawSetColor(Menu::CursorInBox(x, y, w, 15) ? ButtonOutlineHovered : ButtonOutline);

	SDK::Interfaces::Surface->DrawFilledRect(x + 1, y + 1, x + 1 + 1, y + 1 + 1);
	SDK::Interfaces::Surface->DrawFilledRect(x + 2, y, x + w - 2, y + 1);
	SDK::Interfaces::Surface->DrawFilledRect(x + w - 2, y + 1, x + w - 2 + 1, y + 1 + 1);

	SDK::Interfaces::Surface->DrawFilledRect(x, y + 2, x + 1, y + 13);
	SDK::Interfaces::Surface->DrawFilledRect(x + w - 1, y + 2, x + w - 1 + 1, y + 13);

	SDK::Interfaces::Surface->DrawFilledRect(x, y + 2, x + 1, y + 13);

	SDK::Interfaces::Surface->DrawFilledRect(x + 1, y + 13, x + 1 + 1, y + 13 + 1);
	SDK::Interfaces::Surface->DrawFilledRect(x + w - 2, y + 13, x + w - 2 + 1, y + 13 + 1);
	SDK::Interfaces::Surface->DrawFilledRect(x + 2, y + 14, x + w - 2, y + 14 + 1);

	SDK::Interfaces::Surface->DrawSetColor(ButtonFill);

	SDK::Interfaces::Surface->DrawFilledRect(x + 2, y + 1, x + w - 2, y + 1 + 1);
	SDK::Interfaces::Surface->DrawFilledRect(x + 1, y + 2, x + w - 1, y + 12 + 1);
	SDK::Interfaces::Surface->DrawFilledRect(x + 2, y + 13, x + w - 2, y + 13 + 1);

	SDK::Interfaces::Surface->DrawSetTextFont(Util::DrawingHelper::Fonts::SmallFonts12Medium);
	SDK::Interfaces::Surface->GetTextSize(Util::DrawingHelper::Fonts::SmallFonts12Medium, pszTitle, tw, th);
	SDK::Interfaces::Surface->DrawSetTextColor(ButtonText);
	SDK::Interfaces::Surface->DrawSetTextPos(x + w / 2 - tw / 2, y + 15 / 2 - th / 2);
	SDK::Interfaces::Surface->DrawPrintText(pszTitle, WStringLength(pszTitle));
}