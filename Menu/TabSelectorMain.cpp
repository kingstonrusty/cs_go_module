#include "TabSelectorMain.h"

#include "Util/DrawingHelper.h"

void Menu::DrawTabSelectorMain(int x, int y, int h, int& tab, int nTabs, ...)
{
	int iSizePer = (h - nTabs * 5) / nTabs;
	int tw, th;

	va_list list;
	va_start(list, nTabs);

	for (int i = 0; i < nTabs; i++)
	{
		if (Menu::CursorInBox(x, y + 5 * i + iSizePer * i, 15, iSizePer) && Util::Input::GetKeyState(SDK::MOUSE_LEFT) & 1)
		{
			Util::Input::SetKeyState(SDK::MOUSE_LEFT, 0);

			tab = i;
		}

		const wchar_t* pwszName = va_arg(list, const wchar_t*);
		int totalsize = WStringLength(pwszName) * 10;

		Util::DrawingHelper::DrawFilledRect(x, y + 5 * i + iSizePer * i, 10, iSizePer, (tab == i) ? TabSelectorMainFillSelected : TabSelectorMainFill);

		for (int j = 0; j < 5; j++)
		{
			Util::DrawingHelper::DrawFilledRect(x + 10 + j, y + 5 * i + iSizePer * i + j, 1, iSizePer - j * 2, (tab == i) ? TabSelectorMainFillSelected : TabSelectorMainFill);
		}	

		Util::DrawingHelper::DrawFilledRect(x, y + 5 * i + iSizePer * i, 10, 1, (Menu::CursorInBox(x, y + 5 * i + iSizePer * i, 15, iSizePer) || tab == i) ? TabSelectorMainOutlineHovered : TabSelectorMainOutline);

		for (int j = 0; j < 5; j++)
		{
			Util::DrawingHelper::DrawFilledRect(x + 10 + j, y + 5 * i + iSizePer * i + j, 1, 1, (Menu::CursorInBox(x, y + 5 * i + iSizePer * i, 15, iSizePer) || tab == i) ? TabSelectorMainOutlineHovered : TabSelectorMainOutline);
		}

		for (int j = 0; j < 5; j++)
		{
			Util::DrawingHelper::DrawFilledRect(x + 10 + j, y + 5 * i + iSizePer * i - j + iSizePer, 1, 1, (Menu::CursorInBox(x, y + 5 * i + iSizePer * i, 15, iSizePer) || tab == i) ? TabSelectorMainOutlineHovered : TabSelectorMainOutline);
		}

		Util::DrawingHelper::DrawFilledRect(x, y + 5 * i + iSizePer * i + iSizePer, 10, 1, (Menu::CursorInBox(x, y + 5 * i + iSizePer * i, 15, iSizePer) || tab == i) ? TabSelectorMainOutlineHovered : TabSelectorMainOutline);

		Util::DrawingHelper::DrawFilledRect(x + 15, y + 5 * i + iSizePer * i + 5, 1, iSizePer - 9, (Menu::CursorInBox(x, y + 5 * i + iSizePer * i, 15, iSizePer) || tab == i) ? TabSelectorMainOutlineHovered : TabSelectorMainOutline);

		for (int j = 0; j < (int)WStringLength(pwszName); j++)
		{
			wchar_t szLetter[2] = { pwszName[j], '\0' };

			Util::DrawingHelper::GetTextSize(tw, th, Util::DrawingHelper::Fonts::SmallFonts12SemiBold, szLetter);
			Util::DrawingHelper::DrawText(Util::DrawingHelper::Fonts::SmallFonts12SemiBold, x + 15 / 2 - tw / 2 - 1, y + 5 * i + iSizePer * i + 5 + j * 10 + iSizePer / 2 - totalsize / 2 - 5, szLetter, TabSelectorMainText);
		}
	}

	va_end(list);
}