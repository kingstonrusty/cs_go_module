#include "Menu.h"

namespace Menu
{
	__SVars Vars;
}

static int s_MenuX, s_MenuY;
static int s_iMenuTab;
static bool s_bFirst;
static bool s_bShouldDraw;
short g_OriginalMouseState;

static void DrawCursor();
extern void DrawAimbot(int, int);
extern void DrawVisuals(int, int);
extern void DrawOther(int, int);

void Menu::Draw()
{
	if (!s_bShouldDraw)
	{
		return;
	}

	if (!s_bFirst)
	{
		int sw, sh;
		SDK::Interfaces::Surface->GetScreenSize(sw, sh);

		s_MenuX = sw / 2 - 573 / 2;
		s_MenuY = sh / 2 - 413 / 2;

		s_bFirst = true;
	}

	if (IsDropdownActive())
	{
		g_OriginalMouseState = Util::Input::GetKeyState(SDK::MOUSE_LEFT);
		Util::Input::SetKeyState(SDK::MOUSE_LEFT, 0);
	}

	DrawFrame(s_MenuX, s_MenuY, 579, 413, MENU_FRAME, NAMEW);
	DrawTabSelectorMain(s_MenuX + 1, s_MenuY + 21, 398, s_iMenuTab, 5, WXorStr(L"AIMBOT"), WXorStr(L"VISUAL"), WXorStr(L"MISC"), WXorStr(L"OTHER"), WXorStr(L"PLAYER"));

	if (s_iMenuTab == 0)
	{
		DrawAimbot(s_MenuX + 27, s_MenuY + 31);
	}
	else if (s_iMenuTab == 1)
	{
		DrawVisuals(s_MenuX + 27, s_MenuY + 31);
	}
	else if (s_iMenuTab == 3)
	{
		DrawOther(s_MenuX + 27, s_MenuY + 31);
	}
	else if (s_iMenuTab == 4)
	{
		DrawPlayerList(s_MenuX + 27, s_MenuY + 31);
	}

	if (IsDropdownActive())
	{
		DrawDropdownCallback();
	}

	DrawCursor();
}

bool Menu::GetShouldDraw()
{
	return s_bShouldDraw;
}

void Menu::SetShouldDraw(bool bDrawMenu)
{
	s_bShouldDraw = bDrawMenu;
}

bool Menu::CursorInBox(int x, int y, int w, int h)
{
	int mx, my;
	SDK::Interfaces::Surface->SurfaceGetCursorPos(mx, my);

	return (
		mx >= x &&
		mx <= (x + w) &&
		my >= y &&
		my <= (y + h)
	);
}

void DrawCursor()
{
	int x, y;
	SDK::Interfaces::Surface->SurfaceGetCursorPos(x, y);

	SDK::Interfaces::Surface->DrawSetColor(Menu::CursorOutline);

	SDK::Interfaces::Surface->DrawFilledRect(x, y, x + 1, y + 12);
	SDK::Interfaces::Surface->DrawFilledRect(x + 1, y, x + 1 + 1, y + 11);
	SDK::Interfaces::Surface->DrawFilledRect(x + 1, y + 11, y + 11 + 1, x + 1 + 1);
	SDK::Interfaces::Surface->DrawFilledRect(x + 2, y + 1, x + 2 + 1, y + 1 + 1);
	SDK::Interfaces::Surface->DrawFilledRect(x + 2, y + 10, x + 2 + 1, y + 10 + 1);
	SDK::Interfaces::Surface->DrawFilledRect(x + 3, y + 2, x + 3 + 1, y + 2 + 1);
	SDK::Interfaces::Surface->DrawFilledRect(x + 3, y + 9, x + 3 + 1, y + 9 + 1);
	SDK::Interfaces::Surface->DrawFilledRect(x + 4, y + 3, x + 4 + 1, y + 3 + 1);
	SDK::Interfaces::Surface->DrawFilledRect(x + 5, y + 4, x + 5 + 1, y + 4 + 1);
	SDK::Interfaces::Surface->DrawFilledRect(x + 6, y + 5, x + 6 + 1, y + 5 + 1);
	SDK::Interfaces::Surface->DrawFilledRect(x + 7, y + 6, x + 7 + 1, y + 6 + 1);
	SDK::Interfaces::Surface->DrawFilledRect(x + 8, y + 7, x + 8 + 1, y + 7 + 1);
	SDK::Interfaces::Surface->DrawFilledRect(x + 4, y + 8, x + 4 + 4, y + 8 + 1);
	SDK::Interfaces::Surface->DrawFilledRect(x + 1, y + 11, x + 1 + 1, y + 11 + 1);

	SDK::Interfaces::Surface->DrawSetColor(Menu::Cursor);

	SDK::Interfaces::Surface->DrawFilledRect(x + 1, y + 1, x + 1 + 1, y + 1 + 10);
	SDK::Interfaces::Surface->DrawFilledRect(x + 2, y + 2, x + 2 + 1, y + 2 + 8);
	SDK::Interfaces::Surface->DrawFilledRect(x + 3, y + 3, x + 3 + 1, y + 3 + 6);
	SDK::Interfaces::Surface->DrawFilledRect(x + 4, y + 4, x + 4 + 1, y + 4 + 4);
	SDK::Interfaces::Surface->DrawFilledRect(x + 5, y + 5, x + 5 + 1, y + 5 + 3);
	SDK::Interfaces::Surface->DrawFilledRect(x + 6, y + 6, x + 6 + 1, y + 6 + 2);
	SDK::Interfaces::Surface->DrawFilledRect(x + 7, y + 7, x + 7 + 1, y + 7 + 1);
}

void Menu::DrawText(SDK::Color col, SDK::HFont font, int x, int y, bool bCenter, const wchar_t* pwszText)
{
	SDK::Interfaces::Surface->DrawSetTextFont(font);

	if (bCenter)
	{
		int tw, th;
		SDK::Interfaces::Surface->GetTextSize(font, pwszText, tw, th);

		y -= th / 2;
	}

	SDK::Interfaces::Surface->DrawSetTextPos(x, y);
	SDK::Interfaces::Surface->DrawSetTextColor(col);
	SDK::Interfaces::Surface->DrawPrintText(pwszText, WStringLength(pwszText));
}