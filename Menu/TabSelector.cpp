#include "TabSelector.h"

#include "Util/DrawingHelper.h"

void Menu::DrawTabSelector(int x, int y, int w, int h, int& value, int nTabs, ...)
{
	int tw, th;
	int iSizePer = w / nTabs;

	va_list list;
	va_start(list, nTabs);

	SDK::Interfaces::Surface->DrawSetTextFont(Util::DrawingHelper::Fonts::SmallFonts12SemiBold);
	SDK::Interfaces::Surface->DrawSetTextColor(TabSelectorText);

	for (int i = 0; i < nTabs; i++)
	{
		if (Menu::CursorInBox(x + iSizePer * i + 1, y, iSizePer - 2, 15) && Util::Input::GetKeyState(SDK::MOUSE_LEFT) & 1)
		{
			value = i;

			Util::Input::SetKeyState(SDK::MOUSE_LEFT, 0);
		}

		const wchar_t* pszName = va_arg(list, const wchar_t*);

		SDK::Interfaces::Surface->DrawSetColor((Menu::CursorInBox(x + iSizePer * i + 1, y, iSizePer - 2, 15) || value == i) ? TabSelectorOutlineHovered : TabSelectorOutline);

		int _x = x + iSizePer * i;

		SDK::Interfaces::Surface->DrawFilledRect(_x + 1, y + 1, _x + 1 + 1, y + 1 + 1);
		SDK::Interfaces::Surface->DrawFilledRect(_x + 2, y, _x + iSizePer - 2, y + 1);
		SDK::Interfaces::Surface->DrawFilledRect(_x + iSizePer - 2, y + 1, _x + iSizePer - 2 + 1, y + 1 + 1);

		SDK::Interfaces::Surface->DrawFilledRect(_x, y + 2, _x + 1, y + 14);
		SDK::Interfaces::Surface->DrawFilledRect(_x + iSizePer - 1, y + 2, _x + iSizePer - 1 + 1, y + 14);

		SDK::Interfaces::Surface->DrawFilledRect(_x, y + 2, _x + 1, y + 14);

		SDK::Interfaces::Surface->DrawFilledRect(_x, y + 14, _x + iSizePer, y + 14 + 1);

		SDK::Interfaces::Surface->DrawSetColor((value == i) ? TabSelectorFillSelected : TabSelectorFill);

		SDK::Interfaces::Surface->DrawFilledRect(_x + 1, y + 2, _x + iSizePer - 1, y + 14);
		SDK::Interfaces::Surface->DrawFilledRect(_x + 2, y + 1, _x + iSizePer - 2, y + 1 + 1);

		SDK::Interfaces::Surface->GetTextSize(Util::DrawingHelper::Fonts::SmallFonts12SemiBold, pszName, tw, th);
		SDK::Interfaces::Surface->DrawSetTextPos(x + iSizePer * i + iSizePer / 2 - tw / 2, y + 15 / 2 - th / 2);
		SDK::Interfaces::Surface->DrawPrintText(pszName, WStringLength(pszName));
	}

	SDK::Interfaces::Surface->DrawSetColor(TabSelectorAreaFill);
	SDK::Interfaces::Surface->DrawFilledRect(x, y + 15, x + w - 1, y + h - 2);

	SDK::Interfaces::Surface->DrawSetColor(TabSelectorOutline);
	SDK::Interfaces::Surface->DrawFilledRect(x, y + 15, x + w - 1, y + 15 + 1);
	SDK::Interfaces::Surface->DrawFilledRect(x, y + 15, x + 1, y + h - 2);
	SDK::Interfaces::Surface->DrawFilledRect(x + w - 2, y + 15, x + w + 1 - 2, y + h - 2);
	SDK::Interfaces::Surface->DrawFilledRect(x + 1, y + h - 2, x + 1 + 1, y + h - 2 + 1);
	SDK::Interfaces::Surface->DrawFilledRect(x + 2, y + h - 1, x + w - 3, y + h - 1 + 1);
	SDK::Interfaces::Surface->DrawFilledRect(x + w - 3, y + h - 2, x + w - 3 + 1, y + h - 2 + 1);

	SDK::Interfaces::Surface->DrawSetColor(TabSelectorAreaFill);
	SDK::Interfaces::Surface->DrawFilledRect(x + 2, y + h - 2, x + w - 3, y + h - 2 + 1);

	va_end(list);
}