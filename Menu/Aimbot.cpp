#include "Menu/Menu.h"

#include "Util/DrawingHelper.h"

void DrawAimbot(int x, int y)
{
	using namespace Menu;

	DrawArea(x, y, 541, 230, WXorStr(L"Rage"));
	{
		int _x = x + 5;
		int _y = y + 10;

		DrawText(LabelColor, Util::DrawingHelper::Fonts::SmallFonts12Medium, _x, _y + 15 / 2, true, WXorStr(L"Enabled"));
		DrawDropdown(_x + 95, _y, 75, Vars.Aimbot.Rage.Enabled, 3, WXorStr(L"Off"), WXorStr(L"Always"), WXorStr(L"On Key"));

		_y += 20;

		DrawText(LabelColor, Util::DrawingHelper::Fonts::SmallFonts12Medium, _x, _y + 15 / 2, true, WXorStr(L"Silent"));
		DrawDropdown(_x + 95, _y, 75, Vars.Aimbot.Rage.Silent, 3, WXorStr(L"Off"), WXorStr(L"Client"), WXorStr(L"Server"));

		_y += 20;

		DrawText(LabelColor, Util::DrawingHelper::Fonts::SmallFonts12Medium, _x, _y + 15 / 2, true, WXorStr(L"Selection"));
		DrawDropdown(_x + 95, _y, 75, Vars.Aimbot.Rage.Selection, 4, WXorStr(L"Iterate"), WXorStr(L"Distance"), WXorStr(L"Health"), WXorStr(L"Smart"));

		_y += 20;

		DrawText(LabelColor, Util::DrawingHelper::Fonts::SmallFonts12Medium, _x, _y + 15 / 2, true, L"Aimspot");
		DrawDropdown(_x + 95, _y, 75, Vars.Aimbot.Rage.Aimspot, 7, WXorStr(L"Head"), WXorStr(L"Chest"), WXorStr(L"Stomach"), WXorStr(L"Right Arm"), WXorStr(L"Left Arm"), WXorStr(L"Left Leg"), WXorStr(L"Right Leg"));

		_y += 20;

		DrawText(LabelColor, Util::DrawingHelper::Fonts::SmallFonts12Medium, _x, _y + 15 / 2, true, WXorStr(L"Hitscan"));
		DrawDropdown(_x + 95, _y, 75, Vars.Aimbot.Rage.Hitscan, 3, WXorStr(L"Off"), WXorStr(L"Minimal"), WXorStr(L"Full"));

		_y += 20;

		DrawText(LabelColor, Util::DrawingHelper::Fonts::SmallFonts12Medium, _x, _y + 15 / 2, true, WXorStr(L"Multipoint"));
		DrawDropdown(_x + 95, _y, 75, Vars.Aimbot.Rage.Multipoint, 4, WXorStr(L"Off"), WXorStr(L"Low"), WXorStr(L"Medium"), WXorStr(L"High"));

		_y += 20;

		DrawText(LabelColor, Util::DrawingHelper::Fonts::SmallFonts12Medium, _x, _y + 15 / 2, true, WXorStr(L"Autowall"));
		DrawDropdown(_x + 95, _y, 75, Vars.Aimbot.Rage.Autowall, 3, WXorStr(L"Off"), WXorStr(L"On"), WXorStr(L"On + Hitscan"));

		_y += 20;

		DrawText(LabelColor, Util::DrawingHelper::Fonts::SmallFonts12Medium, _x, _y + 15 / 2, true, WXorStr(L"Min. Damage"));
		DrawIncrDecrBox(_x + 95, _y, 75, Vars.Aimbot.Rage.AutowallMinDmg, 0, 100);

		_y += 20;

		DrawText(LabelColor, Util::DrawingHelper::Fonts::SmallFonts12Medium, _x, _y + 15 / 2, true, WXorStr(L"Backtracking"));
		DrawCheckbox(_x + 95, _y + 2, Vars.Aimbot.Rage.Backtrack);

		_y += 20;

		DrawText(LabelColor, Util::DrawingHelper::Fonts::SmallFonts12Medium, _x, _y + 15 / 2, true, WXorStr(L"Backtrack Amount"));
		DrawIncrDecrBox(_x + 95, _y, 75, Vars.Aimbot.Rage.BacktrackAmount, 1, 16);

		_y += 20;

		DrawText(LabelColor, Util::DrawingHelper::Fonts::SmallFonts12Medium, _x, _y + 15 / 2, true, WXorStr(L"Friendly Fire"));
		DrawCheckbox(_x + 95, _y + 2, Vars.Aimbot.Rage.FriendlyFire);

		_y = y + 10;
		_x += 95 + 75 + 10;

		DrawText(LabelColor, Util::DrawingHelper::Fonts::SmallFonts12Medium, _x, _y + 15 / 2, true, WXorStr(L"Ignore Babygod"));
		DrawCheckbox(_x + 95, _y + 2, Vars.Aimbot.Rage.IgnoreBabygod);

		_y += 20;

		DrawText(LabelColor, Util::DrawingHelper::Fonts::SmallFonts12Medium, _x, _y + 15 / 2, true, WXorStr(L"Hit Chance"));
		DrawDropdown(_x + 95, _y, 75, Vars.Aimbot.Rage.HitChance, 3, WXorStr(L"Off"), WXorStr(L"Intersection"), WXorStr(L"Inaccuracy"));

		_y += 20;

		DrawText(LabelColor, Util::DrawingHelper::Fonts::SmallFonts12Medium, _x, _y + 15 / 2, true, WXorStr(L"Hit Chance %"));
		DrawIncrDecrBox(_x + 95, _y, 75, Vars.Aimbot.Rage.HitChanceAmount, 1, 100);
	}

	DrawArea(x, y + 240, 541, 132, WXorStr(L"Legit"));
	{
		int _x = x + 5;
		int _y = y + 240 + 10;

		DrawText(LabelColor, Util::DrawingHelper::Fonts::SmallFonts12Medium, _x, _y + 15 / 2, true, WXorStr(L"Enabled"));
		DrawCheckbox(_x + 95, _y + 2, Vars.Aimbot.Legit.Enabled);

		_y += 20;

		DrawText(LabelColor, Util::DrawingHelper::Fonts::SmallFonts12Medium, _x, _y + 15 / 2, true, WXorStr(L"Backtracking"));
		DrawCheckbox(_x + 95, _y + 2, Vars.Aimbot.Legit.Backtrack);

		_y += 20;

		DrawText(LabelColor, Util::DrawingHelper::Fonts::SmallFonts12Medium, _x, _y + 15 / 2, true, WXorStr(L"Backtrack Amount"));
		DrawIncrDecrBox(_x + 95, _y, 75, Vars.Aimbot.Legit.BacktrackAmount, 1, 16);
	}
}