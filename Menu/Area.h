#ifndef __HEADER_AREA__
#define __HEADER_AREA__
#pragma once

#include "Colors.h"
#include "Menu.h"

namespace Menu
{
	void DrawArea(int x, int y, int w, int h, const wchar_t* pwszName);
}

#endif