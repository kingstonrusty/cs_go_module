#ifndef __HEADER_FRAME__
#define __HEADER_FRAME__
#pragma once

#include "Colors.h"
#include "Menu.h"

namespace Menu
{
	enum
	{
		MENU_FRAME = 0,
		MENU_MAXFRAMES
	};

	void DrawFrame(int& x, int& y, int w, int h, int frameindex, const wchar_t* pwszTitle);
}

#endif