#ifndef __HEADER_TEXTBOT__
#define __HEADER_TEXTBOT__
#pragma once

#include "Colors.h"
#include "Menu.h"

namespace Menu
{
	void _DrawTextBox(int x, int y, int w, wchar_t* pwszBuffer, int maxlen, bool& bTyping, wchar_t* pwszBackup, bool bForceTyping = false);
}

#define DrawTextBox(x, y, w, buffer, maxlen, forcetyping) \
{ \
static bool s_bTyping; \
static wchar_t s_Backup[maxlen + 1]; \
Menu::_DrawTextBox(x, y, w, buffer, maxlen, s_bTyping, s_Backup, forcetyping); \
}

#endif