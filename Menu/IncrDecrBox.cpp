#include "IncrDecrBox.h"

#include "Util/DrawingHelper.h"

#define CHECKKEY(key, c) \
if(WStringLength(pwszBuf) < 11 && Util::Input::GetKeyState(key) & 1) \
{ \
pwszBuf[WStringLength(pwszBuf)] = c; \
}

static void ProcessTyping(Menu::INCRDECR& value, bool bInBox, bool bClick, bool& bTyping, wchar_t* pwszBuf)
{
	if (bClick && bInBox)
	{
		bTyping = true;
		Util::Input::SetKeyState(SDK::MOUSE_LEFT, 0);

		for (int i = 0; i < 12; i++)
		{
			pwszBuf[i] = '\0';
		}
	}

	if (!bTyping)
	{
		return;
	}

	if (!bInBox && bClick)
	{
		bTyping = false;
		Util::Input::SetKeyState(SDK::MOUSE_LEFT, 0);

		return;
	}

	if (Util::Input::GetKeyState(SDK::KEY_BACKSPACE) & 1 && WStringLength(pwszBuf) > 0)
	{
		pwszBuf[WStringLength(pwszBuf) - 1] = '\0';
	}

	if (WStringLength(pwszBuf) == 0 && (Util::Input::GetKeyState(SDK::KEY_MINUS) & 1 || Util::Input::GetKeyState(SDK::KEY_PAD_MINUS) & 1))
	{
		pwszBuf[0] = '-';
	}

	CHECKKEY(SDK::KEY_0, '0');
	CHECKKEY(SDK::KEY_1, '1');
	CHECKKEY(SDK::KEY_2, '2');
	CHECKKEY(SDK::KEY_3, '3');
	CHECKKEY(SDK::KEY_4, '4');
	CHECKKEY(SDK::KEY_5, '5');
	CHECKKEY(SDK::KEY_6, '6');
	CHECKKEY(SDK::KEY_7, '7');
	CHECKKEY(SDK::KEY_8, '8');
	CHECKKEY(SDK::KEY_9, '9');

	CHECKKEY(SDK::KEY_PAD_0, '0');
	CHECKKEY(SDK::KEY_PAD_1, '1');
	CHECKKEY(SDK::KEY_PAD_2, '2');
	CHECKKEY(SDK::KEY_PAD_3, '3');
	CHECKKEY(SDK::KEY_PAD_4, '4');
	CHECKKEY(SDK::KEY_PAD_5, '5');
	CHECKKEY(SDK::KEY_PAD_6, '6');
	CHECKKEY(SDK::KEY_PAD_7, '7');
	CHECKKEY(SDK::KEY_PAD_8, '8');
	CHECKKEY(SDK::KEY_PAD_9, '9');

	pwszBuf[WStringLength(pwszBuf)] = '\0';

	if (Util::Input::GetKeyState(SDK::KEY_ENTER) & 1 || Util::Input::GetKeyState(SDK::KEY_PAD_ENTER) & 1)
	{
		bTyping = false;
		value = WStringToInt(pwszBuf);
	}
}

void Menu::_DrawIncrDecrBox(int x, int y, int w, INCRDECR& value, int min, int max, bool& bTyping, wchar_t* pwszBuf)
{
	if (!bTyping)
	{
	#if defined(XOR_ENABLED)
		unsigned int _esp;
		__asm
		{
			mov _esp, esp
		}

		for (int i = 0; i < 12; i++)
		{
			pwszBuf[i] = Util::XOR::LinearCongruentGenerator((pwszBuf[i] & 0xFF) | (_esp & 0xFF)) & 0xFFFF;
		}
	#endif
	}

	int tw, th;

	value = value > max ? max : (value < min ? min : value);

	if (Menu::CursorInBox(x, y, 15, 15) && Util::Input::GetKeyState(SDK::MOUSE_LEFT) & 1)
	{
		value = value - 1;
		if (value < min) value = min;

		Util::Input::SetKeyState(SDK::MOUSE_LEFT, 0);
	}

	if (Menu::CursorInBox(x + w - 15, y, 15, 15) && Util::Input::GetKeyState(SDK::MOUSE_LEFT) & 1)
	{
		value = value + 1;
		if (value > max) value = max;

		Util::Input::SetKeyState(SDK::MOUSE_LEFT, 0);
	}

	ProcessTyping(value, Menu::CursorInBox(x + 16, y, w - 32, 15), Util::Input::GetKeyState(SDK::MOUSE_LEFT) & 1, bTyping, pwszBuf);

	SDK::Interfaces::Surface->DrawSetColor(IncrDecrFill);

	SDK::Interfaces::Surface->DrawFilledRect(x + 2, y + 1, x + w - 2, y + 1 + 1);
	SDK::Interfaces::Surface->DrawFilledRect(x + 1, y + 2, x + w - 1, y + 12 + 1);
	SDK::Interfaces::Surface->DrawFilledRect(x + 2, y + 13, x + w - 2, y + 13 + 1);

	SDK::Interfaces::Surface->DrawSetColor(Menu::CursorInBox(x, y, 15, 15) ? IncrDecrOutlineHovered : IncrDecrOutline);

	SDK::Interfaces::Surface->DrawFilledRect(x + 1, y + 1, x + 1 + 1, y + 1 + 1);
	SDK::Interfaces::Surface->DrawFilledRect(x, y + 2, x + 1, y + 13);
	SDK::Interfaces::Surface->DrawFilledRect(x + 1, y + 13, x + 1 + 1, y + 13 + 1);
	SDK::Interfaces::Surface->DrawFilledRect(x + 14, y, x + 14 + 1, y + 14);
	SDK::Interfaces::Surface->DrawFilledRect(x + 2, y, x + 14, y + 1);
	SDK::Interfaces::Surface->DrawFilledRect(x + 2, y + 14, x + 15, y + 14 + 1);

	SDK::Interfaces::Surface->DrawSetColor(Menu::CursorInBox(x + w - 15, y, 15, 15) ? IncrDecrOutlineHovered : IncrDecrOutline);

	SDK::Interfaces::Surface->DrawFilledRect(x + w - 2, y + 1, x + w - 2 + 1, y + 1 + 1);
	SDK::Interfaces::Surface->DrawFilledRect(x + w - 1, y + 2, x + w - 1 + 1, y + 13);
	SDK::Interfaces::Surface->DrawFilledRect(x + w - 2, y + 13, x + w - 2 + 1, y + 13 + 1);
	SDK::Interfaces::Surface->DrawFilledRect(x + w - 15, y, x + w - 15 + 1, y + 14);
	SDK::Interfaces::Surface->DrawFilledRect(x + w - 14, y, x + w - 3 + 1, y + 1);
	SDK::Interfaces::Surface->DrawFilledRect(x + w - 15, y + 14, x + w - 3 + 1, y + 14 + 1);

	SDK::Interfaces::Surface->DrawSetColor((Menu::CursorInBox(x + 16, y, w - 32, 15) || bTyping) ? IncrDecrOutlineHovered : IncrDecrOutline);
	SDK::Interfaces::Surface->DrawFilledRect(x + 15, y, x + w - 15, y + 1);
	SDK::Interfaces::Surface->DrawFilledRect(x + 15, y + 14, x + w - 15, y + 14 + 1);

	SDK::Interfaces::Surface->DrawSetColor(Menu::CursorInBox(x, y, 15, 15) ? IncrDecrOutlineHovered : IncrDecrOutline);

	SDK::Interfaces::Surface->DrawFilledRect(x + 4, y + 15 / 2, x + 11, y + 15 / 2 + 1);

	SDK::Interfaces::Surface->DrawSetColor(Menu::CursorInBox(x + w - 15, y, 15, 15) ? IncrDecrOutlineHovered : IncrDecrOutline);

	SDK::Interfaces::Surface->DrawFilledRect(x + w - 15 + 4, y + 15 / 2, x + w - 15 + 11, y + 15 / 2 + 1);
	SDK::Interfaces::Surface->DrawFilledRect(x + w - 8, y + 4, x + w - 8 + 1, y + 11);

	wchar_t szBuf[12];
	FormatWString(szBuf, 12, WXorStr(L"%d"), value);

	const wchar_t* pszString = bTyping ? pwszBuf : szBuf;

	SDK::Interfaces::Surface->DrawSetTextFont(Util::DrawingHelper::Fonts::SmallFonts12SemiBold);
	SDK::Interfaces::Surface->DrawSetTextColor(IncrDecrText);
	SDK::Interfaces::Surface->GetTextSize(Util::DrawingHelper::Fonts::SmallFonts12SemiBold, pszString, tw, th);
	SDK::Interfaces::Surface->DrawSetTextPos(x + 15 + (w - 30) / 2 - tw / 2, y + 15 / 2 - th / 2);
	SDK::Interfaces::Surface->DrawPrintText(pszString, WStringLength(pszString));

	if (bTyping)
	{
		SDK::Interfaces::Surface->DrawSetColor(IncrDecrOutline);
		SDK::Interfaces::Surface->DrawFilledRect(x + 15 + (w - 30) / 2 + tw / 2 + 1, y + 3, x + 15 + (w - 30) / 2 + tw / 2 + 2 + 1, y + 12);
	}

	if (!bTyping)
	{
	#if defined(XOR_ENABLED)
		unsigned int _esp;
		__asm
		{
			mov _esp, esp
		}

		for (int i = 0; i < 12; i++)
		{
			pwszBuf[i] = Util::XOR::LinearCongruentGenerator((pwszBuf[i] & 0xFF) | (_esp & 0xFF)) & 0xFFFF;
		}
	#endif
	}
}