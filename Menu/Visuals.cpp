#include "Menu/Menu.h"

#include "Util/DrawingHelper.h"

void DrawVisuals(int x, int y)
{
	using namespace Menu;

	DrawArea(x, y, 541, 230, WXorStr(L"ESP"));
	{
		int _x = x + 5;
		int _y = y + 10;

		DrawText(LabelColor, Util::DrawingHelper::Fonts::SmallFonts12Medium, _x, _y + 15 / 2, true, WXorStr(L"Enabled"));
		DrawCheckbox(_x + 95, _y + 2, Vars.Visuals.ESP.Enabled);

		_y += 20;

		DrawText(LabelColor, Util::DrawingHelper::Fonts::SmallFonts12Medium, _x, _y + 15 / 2, true, WXorStr(L"Show Players"));
		DrawDropdown(_x + 95, _y, 75, Vars.Visuals.ESP.Players, 3, WXorStr(L"Off"), WXorStr(L"Enabled"), WXorStr(L"Enemy Only"));

		_y += 20;

		DrawText(LabelColor, Util::DrawingHelper::Fonts::SmallFonts12Medium, _x, _y + 15 / 2, true, WXorStr(L"Show Local Player"));
		DrawCheckbox(_x + 95, _y + 2, Vars.Visuals.ESP.DrawLocalPlayer);

		_y += 20;

		DrawText(LabelColor, Util::DrawingHelper::Fonts::SmallFonts12Medium, _x, _y + 15 / 2, true, WXorStr(L"Dormant"));
		DrawCheckbox(_x + 95, _y + 2, Vars.Visuals.ESP.Dormant);

		_y += 20;

		DrawText(LabelColor, Util::DrawingHelper::Fonts::SmallFonts12Medium, _x, _y + 15 / 2, true, WXorStr(L"Off-Screen"));
		DrawCheckbox(_x + 95, _y + 2, Vars.Visuals.ESP.OffScreen);

		_y += 20;

		DrawText(LabelColor, Util::DrawingHelper::Fonts::SmallFonts12Medium, _x, _y + 15 / 2, true, WXorStr(L"Snaplines"));
		DrawCheckbox(_x + 95, _y + 2, Vars.Visuals.ESP.Snaplines);

		_y += 20;

		DrawText(LabelColor, Util::DrawingHelper::Fonts::SmallFonts12Medium, _x, _y + 15 / 2, true, WXorStr(L"Name"));
		DrawDropdown(_x + 95, _y, 75, Vars.Visuals.ESP.Name, 3, WXorStr(L"Off"), WXorStr(L"Top"), WXorStr(L"Bottom"));

		_y += 20;

		DrawText(LabelColor, Util::DrawingHelper::Fonts::SmallFonts12Medium, _x, _y + 15 / 2, true, WXorStr(L"Health"));
		DrawDropdown(_x + 95, _y, 75, Vars.Visuals.ESP.Health, 3, WXorStr(L"Off"), WXorStr(L"Top"), WXorStr(L"Bottom"));

		_y += 20;

		DrawText(LabelColor, Util::DrawingHelper::Fonts::SmallFonts12Medium, _x, _y + 15 / 2, true, WXorStr(L"Box"));
		DrawDropdown(_x + 95, _y, 75, Vars.Visuals.ESP.Box, 3, WXorStr(L"Off"), WXorStr(L"Normal"), WXorStr(L"Interwebz"));

		_y += 20;
	}
}