#ifndef __HEADER_CBASEENTITY__
#define __HEADER_CBASEENTITY__
#pragma once

#include "IClientEntity.h"

namespace SDK
{
	enum thinkmethods_t
	{
		THINK_FIRE_ALL_FUNCTIONS,
		THINK_FIRE_BASE_ONLY,
		THINK_FIRE_ALL_BUT_BASE,
	};

	class C_BaseEntity;
}

class SDK::C_BaseEntity : public IClientEntity
{
public:
	void SetAbsOrigin(Vector& origin)
	{
		return ((void(__thiscall*)(C_BaseEntity*, Vector&))Offsets::C_BaseEntity::SetAbsOrigin)(this, origin);
	}

	void SetAbsAngles(Vector& angles)
	{
		return ((void(__thiscall*)(C_BaseEntity*, Vector&))Offsets::C_BaseEntity::SetAbsAngles)(this, angles);
	}

	float GetSimulationTime()
	{
		return *(float*)((uint32_t)this + Offsets::C_BaseEntity::m_flSimulationTime);
	}

	float GetAnimTime()
	{
		return *(float*)((uint32_t)this + Offsets::C_BaseEntity::m_flAnimTime);
	}

	void SetAnimTime(float time)
	{
		*(float*)((uint32_t)this + Offsets::C_BaseEntity::m_flAnimTime) = time;
	}

	Vector& GetLocalOrigin()
	{
		return *(Vector*)((uint32_t)this + Offsets::C_BaseEntity::m_vecOrigin);
	}

	void RemoveFromInterpolationList()
	{
		((void(__thiscall*)(C_BaseEntity*, int))Offsets::C_BaseEntity::RemoveFromInterpolationList)(this, 0);
	}
	
	void AddToInterpolationList()
	{

	}

	int GetTeamNum()
	{
		return *(int*)((uint32_t)this + Offsets::C_BaseEntity::m_iTeamNum);
	}

	bool IsBreakable()
	{
		return ((bool(__thiscall*)(C_BaseEntity*))Offsets::C_BaseEntity::IsBreakable)(this);
	}

	bool PhysicsRunThink(thinkmethods_t thinkMethod)
	{
		return ((bool(__thiscall*)(C_BaseEntity*, thinkmethods_t))Offsets::C_BaseEntity::PhysicsRunThink)(this, thinkMethod);
	}

	int GetNextThinkTick()
	{
		return *(int*)((uint32_t)this + 248);
	}

	void SetNextThinkTick(int nNextThink)
	{
		*(int*)((uint32_t)this + 248) = nNextThink;
	}

	void Think()
	{
		return GET_VFUNC(void(__thiscall*)(C_BaseEntity*), this, 137)(this);
	}

	bool Think_NoIdea1(char a2)
	{
		return ((bool(__thiscall*)(C_BaseEntity*, char))Offsets::C_BaseEntity::Think_NoIdea1)(this, a2);
	}
};

#endif