#ifndef __HEADER_ICLIENTENTITY__
#define __HEADER_ICLIENTENTITY__
#pragma once

#include "IClientRenderable.h"
#include "IClientNetworkable.h"

namespace SDK
{
	class IClientEntity;
}

class SDK::IClientEntity
{
public:
	IClientRenderable* GetRenderable()
	{
		return (IClientRenderable*)((uint32_t)this + 0x04);
	}

	IClientNetworkable* GetNetworkable()
	{
		return (IClientNetworkable*)((uint32_t)this + 0x08);
	}

	Vector& GetAbsOrigin()
	{
		return GET_VFUNC(Vector&(__thiscall*)(IClientEntity*), this, 10)(this);
	}

	Vector& GetAbsAngles()
	{
		return GET_VFUNC(Vector&(__thiscall*)(IClientEntity*), this, 11)(this);
	}
};

#endif