#ifndef __HEADER_VECTOR__
#define __HEADER_VECTOR__
#pragma once

namespace SDK
{
	class Vector;

	__forceinline void AngleVectors(Vector& rangAngle, Vector* pForward = nullptr, Vector* pRight = nullptr, Vector* pUp = nullptr);
	__forceinline float DotProduct(const SDK::Vector& a, const SDK::Vector& b);

	__forceinline float Deg2Rad(float flAngle) 
	{
		constexpr float _DEG2RAD = 3.14159265358979323846f / 180.f;
		constexpr float _RAD2DEG = 180.f / 3.14159265358979323846f;
		return flAngle * _DEG2RAD;
	}

	__forceinline float Rad2Deg(float flAngle) 
	{
		constexpr float _DEG2RAD = 3.14159265358979323846f / 180.f;
		constexpr float _RAD2DEG = 180.f / 3.14159265358979323846f;
		return flAngle * _RAD2DEG;
	}

	__forceinline void Deg2Rad(float flAngle, float& rflPut)
	{
		rflPut = Deg2Rad(flAngle);
	}

	__forceinline void Rad2Deg(float flAngle, float& rflPut) 
	{
		rflPut = Rad2Deg(flAngle);
	}

	__forceinline void SinCos(const float flValue, float& rflSin, float& rflCos) 
	{
		__asm
		{
			fld DWORD PTR [flValue]
			fsincos
			mov ebx, [rflCos]
			fstp DWORD PTR [ebx]
			mov ebx, [rflSin]
			fstp DWORD PTR [ebx]
		}
	}
}

class SDK::Vector
{
public:
	Vector()
	{
		Init(0.f, 0.f, 0.f);
	}

	Vector(float x, float y, float z)
	{
		Init(x, y, z);
	}

	Vector(const Vector& other)
	{
		Init(other.x, other.y, other.z);
	}

	void Init(float x, float y, float z)
	{
		this->x = x;
		this->y = y;
		this->z = z;
	}

	float operator[](int i) const
	{
		return ((float*)this)[i];
	}

	float& operator[](int i)
	{
		return ((float*)this)[i];
	}

	float Length()
	{
		float root;
		float sqsr = x*x + y*y + z*z;

		__asm
		{
			sqrtss xmm0, sqsr
			movss root, xmm0
		}

		return root;
	}

	float Length2D()
	{
		float root;
		float sqst = x*x + y*y;

		__asm
		{
			sqrtss xmm0, sqst
			movss root, xmm0
		}

		return root;
	}

	float LengthSqr()
	{
		return (x * x + y * y + z * z);
	}

	float Length2DSqr()
	{
		return (x * x + y * y);
	}

	bool operator==(const Vector& other) const
	{
		return (other.x == x) && (other.y == y) && (other.z == z);
	}

	bool operator!=(const Vector& other) const
	{
		return (other.x != x) || (other.y != y) || (other.z != z);
	}

	Vector& operator=(const Vector& other)
	{
		x = other.x;
		y = other.y;
		z = other.z;

		return *this;
	}

	Vector operator+(const Vector& other) const
	{
		Vector tmp;
		tmp.x = x + other.x;
		tmp.y = y + other.y;
		tmp.z = z + other.z;

		return tmp;
	}

	Vector operator-(const Vector& other) const
	{
		Vector tmp;
		tmp.x = x - other.x;
		tmp.y = y - other.y;
		tmp.z = z - other.z;

		return tmp;
	}

	Vector operator*(float fl) const
	{
		Vector tmp;
		tmp.x = x * fl;
		tmp.y = y * fl;
		tmp.z = z * fl;

		return tmp;
	}

	Vector operator/(float fl) const
	{
		Vector tmp;
		tmp.x = x / fl;
		tmp.y = y / fl;
		tmp.z = z / fl;

		return tmp;
	}

	Vector& operator+=(const Vector& other)
	{
		x += other.x;
		y += other.y;
		z += other.z;

		return *this;
	}

	Vector& operator-=(const Vector& other)
	{
		x -= other.x;
		y -= other.y;
		z -= other.z;

		return *this;
	}

	Vector& operator*=(float fl)
	{
		x *= fl;
		y *= fl;
		z *= fl;

		return *this;
	}

	Vector& operator/=(float fl)
	{
		x /= fl;
		y /= fl;
		z /= fl;

		return *this;
	}

	void Normalize()
	{
		float iradius = 1.f / (Length() + 1.192092896e-07F); //FLT_EPSILON

		x *= iradius;
		y *= iradius;
		z *= iradius;
	}

	Vector GetNormalized() const
	{
		Vector tmp = *this;
		tmp.Normalize();

		return tmp;
	}

	Vector Forward()
	{
		Vector vForward;

		AngleVectors(*this, &vForward);

		return vForward;
	}

	inline float Dot(const Vector& vOther) const
	{
		return DotProduct(*this, vOther);
	}

	float x, y, z;
};
      
__forceinline float SDK::DotProduct(const SDK::Vector& a, const SDK::Vector& b)
{
	return (a.x * b.x + a.y * b.y + a.z * b.z);
}

__forceinline void SDK::AngleVectors(Vector& rangAngle, Vector* pForward, Vector* pRight, Vector* pUp)
{
	constexpr float _DEG2RAD = 3.14159265358979323846f / 180.f;
	constexpr float cpi = _DEG2RAD;

	float flAngle, flAngle2, flAngle3;
	float SinRoll, SinPitch, SinYaw, CosRoll, CosPitch, CosYaw;

	Deg2Rad(rangAngle.y, flAngle);
	SinCos(flAngle, SinYaw, CosYaw);

	Deg2Rad(rangAngle.x, flAngle2);
	SinCos(flAngle2, SinPitch, CosPitch);

	Deg2Rad(rangAngle.z, flAngle3);
	SinCos(flAngle3, SinRoll, CosRoll);

	if (pForward != nullptr) pForward->Init(CosPitch*CosYaw, CosPitch*SinYaw, -SinPitch);
	if (pUp != nullptr) pUp->Init(CosRoll*SinPitch*CosYaw + -SinRoll*-SinYaw, CosRoll*SinPitch*SinYaw + -SinRoll*CosYaw, CosRoll*CosPitch);
	if (pRight != nullptr) pRight->Init(
		-1.f * SinRoll*SinPitch*CosYaw + -1.f * CosRoll*-SinYaw,
		-1.f * SinRoll*SinPitch*SinYaw + -1.f * CosRoll*CosYaw,
		-1.f * SinRoll*CosPitch
	);
}

#endif