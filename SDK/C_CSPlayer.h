#ifndef __HEADER_CCSPLAYER__
#define __HEADER_CCSPLAYER__
#pragma once

#include "C_BasePlayer.h"

namespace SDK
{
	class CCSPlayerAnimState;
	class ICSPlayerAnimStateHelpers;

	class C_CSPlayer;
}

class SDK::C_CSPlayer : public C_BasePlayer
{
public:
	CCSPlayerAnimState* GetAnimState()
	{
		return *(CCSPlayerAnimState**)((uint32_t)this + Offsets::C_CSPlayer::m_pAnimState);
	}

	void SetAnimState(CCSPlayerAnimState* state)
	{
		*(CCSPlayerAnimState**)((uint32_t)this + Offsets::C_CSPlayer::m_pAnimState) = state;
	}

	Vector& GetEyeAngles()
	{
		return *(Vector*)((uint32_t)this + Offsets::C_CSPlayer::m_angEyeAngles);
	}

	ICSPlayerAnimStateHelpers* GetAnimStateHelpers()
	{
		return (ICSPlayerAnimStateHelpers*)((uint32_t)this + Offsets::C_CSPlayer::m_AnimStateHelpers);
	}

	float GetLowerBodyYaw()
	{
		return *(float*)((uint32_t)this + Offsets::C_CSPlayer::m_flLowerBodyYawTarget);
	}

	bool HasHelmet()
	{
		return *(bool*)((uint32_t)this + Offsets::C_CSPlayer::m_bHasHelmet);
	}

	bool HasHeavyArmor()
	{
		return *(bool*)((uint32_t)this + Offsets::C_CSPlayer::m_bHasHeavyArmor);
	}

	int GetArmorValue()
	{
		return *(int*)((uint32_t)this + Offsets::C_CSPlayer::m_ArmorValue);
	}
};

#include "CCSPlayerAnimState.h"

#endif