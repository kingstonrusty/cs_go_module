#ifndef __HEADER_IENGINETRACE__
#define __HEADER_IENGINETRACE__
#pragma once

#include "CGlobalVarsBase.h"

namespace SDK
{
	namespace Interfaces
	{
		extern CGlobalVarsBase* Globals;
	}
}

namespace SDK
{
	class IEngineTrace;

	class VectorAligned : public Vector
	{
	public:
		VectorAligned() { x = y = z = 0.0f; }
		VectorAligned(const Vector& v) { x = v.x; y = v.y; z = v.z; }
		float w;
	};

	class Ray_t
	{
	public:
		VectorAligned  m_Start;
		VectorAligned  m_Delta;
		VectorAligned  m_StartOffset;
		VectorAligned  m_Extents;
		const matrix3x4 *m_pWorldAxisTransform;
		bool	m_IsRay;
		bool	m_IsSwept;

		Ray_t() : m_pWorldAxisTransform(nullptr) {}

		void Init(Vector const& start, Vector const& end)
		{
			m_Delta = end - start;

			m_IsSwept = (m_Delta.LengthSqr() != 0);

			m_Extents.Init(0.f, 0.f, 0.f);
			m_pWorldAxisTransform = nullptr;
			m_IsRay = true;

			// Offset m_Start to be in the center of the box...
			m_StartOffset.Init(0.f, 0.f, 0.f);
			m_Start = start;
		}

		void Init(Vector const& start, Vector const& end, Vector const& mins, Vector const& maxs)
		{
			m_Delta = end - start;

			m_pWorldAxisTransform = nullptr;
			m_IsSwept = (m_Delta.LengthSqr() != 0);

			m_Extents = maxs - mins;
			m_Extents *= 0.5f;
			m_IsRay = (m_Extents.LengthSqr() < 1e-6);

			// Offset m_Start to be in the center of the box...
			m_StartOffset = mins + maxs;
			m_StartOffset *= 0.5f;
			m_Start = start + m_StartOffset;
			m_StartOffset *= -1.0f;
		}
		// compute inverse delta
		Vector InvDelta() const
		{
			Vector vecInvDelta;
			for (int iAxis = 0; iAxis < 3; ++iAxis)
			{
				if (m_Delta[iAxis] != 0.0f)
				{
					vecInvDelta[iAxis] = 1.0f / m_Delta[iAxis];
				}
				else
				{
					vecInvDelta[iAxis] = 3.402823466e+38F;
				}
			}
			return vecInvDelta;
		}
	};

	struct cplane_t
	{
		Vector normal;
		float dist;
		uint8_t type;
		uint8_t signBits;
		uint8_t pad[2];
	};

	struct csurface_t 
	{
		const char* name;
		short surfaceProps;
		unsigned short flags;
	};

	class CBaseTrace
	{
	public:

		// these members are aligned!!
		Vector			startpos;				// start position
		Vector			endpos;					// final position
		cplane_t		plane;					// surface normal at impact

		float			fraction;				// time completed, 1.0 = didn't hit anything

		int				contents;				// contents on other side of surface hit
		unsigned short	dispFlags;				// displacement flags for marking surfaces with data

		bool			allsolid;				// if true, plane is not valid
		bool			startsolid;				// if true, the initial point was in a solid area

	};

	class trace_t : public CBaseTrace 
	{
	public:
		float			fractionleftsolid;	// time we left a solid, only valid if we started in solid
		csurface_t		surface;			// surface hit (impact surface)
		int				hitgroup;			// 0 == generic, non-zero is specific body part
		short			physicsbone;		// physics bone hit by trace in studio
		unsigned short	worldSurfaceIndex;	// Index of the msurface2_t, if applicable
		IClientEntity *m_pEnt;
		// NOTE: this member is overloaded.
		// If hEnt points at the world entity, then this is the static prop index.
		// Otherwise, this is the hitbox index.
		int			hitbox;					// box hit by trace in studio
	};

	enum TraceType_t 
	{
		TRACE_EVERYTHING = 0,
		TRACE_WORLD_ONLY,
		TRACE_ENTITIES_ONLY,
		TRACE_EVERYTHING_FILTER_PROPS,
	};

	class ITraceFilter 
	{
	public:
		virtual bool ShouldHitEntity(SDK::C_BaseEntity *pEntity, int contentsMask) = 0;
		virtual TraceType_t	GetTraceType() const = 0;
	};

	class CTraceFilterSimple : public ITraceFilter
	{
	public:
		CTraceFilterSimple()
		{

		}

		CTraceFilterSimple(SDK::C_BaseEntity* pSkip)
		{
			this->pSkip = pSkip;
		}

		bool ShouldHitEntity(SDK::C_BaseEntity* pEntityHandle, int contentsMask) override
		{
			return !(pEntityHandle == pSkip);
		}

		virtual TraceType_t GetTraceType() const override 
		{
			return TRACE_EVERYTHING;
		}

		SDK::C_BaseEntity* pSkip;
	};

	class CTraceFilterSkipTwoEntities : public ITraceFilter
	{
	public:
		CTraceFilterSkipTwoEntities(SDK::C_BaseEntity *pPassEnt1, SDK::C_BaseEntity *pPassEnt2)
		{
			passentity1 = pPassEnt1;
			passentity2 = pPassEnt2;
		}

		virtual bool ShouldHitEntity(SDK::C_BaseEntity *pEntityHandle, int contentsMask) override
		{
			return !(pEntityHandle == passentity1 || pEntityHandle == passentity2);
		}

		virtual TraceType_t    GetTraceType() const override
		{
			return TRACE_EVERYTHING;
		}

		SDK::C_BaseEntity *passentity1;
		SDK::C_BaseEntity *passentity2;
	};

	class CTraceFilterSkipPlayers : public ITraceFilter
	{
	public:
		virtual bool ShouldHitEntity(SDK::C_BaseEntity* pEntityHandle, int contentsMask) override
		{
			if (pEntityHandle->GetNetworkable()->EntIndex() >= 1 && pEntityHandle->GetNetworkable()->EntIndex() <= SDK::Interfaces::Globals->maxClients())
			{
				return false;
			}

			return true;
		}

		virtual TraceType_t    GetTraceType() const override
		{
			return TRACE_EVERYTHING;
		}
	};

	enum
	{
		CONTENTS_EMPTY = 0,
		CONTENTS_SOLID = (1 << 0),
		CONTENTS_WINDOW = (1 << 1),
		CONTENTS_AUX = (1 << 2),
		CONTENTS_GRATE = (1 << 3),
		CONTENTS_SLIME = (1 << 4),
		CONTENTS_WATER = (1 << 5),
		CONTENTS_BLOCKLOS = (1 << 6),
		CONTENTS_OPAQUE = (1 << 7),
		CONTENTS_TESTFOGVOLUME = (1 << 8),
		CONTENTS_UNUSED = (1 << 9),
		CONTENTS_UNUSED6 = (1 << 10),
		CONTENTS_TEAM1 = (1 << 11),
		CONTENTS_TEAM2 = (1 << 12),
		CONTENTS_IGNORE_NODRAW_OPAQUE = (1 << 13),
		CONTENTS_MOVEABLE = (1 << 14),
		CONTENTS_AREAPORTAL = (1 << 15),
		CONTENTS_PLAYERCLIP = (1 << 16),
		CONTENTS_MONSTERCLIP = (1 << 17),
		CONTENTS_CURRENT_0 = (1 << 18),
		CONTENTS_CURRENT_90 = (1 << 19),
		CONTENTS_CURRENT_180 = (1 << 20),
		CONTENTS_CURRENT_270 = (1 << 21),
		CONTENTS_CURRENT_UP = (1 << 22),
		CONTENTS_CURRENT_DOWN = (1 << 23),
		CONTENTS_ORIGIN = (1 << 24),
		CONTENTS_MONSTER = (1 << 25),
		CONTENTS_DEBRIS = (1 << 26),
		CONTENTS_DETAIL = (1 << 27),
		CONTENTS_TRANSLUCENT = (1 << 28),
		CONTENTS_LADDER = (1 << 29),
		CONTENTS_HITBOX = (1 << 30),

		LAST_VISIBLE_CONTENTS = CONTENTS_OPAQUE,
		ALL_VISIBLE_CONTENTS = (LAST_VISIBLE_CONTENTS | (LAST_VISIBLE_CONTENTS - 1))
	};

	enum
	{
		MASK_ALL = 0xFFFFFFFF,
		MASK_SOLID = (CONTENTS_SOLID | CONTENTS_MOVEABLE | CONTENTS_WINDOW | CONTENTS_MONSTER | CONTENTS_GRATE),
		MASK_PLAYERSOLID = (CONTENTS_SOLID | CONTENTS_MOVEABLE | CONTENTS_PLAYERCLIP | CONTENTS_WINDOW | CONTENTS_MONSTER | CONTENTS_GRATE),
		MASK_NPCSOLID = (CONTENTS_SOLID | CONTENTS_MOVEABLE | CONTENTS_MONSTERCLIP | CONTENTS_WINDOW | CONTENTS_MONSTER | CONTENTS_GRATE),
		MASK_WATER = (CONTENTS_WATER | CONTENTS_MOVEABLE | CONTENTS_SLIME),
		MASK_OPAQUE = (CONTENTS_SOLID | CONTENTS_MOVEABLE | CONTENTS_OPAQUE),
		MASK_OPAQUE_AND_NPCS = (CONTENTS_SOLID | CONTENTS_MOVEABLE | CONTENTS_OPAQUE | CONTENTS_MONSTER),
		MASK_BLOCKLOS = (CONTENTS_SOLID | CONTENTS_MOVEABLE | CONTENTS_BLOCKLOS),
		MASK_BLOCKLOS_AND_NPCS = (CONTENTS_SOLID | CONTENTS_MOVEABLE | CONTENTS_BLOCKLOS | CONTENTS_MONSTER),
		MASK_VISIBLE = (CONTENTS_SOLID | CONTENTS_MOVEABLE | CONTENTS_OPAQUE | CONTENTS_IGNORE_NODRAW_OPAQUE),
		MASK_VISIBLE_AND_NPCS = (CONTENTS_SOLID | CONTENTS_MOVEABLE | CONTENTS_OPAQUE | CONTENTS_MONSTER | CONTENTS_IGNORE_NODRAW_OPAQUE),
		MASK_SHOT = (CONTENTS_SOLID | CONTENTS_MOVEABLE | CONTENTS_MONSTER | CONTENTS_WINDOW | CONTENTS_DEBRIS | CONTENTS_HITBOX),
		MASK_SHOT_HULL = (CONTENTS_SOLID | CONTENTS_MOVEABLE | CONTENTS_MONSTER | CONTENTS_WINDOW | CONTENTS_DEBRIS | CONTENTS_GRATE),
		MASK_SHOT_PORTAL = (CONTENTS_SOLID | CONTENTS_MOVEABLE | CONTENTS_WINDOW | CONTENTS_MONSTER),
		MASK_SOLID_BRUSHONLY = (CONTENTS_SOLID | CONTENTS_MOVEABLE | CONTENTS_WINDOW | CONTENTS_GRATE),
		MASK_PLAYERSOLID_BRUSHONLY = (CONTENTS_SOLID | CONTENTS_MOVEABLE | CONTENTS_WINDOW | CONTENTS_PLAYERCLIP | CONTENTS_GRATE),
		MASK_NPCSOLID_BRUSHONLY = (CONTENTS_SOLID | CONTENTS_MOVEABLE | CONTENTS_WINDOW | CONTENTS_MONSTERCLIP | CONTENTS_GRATE),
		MASK_NPCWORLDSTATIC = (CONTENTS_SOLID | CONTENTS_WINDOW | CONTENTS_MONSTERCLIP | CONTENTS_GRATE),
		MASK_SPLITAREAPORTAL = (CONTENTS_WATER | CONTENTS_SLIME),
		MASK_CURRENT = (CONTENTS_CURRENT_0 | CONTENTS_CURRENT_90 | CONTENTS_CURRENT_180 | CONTENTS_CURRENT_270 | CONTENTS_CURRENT_UP | CONTENTS_CURRENT_DOWN),
		MASK_DEADSOLID = (CONTENTS_SOLID | CONTENTS_PLAYERCLIP | CONTENTS_WINDOW | CONTENTS_GRATE)
	};

	struct surfacephysicsparams_t
	{
		float    friction;
		float    elasticity;
		float    density;
		float    thickness;
		float    dampening;
	};

	struct surfaceaudioparams_t
	{
		float    reflectivity;             // like elasticity, but how much sound should be reflected by this surface
		float    hardnessFactor;           // like elasticity, but only affects impact sound choices
		float    roughnessFactor;          // like friction, but only affects scrape sound choices   
		float    roughThreshold;           // surface roughness > this causes "rough" scrapes, < this causes "smooth" scrapes
		float    hardThreshold;            // surface hardness > this causes "hard" impacts, < this causes "soft" impacts
		float    hardVelocityThreshold;    // collision velocity > this causes "hard" impacts, < this causes "soft" impacts   
		float    highPitchOcclusion;       //a value betweeen 0 and 100 where 0 is not occluded at all and 100 is silent (except for any additional reflected sound)
		float    midPitchOcclusion;
		float    lowPitchOcclusion;
	};

	struct surfacesoundnames_t
	{
		unsigned short    walkStepLeft;
		unsigned short    walkStepRight;
		unsigned short	  runStepLeft;
		unsigned short	  runStepRight;
		unsigned short    impactSoft;
		unsigned short    impactHard;
		unsigned short    scrapeSmooth;
		unsigned short    scrapeRough;
		unsigned short    bulletImpact;
		unsigned short    rolling;
		unsigned short    breakSound;
		unsigned short    strainSound;
	};

	struct surfacegameprops_t
	{
	public:
		float maxSpeedFactor;
		float jumpFactor;
		float flPenetrationModifier;
		float flDamageModifier;
		unsigned short material;
		uint8_t climbable;
		uint8_t pad00[0x4];

	};

	struct surfacedata_t
	{
		surfacephysicsparams_t    physics;
		surfaceaudioparams_t    audio;
		surfacesoundnames_t        sounds;
		surfacegameprops_t        game;
	};
}

class SDK::IEngineTrace
{
public:
	int GetPointContents(SDK::Vector& point, int a3, void** a4)
	{
		return GET_VFUNC(int(__thiscall*)(IEngineTrace*, SDK::Vector&, int, void**), this, 0)(this, point, a3, a4);
	}

	void TraceRay(const Ray_t& ray, unsigned int mask, ITraceFilter* filter, trace_t* trace)
	{
		return GET_VFUNC(void(__thiscall*)(IEngineTrace*, const Ray_t&, unsigned int, ITraceFilter*, trace_t*), this, 5)(this, ray, mask, filter, trace);
	}
};

#endif