#ifndef __HEADER_SDK__
#define __HEADER_SDK__
#pragma once

#define MAX_PLAYERS 64

#include "Offsets.h"

#include "Color.h"
#include "Vector.h"
#include "matrix3x4.h"

#include "IClientEntity.h"
#include "C_BaseEntity.h"
#include "C_BaseAnimating.h"
#include "C_BasePlayer.h"
#include "C_CSPlayer.h"
#include "C_BaseCombatWeapon.h"
#include "C_WeaponCSBase.h"

#include "CGlobalVarsBase.h"
#include "IBaseClientDLL.h"
#include "IClientEntityList.h"
#include "IClientMode.h"
#include "ICvar.h"
#include "IEngineTrace.h"
#include "IEngineVGui.h"
#include "IGameMovement.h"
#include "IInput.h"
#include "IPhysicsSurfaceProps.h"
#include "IPrediction.h"
#include "ISurface.h"
#include "IVEngineClient.h"

#include "Imports.h"
#include "Interfaces.h"

namespace SDK
{
	extern void Initialize();

	__forceinline int TIME_TO_TICKS(float dt)
	{
		return (int)(0.5f + (float)dt / SDK::Interfaces::Globals->interval_per_tick());
	}

	__forceinline float TICKS_TO_TIME(int t)
	{
		return t * SDK::Interfaces::Globals->interval_per_tick();
	}

	__forceinline unsigned int MD5_PseudoRandom(unsigned int seed)
	{
		return ((unsigned int(*)(unsigned int))Offsets::Other::MD5_PseudoRandom)(seed);
	}
}

#endif