#ifndef __HEADER_ICVAR__
#define __HEADER_ICVAR__
#pragma once

namespace SDK
{
	class ConVar;

	class ICvar;
}

class SDK::ConVar
{
public:
	int& m_iInt()
	{
		return *(int*)((uint32_t)this + 48);
	}

	float& m_flValue()
	{
		return *(float*)((uint32_t)this + 44);
	}

	int ToInt()
	{
		return (int)this;
	}

	void SetInt(int value)
	{
		m_iInt() = value ^ ToInt();
	}

	int GetInt()
	{
		return m_iInt() ^ ToInt();
	}

	void SetFloat(float value)
	{
		m_flValue() = value;

		*(int*)((uint32_t)this + 44) ^= ToInt();
	}

	float GetFloat()
	{
		int iValue = *(int*)((uint32_t)this + 44) ^ ToInt();
		return *(float*)&iValue;
	}
};

class SDK::ICvar
{
public:
	ConVar* FindVar(const char* name)
	{
		return GET_VFUNC(ConVar*(__thiscall*)(ICvar*, const char*), this, 15)(this, name);
	}
};

#endif