#ifndef __HEADER_IBASECLIENTDLL__
#define __HEADER_IBASECLIENTDLL__
#pragma once

namespace SDK
{
	enum ClientFrameStage_t
	{
		FRAME_UNDEFINED = -1,
		FRAME_START,
		FRAME_NET_UPDATE_START,
		FRAME_NET_UPDATE_POSTDATAUPDATE_START,
		FRAME_NET_UPDATE_POSTDATAUPDATE_END,
		FRAME_NET_UPDATE_END,
		FRAME_RENDER_START,
		FRAME_RENDER_END
	};

	class DVariant
	{
	public:
		union
		{
			float	m_Float;
			long	m_Int;
			char	*m_pString;
			void	*m_pData;
			float	m_Vector[3];
			long long m_Int64;
		};
	};

	class CRecvTable;
	class CRecvProxyData;
	typedef void(__cdecl *RecvVarProxyFn)(const CRecvProxyData *pData, void *pStruct, void *pOut);

	class CRecvProp
	{
	public:
		const char* m_pVarName;
		int	m_RecvType;
		int m_Flags;
		int m_StringBufferSize;
		bool m_bInsideArray;
		const void *m_pExtraData;
		CRecvProp* m_pArrayProp;
		void* m_ArrayLengthProxy;
		RecvVarProxyFn m_ProxyFn;
		void* m_DataTableProxyFn;
		CRecvTable* m_pDataTable;
		int m_Offset;
		int m_ElementStride;
		int m_nElements;
		const char* m_pParentArrayPropName;
	};

	class CRecvProxyData
	{
	public:
		const CRecvProp	*m_pRecvProp;
		DVariant		m_Value;
		int				m_iElement;
		int				m_ObjectID;
	};

	class CRecvTable
	{
	public:
		CRecvProp* m_pProps;
		int m_nProps;
		void* m_pDecoder;
		const char* m_pNetTableName;
		bool			m_bInitialized;
		bool			m_bInMainList;
	};

	class ClientClass
	{
	public:
		void* m_pCreateFn;
		void* m_pCreateEventFn;
		char* m_pNetworkName;
		CRecvTable* m_pRecvTable;
		ClientClass* m_pNext;
		int m_ClassID;
	};

	class IBaseClientDLL;
}

class SDK::IBaseClientDLL
{
public:
	ClientClass* GetAllClasses()
	{
		return GET_VFUNC(ClientClass*(__thiscall*)(IBaseClientDLL*), this, 8)(this);
	}

	void FrameStageNotify(ClientFrameStage_t stage)
	{
		return GET_VFUNC(void(__thiscall*)(IBaseClientDLL*, ClientFrameStage_t), this, 36)(this, stage);
	}
};

#endif