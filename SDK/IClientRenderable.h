#ifndef __HEADER_ICLIENTRENDERABLE__
#define __HEADER_ICLIENTRENDERABLE__
#pragma once

namespace SDK
{
	class IClientRenderable;
}

class SDK::IClientRenderable
{
public:
	bool SetupBones(matrix3x4* pBoneToWorld, int maxbones, int bonemask, float curtime)
	{
		return GET_VFUNC(bool(__thiscall*)(IClientRenderable*, matrix3x4*, int, int, float), this, 13)(this, pBoneToWorld, maxbones, bonemask, curtime);
	}
};

#endif