#ifndef __HEADER_SDKIMPORTS__
#define __HEADER_SDKIMPORTS__
#pragma once

namespace SDK
{
	typedef void(*RandomSeed_t)(unsigned int);
	typedef float(*RandomFloat_t)(float, float);
	typedef int(*RandomInt_t)(int, int);
	typedef void(*Msg_t)(const char*, ...);
	typedef void*(*CreateInterface_t)(const char*, int*);

	extern RandomSeed_t RandomSeed;
	extern RandomFloat_t RandomFloat;
	extern RandomInt_t RandomInt;
	extern Msg_t Msg;
}

#endif