#ifndef __HEADER_CBASEPLAYER__
#define __HEADER_CBASEPLAYER__
#pragma once

#include "C_BaseAnimating.h"
#include "IInput.h"

namespace SDK
{
	typedef void* EHANDLE;

	class C_BasePlayer;
}

class SDK::C_BasePlayer : public C_BaseAnimating
{
public:
	Vector& GetViewOffset()
	{
		return *(Vector*)((uint32_t)this + Offsets::C_BasePlayer::m_vecViewOffset);
	}

	char GetLifeState()
	{
		return *(char*)((uint32_t)this + Offsets::C_BasePlayer::m_lifeState);
	}

	bool IsAlive()
	{
		return (GetLifeState() == 0/*LIFE_ALIVE*/);
	}

	int GetFlags()
	{
		return *(int*)((uint32_t)this + Offsets::C_BasePlayer::m_fFlags);
	}

	Vector& GetVelocity()
	{
		return *(Vector*)((uint32_t)this + Offsets::C_BasePlayer::m_vecVelocity);
	}

	EHANDLE GetActiveWeapon()
	{
		return *(EHANDLE*)((uint32_t)this + Offsets::C_BasePlayer::m_hActiveWeapon);
	}

	int GetTickBase()
	{
		return *(int*)((uint32_t)this + Offsets::C_BasePlayer::m_nTickBase);
	}

	void SetTickBase(int base)
	{
		*(int*)((uint32_t)this + Offsets::C_BasePlayer::m_nTickBase) = base;
	}

	static C_BasePlayer* GetPredictionPlayer()
	{
		return *(SDK::C_BasePlayer**)Offsets::C_BasePlayer::m_pPredictionPlayer;
	}

	static void SetPredictionPlayer(C_BasePlayer* pPlayer)
	{
		*(SDK::C_BasePlayer**)Offsets::C_BasePlayer::m_pPredictionPlayer = pPlayer;
	}

	static int GetPredictionRandomSeed()
	{
		return *(int*)Offsets::C_BasePlayer::m_nPredictionRandomSeed;
	}

	static void SetPredictionRandomSeed(int seed)
	{
		*(int*)Offsets::C_BasePlayer::m_nPredictionRandomSeed = seed;
	}

	CUserCmd* GetCurrentCommand()
	{
		return *(CUserCmd**)((uint32_t)this + Offsets::C_BasePlayer::m_pCurrentCommand);
	}

	void SetCurrentCommand(CUserCmd* cmd)
	{
		*(CUserCmd**)((uint32_t)this + Offsets::C_BasePlayer::m_pCurrentCommand) = cmd;
	}

	void PreThink()
	{
		return GET_VFUNC(void(__thiscall*)(C_BasePlayer*), this, 307)(this);
	}

	void PostThink_NoIdea1()
	{
		return GET_VFUNC(void(__thiscall*)(C_BasePlayer*), this, 329)(this);
	}

	void SimulatePlayerSimulatedEntities()
	{
		return ((void(__thiscall*)(C_BasePlayer*))Offsets::C_BasePlayer::SimulatePlayerSimulatedEntities)(this);
	}

	void PostThinkVPhysics()
	{
		return ((void(__thiscall*)(C_BasePlayer*))Offsets::C_BasePlayer::PostThinkVPhysics)(this);
	}

	float GetFallVelocity()
	{
		return *(float*)((uint32_t)this + Offsets::C_BasePlayer::m_flFallVelocity);
	}

	void SetFallVelocity(float vel)
	{
		*(float*)((uint32_t)this + Offsets::C_BasePlayer::m_flFallVelocity) = vel;
	}
};

#endif