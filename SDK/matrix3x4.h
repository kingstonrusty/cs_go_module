#ifndef __HEADER_MATRIX3x4__
#define __HEADER_MATRIX3x4__
#pragma once

namespace SDK
{
	class matrix3x4;
}

class SDK::matrix3x4
{
public:
private:
	float f[3][4];
};

#endif