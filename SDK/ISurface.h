#ifndef __HEADER_ISURFACE__
#define __HEADER_ISURFACE__
#pragma once

#include "Color.h"

namespace SDK
{
	typedef uint32_t HFont;

	enum EFontFlags
	{
		FONTFLAG_NONE,
		FONTFLAG_ITALIC = 0x001,
		FONTFLAG_UNDERLINE = 0x002,
		FONTFLAG_STRIKEOUT = 0x004,
		FONTFLAG_SYMBOL = 0x008,
		FONTFLAG_ANTIALIAS = 0x010,
		FONTFLAG_GAUSSIANBLUR = 0x020,
		FONTFLAG_ROTARY = 0x040,
		FONTFLAG_DROPSHADOW = 0x080,
		FONTFLAG_ADDITIVE = 0x100,
		FONTFLAG_OUTLINE = 0x200,
		FONTFLAG_CUSTOM = 0x400,		// custom generated font - never fall back to asian compatibility mode
		FONTFLAG_BITMAP = 0x800,		// compiled bitmap font - no fallbacks
	};

	enum FontDrawType_t
	{
		FONT_DRAW_DEFAULT = 0,
		FONT_DRAW_NONADDITIVE,
		FONT_DRAW_ADDITIVE,
		FONT_DRAW_TYPE_COUNT = 2,
	};

	class ISurface;
}

class SDK::ISurface
{
public:
	void DrawSetColor(Color clr)
	{
		return GET_VFUNC(void(__thiscall*)(ISurface*, Color), this, 14)(this, clr);
	}

	void DrawFilledRect(int x1, int y1, int x2, int y2)
	{
		return GET_VFUNC(void(__thiscall*)(ISurface*, int, int, int, int), this, 16)(this, x1, y1, x2, y2);
	}

	void DrawOutlinedRect(int x1, int y1, int x2, int y2)
	{
		return GET_VFUNC(void(__thiscall*)(ISurface*, int, int, int, int), this, 18)(this, x1, y1, x2, y2);
	}

	void DrawLine(int x1, int y1, int x2, int y2)
	{
		return GET_VFUNC(void(__thiscall*)(ISurface*, int, int, int, int), this, 19)(this, x1, y1, x2, y2);
	}

	void DrawSetTextFont(HFont font)
	{
		return GET_VFUNC(void(__thiscall*)(ISurface*, HFont), this, 23)(this, font);
	}

	void DrawSetTextColor(Color col)
	{
		return GET_VFUNC(void(__thiscall*)(ISurface*, Color), this, 24)(this, col);
	}

	void DrawSetTextPos(int x, int y)
	{
		return GET_VFUNC(void(__thiscall*)(ISurface*, int, int), this, 26)(this, x, y);
	}

	void DrawPrintText(const wchar_t* text, int len, FontDrawType_t drawType = FONT_DRAW_DEFAULT)
	{
		return GET_VFUNC(void(__thiscall*)(ISurface*, const wchar_t*, int, FontDrawType_t), this, 28)(this, text, len, drawType);
	}

	void GetScreenSize(int& w, int& h)
	{
		return GET_VFUNC(void(__thiscall*)(ISurface*, int&, int&), this, 44)(this, w, h);
	}

	HFont CreateFont()
	{
		return GET_VFUNC(HFont(__thiscall*)(ISurface*), this, 71)(this);
	}

	bool SetFontGlyphSet(HFont font, const char* windowsFontName, int tall, int weight, int blur, int scanlines, int flags, int nRangeMin = 0, int nRangeMax = 0)
	{
		return GET_VFUNC(bool(__thiscall*)(ISurface*, HFont, const char*, int, int, int, int, int, int, int), this, 72)(this, font, windowsFontName, tall, weight, blur, scanlines, flags, nRangeMin, nRangeMax);
	}

	void GetTextSize(HFont font, const wchar_t* text, int& wide, int& tall)
	{
		return GET_VFUNC(void(__thiscall*)(ISurface*, HFont, const wchar_t*, int&, int&), this, 79)(this, font, text, wide, tall);
	}

	void SurfaceGetCursorPos(int& x, int& y)
	{
		return GET_VFUNC(void(__thiscall*)(ISurface*, int&, int&), this, 100)(this, x, y);
	}

	void SurfaceSetCursorPos(int x, int y)
	{
		return GET_VFUNC(void(__thiscall*)(ISurface*, int, int), this, 101)(this, x, y);
	}

	void StartDrawing()
	{
		typedef void(__thiscall* StartDrawing_t)(ISurface*);

		return ((StartDrawing_t)Offsets::ISurface::StartDrawing)(this);
	}

	void FinishDrawing()
	{
		typedef void(__thiscall* FinishDrawing_t)(ISurface*);

		return ((FinishDrawing_t)Offsets::ISurface::FinishDrawing)(this);
	}
};

#endif