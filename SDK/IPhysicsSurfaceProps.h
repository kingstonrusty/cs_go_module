#ifndef __HEADER_IPHYSICSSURFACEPROPS__
#define __HEADER_IPHYSICSSURFACEPROPS__
#pragma once

#include "IEngineTrace.h"

namespace SDK
{
	class IPhysicsSurfaceProps;
}

class SDK::IPhysicsSurfaceProps
{
public:
	surfacedata_t* GetSurfaceData(int surfaceDataIndex)
	{
		return GET_VFUNC(surfacedata_t*(__thiscall*)(IPhysicsSurfaceProps*, int), this, 5)(this, surfaceDataIndex);
	}
};

#endif