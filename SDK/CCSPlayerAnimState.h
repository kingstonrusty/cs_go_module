#ifndef __HEADER_CCSPLAYERANIMSTATE__
#define __HEADER_CCSPLAYERANIMSTATE__
#pragma once

#include "C_CSPlayer.h"

namespace SDK
{
	class ICSPlayerAnimStateHelpers;

	class CCSPlayerAnimState;
}

#define LODWORD(x) (*((uint32_t*)&(x))) // low dword
#define BYTEn(x, n) (*((uint8_t*)&(x)+n))
#define BYTE4(x) BYTEn(x, 4)

class SDK::CCSPlayerAnimState
{
public:
	void Setup(C_CSPlayer* pPlayer)
	{
		int64_t v10 = 0x0000000000000000i64;
		LODWORD(v10) = 0;
		BYTE4(v10) = 1;

		((void(__thiscall*)(CCSPlayerAnimState*))Offsets::CCSPlayerAnimState::SetupAnimState)(this);

		*(C_CSPlayer**)((uint32_t)this + 0x148) = pPlayer;
		*(uint32_t*)((uint32_t)this + 0x04) = 0x42700000; //m_flWalkSpeed (60)
		*(uint32_t*)((uint32_t)this + 0x08) = 0x42B40000; //m_flRunSpeed (90)
		*(uint32_t*)((uint32_t)this + 0x0C) = 0x42DC0000; //m_flSprintSpeed (110)
		*(uint32_t*)((uint32_t)this + 0x10) = 0x41C80000; //m_flBodyYawRate (25)
		*(ICSPlayerAnimStateHelpers**)((uint32_t)this + 0x154) = pPlayer->GetAnimStateHelpers();
		*(int64_t*)((uint32_t)this + 0x14) = v10;
		*(C_CSPlayer**)((uint32_t)this + 0x1C) = pPlayer;

		ClearAnimationState();
	}

	void Update(float y, float x)
	{
		__asm
		{
			mov ecx, this
			mov eax, [ecx]

			sub esp, 8

			movss xmm0, x
			movss [esp + 4], xmm0

			movss xmm0, y
			movss [esp], xmm0

			call dword ptr [eax + 4]
		}
	}

	void ClearAnimationState()
	{
		GET_VFUNC(void(__thiscall*)(CCSPlayerAnimState*), this, 3)(this);
	}

	C_CSPlayer* GetOuter()
	{
		return *(C_CSPlayer**)((uint32_t)this + 0x1C);
	}
private:
	uint8_t __pad[0x21C];
};

#endif