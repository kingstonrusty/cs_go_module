#ifndef __HEADER_ICLIENTENTITYLIST__
#define __HEADER_ICLIENTENTITYLIST__
#pragma once

#include "IClientEntity.h"

namespace SDK
{
	typedef void* EHANDLE;

	class IClientEntityList;
}

class SDK::IClientEntityList
{
public:
	IClientEntity* GetClientEntity(int idx)
	{
		return GET_VFUNC(IClientEntity*(__thiscall*)(IClientEntityList*, int), this, 3)(this, idx);
	}

	IClientEntity* GetClientEntityFromHandle(EHANDLE handle)
	{
		return GET_VFUNC(IClientEntity*(__thiscall*)(IClientEntityList*, EHANDLE), this, 4)(this, handle);
	}

	int GetHighestEntityIndex()
	{
		return GET_VFUNC(int(__thiscall*)(IClientEntityList*), this, 8)(this);
	}
};

#endif