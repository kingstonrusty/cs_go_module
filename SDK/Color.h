#ifndef __HEADER_COLOR__
#define __HEADER_COLOR__
#pragma once

namespace SDK
{
	class Color;
}

class SDK::Color
{
public:
	Color(uint8_t r, uint8_t g, uint8_t b, uint8_t a = 255)
	{
		this->r = r;
		this->g = g;
		this->b = b;
		this->a = a;
	}

	uint8_t r, g, b, a;
};

#endif