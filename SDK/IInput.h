#ifndef __HEADER_IINPUT__
#define __HEADER_IINPUT__
#pragma once

namespace SDK
{
	class CUserCmd;

	enum ButtonCode_t
	{
		KEY_0 = 1,
		KEY_1,
		KEY_2,
		KEY_3,
		KEY_4,
		KEY_5,
		KEY_6,
		KEY_7,
		KEY_8,
		KEY_9,
		KEY_A,
		KEY_B,
		KEY_C,
		KEY_D,
		KEY_E,
		KEY_F,
		KEY_G,
		KEY_H,
		KEY_I,
		KEY_J,
		KEY_K,
		KEY_L,
		KEY_M,
		KEY_N,
		KEY_O,
		KEY_P,
		KEY_Q,
		KEY_R,
		KEY_S,
		KEY_T,
		KEY_U,
		KEY_V,
		KEY_W,
		KEY_X,
		KEY_Y,
		KEY_Z,
		KEY_PAD_0,
		KEY_PAD_1,
		KEY_PAD_2,
		KEY_PAD_3,
		KEY_PAD_4,
		KEY_PAD_5,
		KEY_PAD_6,
		KEY_PAD_7,
		KEY_PAD_8,
		KEY_PAD_9,
		KEY_PAD_DIVIDE,
		KEY_PAD_MULTIPLY,
		KEY_PAD_MINUS,
		KEY_PAD_PLUS,
		KEY_PAD_ENTER,
		KEY_PAD_DECIMAL,
		KEY_LBRACKET,
		KEY_RBRACKET,
		KEY_SEMICOLON,
		KEY_APOSTROPHE,
		KEY_BACKQUOTE,
		KEY_COMMA,
		KEY_PERIOD,
		KEY_SLASH,
		KEY_BACKSLASH,
		KEY_MINUS,
		KEY_EQUAL,
		KEY_ENTER,
		KEY_SPACE,
		KEY_BACKSPACE,
		KEY_TAB,
		KEY_CAPSLOCK,
		KEY_NUMLOCK,
		KEY_ESCAPE,
		KEY_SCROLLLOCK,
		KEY_INSERT,
		KEY_DELETE,
		KEY_HOME,
		KEY_END,
		KEY_PAGEUP,
		KEY_PAGEDOWN,
		KEY_BREAK,
		KEY_LSHIFT,
		KEY_RSHIFT,
		KEY_LALT,
		KEY_RALT,
		KEY_LCONTROL,
		KEY_RCONTROL,
		KEY_LWIN,
		KEY_RWIN,
		KEY_APP,
		KEY_UP,
		KEY_LEFT,
		KEY_DOWN,
		KEY_RIGHT,
		KEY_F1,
		KEY_F2,
		KEY_F3,
		KEY_F4,
		KEY_F5,
		KEY_F6,
		KEY_F7,
		KEY_F8,
		KEY_F9,
		KEY_F10,
		KEY_F11,
		KEY_F12,
		KEY_CAPSLOCKTOGGLE,
		KEY_NUMLOCKTOGGLE,
		KEY_SCROLLLOCKTOGGLE,
		MOUSE_LEFT,
		MOUSE_RIGHT,
		MOUSE_MIDDLE,
		MOUSE_4,
		MOUSE_5,
		MOUSE_WHEEL_UP,		// A fake button which is 'pressed' and 'released' when the wheel is moved up 
		MOUSE_WHEEL_DOWN,	// A fake button which is 'pressed' and 'released' when the wheel is moved down
		BUTTON_COUNT = MOUSE_WHEEL_DOWN - KEY_0 + 1,
	};

	enum InputEventType_t
	{
		IE_ButtonPressed = 0,	// m_nData contains a ButtonCode_t
		IE_ButtonReleased,		// m_nData contains a ButtonCode_t
		IE_ButtonDoubleClicked,	// m_nData contains a ButtonCode_t
		IE_AnalogValueChanged,	// m_nData contains an AnalogCode_t, m_nData2 contains the value

		IE_FirstSystemEvent = 100,
		IE_Quit = IE_FirstSystemEvent,
		IE_ControllerInserted,	// m_nData contains the controller ID
		IE_ControllerUnplugged,	// m_nData contains the controller ID

		IE_FirstVguiEvent = 1000,	// Assign ranges for other systems that post user events here
		IE_FirstAppEvent = 2000,
	};

	struct InputEvent_t
	{
		int m_nType;				// Type of the event (see InputEventType_t)
		int m_nTick;				// Tick on which the event occurred
		int m_nData;				// Generic 32-bit data, what it contains depends on the event
		int m_nData2;				// Generic 32-bit data, what it contains depends on the event
		int m_nData3;				// Generic 32-bit data, what it contains depends on the event
	};
}

class SDK::CUserCmd
{
public:
	int32_t& command_number()
	{
		return *(int32_t*)((uint32_t)this + 0x4);
	}

	int32_t& tick_count()
	{
		return *(int32_t*)((uint32_t)this + 0x8);
	}

	Vector& viewangles()
	{
		return *(Vector*)((uint32_t)this + 0xC);
	}

	float& forwardmove()
	{
		return *(float*)((uint32_t)this + 0x24);
	}

	float& sidemove()
	{
		return *(float*)((uint32_t)this + 0x28);
	}

	int32_t& buttons()
	{
		return *(int32_t*)((uint32_t)this + 0x30);
	}

	uint32_t& weaponselect()
	{
		return *(uint32_t*)((uint32_t)this + 0x38);
	}

	int32_t& weaponsubtype()
	{
		return *(int32_t*)((uint32_t)this + 0x3C);
	}

	int32_t& random_seed()
	{
		return *(int32_t*)((uint32_t)this + 0x40);
	}
};

#endif