#ifndef __HEADER_IENGINEVGUI__
#define __HEADER_IENGINEVGUI__
#pragma once

#include "IInput.h"

namespace SDK
{
	enum PaintMode_t
	{
		PAINT_UIPANELS = (1 << 0),
		PAINT_INGAMEPANELS = (1 << 1),
		PAINT_CURSOR = (1 << 2), // software cursor, if appropriate
	};

	class IEngineVGui;
}

class SDK::IEngineVGui
{
public:
	bool Key_Event(const InputEvent_t& event)
	{
		return GET_VFUNC(bool(__thiscall*)(IEngineVGui*, const InputEvent_t&), this, 10)(this, event);
	}

	void UpdateButtonState(const InputEvent_t& event)
	{
		return GET_VFUNC(void(__thiscall*)(IEngineVGui*, const InputEvent_t&), this, 12)(this, event);
	}

	void Paint(PaintMode_t mode)
	{
		return GET_VFUNC(void(__thiscall*)(IEngineVGui*, PaintMode_t), this, 14)(this, mode);
	}
};

#endif