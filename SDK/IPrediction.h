#ifndef __HEADER_IPREDICTION__
#define __HEADER_IPREDICTION__
#pragma once

#include "C_BasePlayer.h"
#include "IInput.h"
#include "IGameMovement.h"

namespace SDK
{
	class IMoveHelper;

	class IPrediction;
}

class SDK::IMoveHelper 
{
public:
	static IMoveHelper* GetPtr()
	{
		return (IMoveHelper*)Offsets::IMoveHelper::MoveHelperPtr;
	}

	void SetHost(C_BasePlayer* host)
	{
		return GET_VFUNC(void(__thiscall*)(IMoveHelper*, C_BasePlayer*), this, 1)(this, host);
	}
};

class SDK::IPrediction
{
public:
	void SetupMove(C_BasePlayer* player, CUserCmd* cmd, IMoveHelper* movehelper, CMoveData* movedata)
	{
		return GET_VFUNC(void(__thiscall*)(IPrediction*, C_BasePlayer*, CUserCmd*, IMoveHelper*, CMoveData*), this, 20)(this, player, cmd, movehelper, movedata);
	}

	void FinishMove(C_BasePlayer* player, CUserCmd* cmd, CMoveData* movedata)
	{
		return GET_VFUNC(void(__thiscall*)(IPrediction*, C_BasePlayer*, CUserCmd*, CMoveData*), this, 21)(this, player, cmd, movedata);
	}
};

#endif