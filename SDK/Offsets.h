#ifndef __HEADER_OFFSETS__
#define __HEADER_OFFSETS__
#pragma once

namespace SDK
{
	namespace Offsets
	{
		namespace ISurface
		{
			extern uint32_t StartDrawing;
			extern uint32_t FinishDrawing;
		}

		namespace C_BaseEntity
		{
			extern uint32_t SetAbsOrigin;
			extern uint32_t SetAbsAngles;
			extern uint32_t m_flSimulationTime;
			extern uint32_t m_flAnimTime;
			extern uint32_t m_vecOrigin;
			extern uint32_t RemoveFromInterpolationList;
			extern uint32_t m_iTeamNum;
			extern uint32_t IsBreakable;
			extern uint32_t PhysicsRunThink;
			extern uint32_t Think_NoIdea1;
		}

		namespace C_BaseAnimating
		{
			extern uint32_t m_fReadableBones;
			extern uint32_t m_fWriteableBones;
			extern uint32_t m_flLastBoneSetupTime;
			extern uint32_t m_iMostRecentModelBoneCounter;
			extern uint32_t m_iPrevBoneMask;
			extern uint32_t m_iAccumulatedBoneMask;
			extern uint32_t m_nAnimOverlayCount;
			extern uint32_t m_pAnimOverlays;
			extern uint32_t m_flPoseParameter;
			extern uint32_t m_flCycle;
			extern uint32_t m_nSequence;
			extern uint32_t m_pCachedBones;
			extern uint32_t m_nCachedBoneCount;
			extern uint32_t GetSequenceActivity;
		}

		namespace C_BasePlayer
		{
			extern uint32_t m_vecViewOffset;
			extern uint32_t m_fFlags;
			extern uint32_t m_lifeState;
			extern uint32_t m_vecVelocity;
			extern uint32_t m_hActiveWeapon;
			extern uint32_t m_pPredictionPlayer;
			extern uint32_t m_nPredictionRandomSeed;
			extern uint32_t m_nTickBase;
			extern uint32_t m_pCurrentCommand;
			extern uint32_t SimulatePlayerSimulatedEntities;
			extern uint32_t PostThinkVPhysics;
			extern uint32_t ButtonOffset;
			extern uint32_t DisabledButtonOffset;
			extern uint32_t m_flFallVelocity;
		}

		namespace C_CSPlayer
		{
			extern uint32_t m_nLastOcclusionCheckFrameCount;
			extern uint32_t m_fOcclusionFlags;
			extern uint32_t m_pAnimState;
			extern uint32_t m_angEyeAngles;
			extern uint32_t m_AnimStateHelpers;
			extern uint32_t m_flLowerBodyYawTarget;
			extern uint32_t m_bHasHelmet;
			extern uint32_t m_bHasHeavyArmor;
			extern uint32_t m_ArmorValue;
		}

		namespace CCSPlayerAnimState
		{
			extern uint32_t SetupAnimState;
		}

		namespace IMoveHelper
		{
			extern uint32_t MoveHelperPtr;
		}

		namespace Other
		{
			extern uint32_t MD5_PseudoRandom;
			extern uint32_t UTIL_ClipTraceToPlayers;

			namespace Prediction
			{
				extern uint32_t dword_4A87B1C;
			}
		}
	}
}

#endif