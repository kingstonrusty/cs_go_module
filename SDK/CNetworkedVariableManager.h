#ifndef __HEADER_CNETWORKEDVARIABLEMANAGER__
#define __HEADER_CNETWORKEDVARIABLEMANAGER__
#pragma once

#include "IBaseClientDLL.h"

#undef GetProp

namespace SDK
{
	class CNetworkedVariableManager;
}

class SDK::CNetworkedVariableManager
{
public:
	static uint32_t GetOffset(const char* pszTableName, const char* pszPropName)
	{
		return GetProp(pszTableName, pszPropName, nullptr);
	}
private:
	static uint32_t GetProp(const char* pszTableName, const char* pszPropName, CRecvProp** ppProp)
	{
		CRecvTable* pTable = GetTable(pszTableName);
		if (pTable == nullptr)
		{
			return 0x00;
		}

		return GetProp(pTable, pszPropName, ppProp);
	}

	static uint32_t GetProp(CRecvTable* pRecvTable, const char* pszPropName, CRecvProp** ppProp)
	{
		uint32_t ui32Offset = 0;

		for (int i = 0; i < pRecvTable->m_nProps; i++)
		{
			CRecvProp* pProp = &pRecvTable->m_pProps[i];
			CRecvTable* pChild = pProp->m_pDataTable;

			ui32Offset += (pChild && (pChild->m_nProps > 0) && GetProp(pChild, pszPropName, ppProp)) ? pProp->m_Offset + GetProp(pChild, pszPropName, ppProp) : 0;

			if (!StringEquals(pProp->m_pVarName, pszPropName))
			{
				continue;
			}

			if (ppProp)
			{
				*ppProp = pProp;
			}

			return pProp->m_Offset + ui32Offset;
		}

		return ui32Offset;
	}

	static CRecvTable* GetTable(const char* pszTableName)
	{
		ClientClass* pClass = SDK::Interfaces::Client->GetAllClasses();
		while (pClass)
		{
			if (pClass->m_pRecvTable)
			{
				if (StringEquals(pClass->m_pRecvTable->m_pNetTableName, pszTableName))
				{
					return pClass->m_pRecvTable;
				}
			}

			pClass = pClass->m_pNext;
		}

		return nullptr;
	}
private:
};

#endif