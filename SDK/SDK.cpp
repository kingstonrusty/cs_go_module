#include "Include.h"

#include "CNetworkedVariableManager.h"

namespace SDK
{
	namespace Offsets
	{
		namespace ISurface
		{
			uint32_t StartDrawing;
			uint32_t FinishDrawing;
		}

		namespace C_BaseEntity
		{
			uint32_t SetAbsOrigin;
			uint32_t SetAbsAngles;
			uint32_t m_flSimulationTime;
			uint32_t m_flAnimTime;
			uint32_t m_vecOrigin;
			uint32_t RemoveFromInterpolationList;
			uint32_t m_iTeamNum;
			uint32_t IsBreakable;
			uint32_t PhysicsRunThink;
			uint32_t Think_NoIdea1;
		}

		namespace C_BaseAnimating
		{
			uint32_t m_fReadableBones;
			uint32_t m_fWriteableBones;
			uint32_t m_flLastBoneSetupTime;
			uint32_t m_iMostRecentModelBoneCounter;
			uint32_t m_iPrevBoneMask;
			uint32_t m_iAccumulatedBoneMask;
			uint32_t m_nAnimOverlayCount;
			uint32_t m_pAnimOverlays;
			uint32_t m_flPoseParameter;
			uint32_t m_flCycle;
			uint32_t m_nSequence;
			uint32_t m_pCachedBones;
			uint32_t m_nCachedBoneCount;
			uint32_t GetSequenceActivity;
		}

		namespace C_BasePlayer
		{
			uint32_t m_vecViewOffset;
			uint32_t m_fFlags;
			uint32_t m_lifeState;
			uint32_t m_vecVelocity;
			uint32_t m_hActiveWeapon;
			uint32_t m_pPredictionPlayer;
			uint32_t m_nPredictionRandomSeed;
			uint32_t m_nTickBase;
			uint32_t m_pCurrentCommand;
			uint32_t SimulatePlayerSimulatedEntities;
			uint32_t PostThinkVPhysics;
			uint32_t ButtonOffset;
			uint32_t DisabledButtonOffset;
			uint32_t m_flFallVelocity;
		}

		namespace C_CSPlayer
		{
			uint32_t m_nLastOcclusionCheckFrameCount;
			uint32_t m_fOcclusionFlags;
			uint32_t m_pAnimState;
			uint32_t m_angEyeAngles;
			uint32_t m_AnimStateHelpers;
			uint32_t m_flLowerBodyYawTarget;
			uint32_t m_bHasHelmet;
			uint32_t m_bHasHeavyArmor;
			uint32_t m_ArmorValue;
		}

		namespace CCSPlayerAnimState
		{
			uint32_t SetupAnimState;
		}

		namespace IMoveHelper
		{
			uint32_t MoveHelperPtr;
		}

		namespace Other
		{
			uint32_t MD5_PseudoRandom;
			uint32_t UTIL_ClipTraceToPlayers;

			namespace Prediction
			{
				uint32_t dword_4A87B1C;
			}
		}
	}

	void Initialize();

	RandomSeed_t RandomSeed;
	RandomFloat_t RandomFloat;
	RandomInt_t RandomInt;
	Msg_t Msg;

	namespace Interfaces
	{
		CGlobalVarsBase* Globals;
		IBaseClientDLL* Client;
		IClientEntityList* EntityList;
		IClientMode* ClientMode;
		ICvar* Cvar;
		IEngineTrace* EngineTrace;
		IEngineVGui* EngineVGui;
		IGameMovement* GameMovement;
		IPhysicsSurfaceProps* PhysProps;
		IPrediction* Prediction;
		ISurface* Surface;
		IVEngineClient* Engine;
	}
}

#if defined(_DEBUG)
static void* GetInterface(const char* pszInterface, const wchar_t* pwszModule)
{
	SDK::CreateInterface_t pCreateInterface = (SDK::CreateInterface_t)GetProcAddress(GetModuleHandleW(pwszModule), XorStr("CreateInterface"));

	if (pCreateInterface == nullptr)
	{
		return nullptr;
	}

	for (int i = 100; i >= 0; i--)
	{
		char szBuf[128];
		FormatString(szBuf, 128, XorStr("%s%03d"), pszInterface, i);

		if (pCreateInterface(szBuf, nullptr))
		{
			return pCreateInterface(szBuf, nullptr);
		}
	}

	return nullptr;
}
#endif

#define CLIENT_CRC32 0x61EE3ED1
#define ENGINE_CRC32 0x220245D7
#define VGUIMATSURFACE_CRC32 0x783AFF85
#define TIER0_CRC32 0x2E3C2275
#define VSTDLIB_CRC32 0x7FC54971
#define VPHYSICS_CRC32 0x8DB91414

//============OFFSETS============//
#define OFFSET_IMPORT_RANDOMSEED 0x0000EC40
#define OFFSET_IMPORT_RANDOMFLOAT 0x0000EC60
#define OFFSET_IMPORT_RANDOMINT 0x0000ECD0
#define OFFSET_IMPORT_MSG 0x00006140
#define OFFSET_INTERFACE_GLOBALS 0x0057D548
#define OFFSET_INTERFACE_CLIENT 0x04A72810
#define OFFSET_INTERFACE_ENTITYLIST 0x04A97AFC
#define OFFSET_INTERFACE_CLIENTMODE 0x04EBFA28
#define OFFSET_INTERFACE_CVAR 0x0003C290
#define OFFSET_INTERFACE_ENGINETRACE 0x00587E44
#define OFFSET_INTERFACE_ENGINEVGUI 0x007D6578
#define OFFSET_INTERFACE_GAMEMOVEMENT 0x04EC17E0
#define OFFSET_INTERFACE_PHYSPROPS 0x00114008
#define OFFSET_INTERFACE_PREDICTION 0x04EC2A88
#define OFFSET_INTERFACE_SURFACE 0x000F37A8
#define OFFSET_INTERFACE_ENGINE 0x0057BC10
#define OFFSET_ISURFACE_STARTDRAWING 0x0000D0E0
#define OFFSET_ISURFACE_FINISHDRAWING 0x0000D2C0
#define OFFSET_BASEENTITY_SETABSORIGIN 0x001B2120
#define OFFSET_BASEENTITY_SETABSANGLES 0x001B22E0
#define OFFSET_BASEENTITY_SIMULATIONTIME 0x00000264
#define OFFSET_BASEENTITY_ANIMTIME 0x0000025C
#define OFFSET_BASEENTITY_ORIGIN 0x00000134
#define OFFSET_BASEENTITY_REMOVEFROMINTERPLIST 0x001B4BD0
#define OFFSET_BASEENTITY_TEAMNUM 0x000000F0
#define OFFSET_BASEENTITY_ISBREAKABLE 0x002C2740
#define OFFSET_BASEANIMATING_READABLEBONES 0x0000269C
#define OFFSET_BASEANIMATING_WRITEABLEBONES 0x000026A0
#define OFFSET_BASEANIMATING_LASTBONESETUP 0x00002914
#define OFFSET_BASEANIMATING_MOSTRECENTBONECOUNTER 0x00002680
#define OFFSET_BASEANIMATING_PREVBONEMASK 0x0000268C
#define OFFSET_BASEANIMATING_ACCUMULATEDBONEMASK 0x00002690
#define OFFSET_BASEANIMATING_ANIMOVERLAYCOUNT 0x0000297C
#define OFFSET_BASEANIMATING_ANIMOVERLAYS 0x00002970
#define OFFSET_BASEANIMATING_POSEPARAMETERS 0x00002764
#define OFFSET_BASEANIMATING_CYCLE 0x00000A14
#define OFFSET_BASEANIMATING_SEQUENCE 0x000028AC
#define OFFSET_BASEANIMATING_CACHEDBONES 0x00002900
#define OFFSET_BASEANIMATING_CACHEDBONECOUNT 0x0000290C
#define OFFSET_BASEANIMATING_GETSEQUENCEACTIVITY 0x001A1400
#define OFFSET_BASEPLAYER_VIEWOFFSET 0x00000104
#define OFFSET_BASEPLAYER_FLAGS 0x00000100
#define OFFSET_BASEPLAYER_LIFESTATE 0x0000025B
#define OFFSET_BASEPLAYER_VELOCITY 0x00000110
#define OFFSET_BASEPLAYER_ACTIVEWEAPON 0x00002EE8
#define OFFSET_BASEPLAYER_PREDICTIONPLAYER 0x04F76D3C
#define OFFSET_BASEPLAYER_PREDICTIONSEED 0x00A52BEC
#define OFFSET_BASEPLAYER_TICKBASE 0x00003404
#define OFFSET_BASEPLAYER_CURRENTCOMMAND 0x00003314
#define OFFSET_CSPLAYER_LASTOCCLUSIONCHECK 0x00000A30
#define OFFSET_CSPLAYER_OCCLUSIONFLAGS 0x00000A28
#define OFFSET_CSPLAYER_ANIMSTATE 0x00003870
#define OFFSET_CSPLAYER_EYEANGLES 0x0000B23C
#define OFFSET_CSPLAYER_ANIMSTATEHELPERS 0x000037FC
#define OFFSET_CSPLAYER_LOWERBODYYAWTARGET 0x000039DC
#define OFFSET_CSPLAYER_HASHELMET 0x0000B22C
#define OFFSET_CSPLAYER_HASHEAVYARMOR 0x0000B22D
#define OFFSET_CSPLAYER_ARMORVALUE 0x0000B238
#define OFFSET_CSANIMSTATE_SETUP 0x003A5BB0
#define OFFSET_MOVEHELPER_PTR 0x00AA0C50
#define OFFSET_OTHER_MD5PSEUDORANDOM 0x006DDA40
#define OFFSET_OTHER_CLIPTRACETOPLAYERS 0x00315190
//============OFFSETS============//

void SDK::Initialize()
{
#if defined(_DEBUG)
	RandomSeed = (RandomSeed_t)GetProcAddress(GetModuleHandleA(XorStr("vstdlib.dll")), XorStr("RandomSeed"));
	RandomFloat = (RandomFloat_t)GetProcAddress(GetModuleHandleA(XorStr("vstdlib.dll")), XorStr("RandomFloat"));
	RandomInt = (RandomInt_t)GetProcAddress(GetModuleHandleA(XorStr("vstdlib.dll")), XorStr("RandomInt"));
	Msg = (Msg_t)GetProcAddress(GetModuleHandleA(XorStr("tier0.dll")), XorStr("Msg"));

	SDK::Msg(XorStr("//============OFFSETS============//\n"));

	SDK::Msg(XorStr("#define OFFSET_IMPORT_RANDOMSEED 0x%p\n"), (uint32_t)RandomSeed - (uint32_t)GetModuleHandleA(XorStr("vstdlib.dll")));
	SDK::Msg(XorStr("#define OFFSET_IMPORT_RANDOMFLOAT 0x%p\n"), (uint32_t)RandomFloat - (uint32_t)GetModuleHandleA(XorStr("vstdlib.dll")));
	SDK::Msg(XorStr("#define OFFSET_IMPORT_RANDOMINT 0x%p\n"), (uint32_t)RandomInt - (uint32_t)GetModuleHandleA(XorStr("vstdlib.dll")));
	SDK::Msg(XorStr("#define OFFSET_IMPORT_MSG 0x%p\n"), (uint32_t)Msg - (uint32_t)GetModuleHandleA(XorStr("tier0.dll")));
#else
	RandomSeed = (RandomSeed_t)((uint32_t)GetModuleHandle(VSTDLIB_CRC32) + OFFSET_IMPORT_RANDOMSEED);
	RandomFloat = (RandomFloat_t)((uint32_t)GetModuleHandle(VSTDLIB_CRC32) + OFFSET_IMPORT_RANDOMFLOAT);
	RandomInt = (RandomInt_t)((uint32_t)GetModuleHandle(VSTDLIB_CRC32) + OFFSET_IMPORT_RANDOMINT);
	Msg = (Msg_t)((uint32_t)GetModuleHandle(TIER0_CRC32) + OFFSET_IMPORT_MSG);
#endif

#if defined(_DEBUG)
	Interfaces::Client = (IBaseClientDLL*)GetInterface(XorStr("VClient"), WXorStr(L"client.dll"));
	Interfaces::Globals = **(CGlobalVarsBase***)Util::Memory::FindByte(GET_VFUNC(void*, Interfaces::Client, 0), 0xA3, 0xFF, 0x01);
	Interfaces::EntityList = (IClientEntityList*)GetInterface(XorStr("VClientEntityList"), WXorStr(L"client.dll"));
	Interfaces::ClientMode = **(IClientMode***)Util::Memory::FindByte(GET_VFUNC(void*, Interfaces::Client, 10), 0x0D, 0xFF, 0x01);
	Interfaces::Cvar = (ICvar*)GetInterface(XorStr("VEngineCvar"), WXorStr(L"vstdlib.dll"));
	Interfaces::EngineTrace = (IEngineTrace*)GetInterface(XorStr("EngineTraceClient"), WXorStr(L"engine.dll"));
	Interfaces::EngineVGui = (IEngineVGui*)GetInterface(XorStr("VEngineVGui"), WXorStr(L"engine.dll"));
	Interfaces::GameMovement = (IGameMovement*)GetInterface(XorStr("GameMovement"), WXorStr(L"client.dll"));
	Interfaces::PhysProps = (IPhysicsSurfaceProps*)GetInterface(XorStr("VPhysicsSurfaceProps"), WXorStr(L"vphysics.dll"));
	Interfaces::Prediction = (IPrediction*)GetInterface(XorStr("VClientPrediction"), WXorStr(L"client.dll"));
	Interfaces::Surface = (ISurface*)GetInterface(XorStr("VGUI_Surface"), WXorStr(L"vguimatsurface.dll"));
	Interfaces::Engine = (IVEngineClient*)GetInterface(XorStr("VEngineClient"), WXorStr(L"engine.dll"));

	SDK::Msg(XorStr("#define OFFSET_INTERFACE_GLOBALS 0x%p\n"), (uint32_t)Interfaces::Globals - (uint32_t)GetModuleHandleA(XorStr("engine.dll")));
	SDK::Msg(XorStr("#define OFFSET_INTERFACE_CLIENT 0x%p\n"), (uint32_t)Interfaces::Client - (uint32_t)GetModuleHandleA(XorStr("client.dll")));
	SDK::Msg(XorStr("#define OFFSET_INTERFACE_ENTITYLIST 0x%p\n"), (uint32_t)Interfaces::EntityList - (uint32_t)GetModuleHandleA(XorStr("client.dll")));
	SDK::Msg(XorStr("#define OFFSET_INTERFACE_CLIENTMODE 0x%p\n"), (uint32_t)Interfaces::ClientMode - (uint32_t)GetModuleHandleA(XorStr("client.dll")));
	SDK::Msg(XorStr("#define OFFSET_INTERFACE_CVAR 0x%p\n"), (uint32_t)Interfaces::Cvar - (uint32_t)GetModuleHandleA(XorStr("vstdlib.dll")));
	SDK::Msg(XorStr("#define OFFSET_INTERFACE_ENGINETRACE 0x%p\n"), (uint32_t)Interfaces::EngineTrace - (uint32_t)GetModuleHandleA(XorStr("engine.dll")));
	SDK::Msg(XorStr("#define OFFSET_INTERFACE_ENGINEVGUI 0x%p\n"), (uint32_t)Interfaces::EngineVGui - (uint32_t)GetModuleHandleA(XorStr("engine.dll")));
	SDK::Msg(XorStr("#define OFFSET_INTERFACE_GAMEMOVEMENT 0x%p\n"), (uint32_t)Interfaces::GameMovement - (uint32_t)GetModuleHandleA(XorStr("client.dll")));
	SDK::Msg(XorStr("#define OFFSET_INTERFACE_PHYSPROPS 0x%p\n"), (uint32_t)Interfaces::PhysProps - (uint32_t)GetModuleHandleA(XorStr("vphysics.dll")));
	SDK::Msg(XorStr("#define OFFSET_INTERFACE_PREDICTION 0x%p\n"), (uint32_t)Interfaces::Prediction - (uint32_t)GetModuleHandleA(XorStr("client.dll")));
	SDK::Msg(XorStr("#define OFFSET_INTERFACE_SURFACE 0x%p\n"), (uint32_t)Interfaces::Surface - (uint32_t)GetModuleHandleA(XorStr("vguimatsurface.dll")));
	SDK::Msg(XorStr("#define OFFSET_INTERFACE_ENGINE 0x%p\n"), (uint32_t)Interfaces::Engine - (uint32_t)GetModuleHandleA(XorStr("engine.dll")));
#else
	Interfaces::Globals = (CGlobalVarsBase*)((uint32_t)GetModuleHandle(ENGINE_CRC32) + OFFSET_INTERFACE_GLOBALS);
	Interfaces::Client = (IBaseClientDLL*)((uint32_t)GetModuleHandle(CLIENT_CRC32) + OFFSET_INTERFACE_CLIENT);
	Interfaces::EntityList = (IClientEntityList*)((uint32_t)GetModuleHandle(CLIENT_CRC32) + OFFSET_INTERFACE_ENTITYLIST);
	Interfaces::ClientMode = (IClientMode*)((uint32_t)GetModuleHandle(CLIENT_CRC32) + OFFSET_INTERFACE_CLIENTMODE);
	Interfaces::Cvar = (ICvar*)((uint32_t)GetModuleHandle(VSTDLIB_CRC32) + OFFSET_INTERFACE_CVAR);
	Interfaces::EngineTrace = (IEngineTrace*)((uint32_t)GetModuleHandle(ENGINE_CRC32) + OFFSET_INTERFACE_ENGINETRACE);
	Interfaces::EngineVGui = (IEngineVGui*)((uint32_t)GetModuleHandle(ENGINE_CRC32) + OFFSET_INTERFACE_ENGINEVGUI);
	Interfaces::GameMovement = (IGameMovement*)((uint32_t)GetModuleHandle(CLIENT_CRC32) + OFFSET_INTERFACE_GAMEMOVEMENT);
	Interfaces::PhysProps = (IPhysicsSurfaceProps*)((uint32_t)GetModuleHandle(VPHYSICS_CRC32) + OFFSET_INTERFACE_PHYSPROPS);
	Interfaces::Prediction = (IPrediction*)((uint32_t)GetModuleHandle(CLIENT_CRC32) + OFFSET_INTERFACE_PREDICTION);
	Interfaces::Surface = (ISurface*)((uint32_t)GetModuleHandle(VGUIMATSURFACE_CRC32) + OFFSET_INTERFACE_SURFACE);
	Interfaces::Engine = (IVEngineClient*)((uint32_t)GetModuleHandle(ENGINE_CRC32) + OFFSET_INTERFACE_ENGINE);
#endif

#if defined(_DEBUG)
	Offsets::ISurface::StartDrawing = (uint32_t)Util::Memory::FindPattern(WXorStr(L"vguimatsurface.dll"), XorStr("55 8B EC 83 E4 C0 83 EC 38"));
	Offsets::ISurface::FinishDrawing = (uint32_t)Util::Memory::FindPattern(WXorStr(L"vguimatsurface.dll"), XorStr("8B 0D ?? ?? ?? ?? 56 C6 05"));

	SDK::Msg(XorStr("#define OFFSET_ISURFACE_STARTDRAWING 0x%p\n"), Offsets::ISurface::StartDrawing - (uint32_t)GetModuleHandleA(XorStr("vguimatsurface.dll")));
	SDK::Msg(XorStr("#define OFFSET_ISURFACE_FINISHDRAWING 0x%p\n"), Offsets::ISurface::FinishDrawing - (uint32_t)GetModuleHandleA(XorStr("vguimatsurface.dll")));
#else
	Offsets::ISurface::StartDrawing = (uint32_t)GetModuleHandle(VGUIMATSURFACE_CRC32) + OFFSET_ISURFACE_STARTDRAWING;
	Offsets::ISurface::FinishDrawing = (uint32_t)GetModuleHandle(VGUIMATSURFACE_CRC32) + OFFSET_ISURFACE_FINISHDRAWING;
#endif

#if defined(_DEBUG)
	Offsets::C_BaseEntity::SetAbsOrigin = (uint32_t)Util::Memory::FindPattern(WXorStr(L"client.dll"), XorStr("55 8B EC 83 E4 F8 51 53 56 57 8B F1 E8"));
	Offsets::C_BaseEntity::SetAbsAngles = (uint32_t)Util::Memory::FindPattern(WXorStr(L"client.dll"), XorStr("55 8B EC 83 E4 F8 83 EC 64 53 56 57 8B F1"));
	Offsets::C_BaseEntity::m_flSimulationTime = SDK::CNetworkedVariableManager::GetOffset(XorStr("DT_BaseEntity"), XorStr("m_flSimulationTime"));
	Offsets::C_BaseEntity::m_flAnimTime = SDK::CNetworkedVariableManager::GetOffset(XorStr("DT_BaseEntity"), XorStr("m_flAnimTime"));
	Offsets::C_BaseEntity::m_vecOrigin = SDK::CNetworkedVariableManager::GetOffset(XorStr("DT_BaseEntity"), XorStr("m_vecOrigin"));
	Offsets::C_BaseEntity::RemoveFromInterpolationList = (uint32_t)Util::Memory::FindPattern(WXorStr(L"client.dll"), XorStr("55 8B EC 8B 55 08 0F B7 84 51"));
	Offsets::C_BaseEntity::m_iTeamNum = SDK::CNetworkedVariableManager::GetOffset(XorStr("DT_BaseEntity"), XorStr("m_iTeamNum"));
	Offsets::C_BaseEntity::IsBreakable = (uint32_t)Util::Memory::FindPattern(WXorStr(L"client.dll"), XorStr("55 8B EC 51 56 8B F1 85 F6 74 68 83 BE"));
	Offsets::C_BaseEntity::PhysicsRunThink = (uint32_t)Util::Memory::FindPattern(WXorStr(L"client.dll"), XorStr("55 8B EC 83 EC 10 53 56 57 8B F9 8B 87"));
	Offsets::C_BaseEntity::Think_NoIdea1 = (uint32_t)Util::Memory::FindPattern(WXorStr(L"client.dll"), XorStr("55 8B EC 56 57 8B F9 8B B7 ?? ?? ?? ?? 8B C6"));

	SDK::Msg(XorStr("#define OFFSET_BASEENTITY_SETABSORIGIN 0x%p\n"), Offsets::C_BaseEntity::SetAbsOrigin - (uint32_t)GetModuleHandleA(XorStr("client.dll")));
	SDK::Msg(XorStr("#define OFFSET_BASEENTITY_SETABSANGLES 0x%p\n"), Offsets::C_BaseEntity::SetAbsAngles - (uint32_t)GetModuleHandleA(XorStr("client.dll")));
	SDK::Msg(XorStr("#define OFFSET_BASEENTITY_SIMULATIONTIME 0x%p\n"), Offsets::C_BaseEntity::m_flSimulationTime);
	SDK::Msg(XorStr("#define OFFSET_BASEENTITY_ANIMTIME 0x%p\n"), Offsets::C_BaseEntity::m_flAnimTime);
	SDK::Msg(XorStr("#define OFFSET_BASEENTITY_ORIGIN 0x%p\n"), Offsets::C_BaseEntity::m_vecOrigin);
	SDK::Msg(XorStr("#define OFFSET_BASEENTITY_REMOVEFROMINTERPLIST 0x%p\n"), Offsets::C_BaseEntity::RemoveFromInterpolationList - (uint32_t)GetModuleHandleA(XorStr("client.dll")));
	SDK::Msg(XorStr("#define OFFSET_BASEENTITY_TEAMNUM 0x%p\n"), Offsets::C_BaseEntity::m_iTeamNum);
	SDK::Msg(XorStr("#define OFFSET_BASEENTITY_ISBREAKABLE 0x%p\n"), Offsets::C_BaseEntity::IsBreakable - (uint32_t)GetModuleHandleA(XorStr("client.dll")));
	SDK::Msg(XorStr("#define OFFSET_BASEENTITY_PHYSICSRUNTHINK 0x%p\n"), Offsets::C_BaseEntity::PhysicsRunThink - (uint32_t)GetModuleHandleA(XorStr("client.dll")));
	SDK::Msg(XorStr("#define OFFSET_BASEENTITY_THINK_NOIDEA1 0x%p\n"), Offsets::C_BaseEntity::Think_NoIdea1 - (uint32_t)GetModuleHandleA(XorStr("client.dll")));

	Offsets::C_BaseAnimating::m_fReadableBones = *(uint32_t*)((uint32_t)Util::Memory::FindPattern(WXorStr(L"client.dll"), XorStr("C7 87 ?? ?? ?? ?? ?? ?? ?? ?? C7 87 ?? ?? ?? ?? ?? ?? ?? ?? F3 0F 11 87 ?? ?? ?? ?? 8B 87")) + 0x02) + 0x04;
	Offsets::C_BaseAnimating::m_fWriteableBones = *(uint32_t*)((uint32_t)Util::Memory::FindPattern(WXorStr(L"client.dll"), XorStr("C7 87 ?? ?? ?? ?? ?? ?? ?? ?? F3 0F 11 87 ?? ?? ?? ?? 8B 87")) + 0x02) + 0x04;
	Offsets::C_BaseAnimating::m_flLastBoneSetupTime = *(uint32_t*)((uint32_t)Util::Memory::FindPattern(WXorStr(L"client.dll"), XorStr("F3 0F 10 87 ?? ?? ?? ?? F3 0F 11 44 24 ?? 8B 80")) + 0x04) + 0x04;
	Offsets::C_BaseAnimating::m_iMostRecentModelBoneCounter = *(uint32_t*)((uint32_t)Util::Memory::FindPattern(WXorStr(L"client.dll"), XorStr("8B 87 ?? ?? ?? ?? F3 0F 11 44 24 ?? 3B 05")) + 0x02) + 0x04;
	Offsets::C_BaseAnimating::m_iPrevBoneMask = *(uint32_t*)((uint32_t)Util::Memory::FindPattern(WXorStr(L"client.dll"), XorStr("89 87 ?? ?? ?? ?? 8D 47 FC C7 87")) + 0x02) + 0x04;
	Offsets::C_BaseAnimating::m_iAccumulatedBoneMask = *(uint32_t*)((uint32_t)Util::Memory::FindPattern(WXorStr(L"client.dll"), XorStr("8B 87 ?? ?? ?? ?? 89 87 ?? ?? ?? ?? 8D 47 FC")) + 0x02) + 0x04;
	Offsets::C_BaseAnimating::m_nAnimOverlayCount = *(uint32_t*)((uint32_t)Util::Memory::FindPattern(WXorStr(L"client.dll"), XorStr("8B 81 ?? ?? ?? ?? 48 85 C0")) + 0x02);
	Offsets::C_BaseAnimating::m_pAnimOverlays = *(uint32_t*)((uint32_t)Util::Memory::FindPattern(WXorStr(L"client.dll"), XorStr("8B 80 ?? ?? ?? ?? 89 44 24 1C 83 C0 14")) + 0x02);
	Offsets::C_BaseAnimating::m_flPoseParameter = SDK::CNetworkedVariableManager::GetOffset(XorStr("DT_BaseAnimating"), XorStr("m_flPoseParameter"));
	Offsets::C_BaseAnimating::m_flCycle = SDK::CNetworkedVariableManager::GetOffset(XorStr("DT_BaseAnimating"), XorStr("m_flCycle"));
	Offsets::C_BaseAnimating::m_nSequence = SDK::CNetworkedVariableManager::GetOffset(XorStr("DT_BaseAnimating"), XorStr("m_nSequence"));
	Offsets::C_BaseAnimating::m_pCachedBones = *(uint32_t*)((uint32_t)Util::Memory::FindPattern(WXorStr(L"client.dll"), XorStr("FF B7 ?? ?? ?? ?? 52")) + 0x02) + 0x04;
	Offsets::C_BaseAnimating::m_nCachedBoneCount = *(uint32_t*)((uint32_t)Util::Memory::FindPattern(WXorStr(L"client.dll"), XorStr("8B 87 ?? ?? ?? ?? 8B 4D 0C")) + 0x02) + 0x04;
	Offsets::C_BaseAnimating::GetSequenceActivity = (uint32_t)Util::Memory::FindPattern(WXorStr(L"client.dll"), XorStr("55 8B EC 83 7D 08 FF 56 8B F1 74 3D"));

	SDK::Msg(XorStr("#define OFFSET_BASEANIMATING_READABLEBONES 0x%p\n"), Offsets::C_BaseAnimating::m_fReadableBones);
	SDK::Msg(XorStr("#define OFFSET_BASEANIMATING_WRITEABLEBONES 0x%p\n"), Offsets::C_BaseAnimating::m_fWriteableBones);
	SDK::Msg(XorStr("#define OFFSET_BASEANIMATING_LASTBONESETUP 0x%p\n"), Offsets::C_BaseAnimating::m_flLastBoneSetupTime);
	SDK::Msg(XorStr("#define OFFSET_BASEANIMATING_MOSTRECENTBONECOUNTER 0x%p\n"), Offsets::C_BaseAnimating::m_iMostRecentModelBoneCounter);
	SDK::Msg(XorStr("#define OFFSET_BASEANIMATING_PREVBONEMASK 0x%p\n"), Offsets::C_BaseAnimating::m_iPrevBoneMask);
	SDK::Msg(XorStr("#define OFFSET_BASEANIMATING_ACCUMULATEDBONEMASK 0x%p\n"), Offsets::C_BaseAnimating::m_iAccumulatedBoneMask);
	SDK::Msg(XorStr("#define OFFSET_BASEANIMATING_ANIMOVERLAYCOUNT 0x%p\n"), Offsets::C_BaseAnimating::m_nAnimOverlayCount);
	SDK::Msg(XorStr("#define OFFSET_BASEANIMATING_ANIMOVERLAYS 0x%p\n"), Offsets::C_BaseAnimating::m_pAnimOverlays);
	SDK::Msg(XorStr("#define OFFSET_BASEANIMATING_POSEPARAMETERS 0x%p\n"), Offsets::C_BaseAnimating::m_flPoseParameter);
	SDK::Msg(XorStr("#define OFFSET_BASEANIMATING_CYCLE 0x%p\n"), Offsets::C_BaseAnimating::m_flCycle);
	SDK::Msg(XorStr("#define OFFSET_BASEANIMATING_SEQUENCE 0x%p\n"), Offsets::C_BaseAnimating::m_nSequence);
	SDK::Msg(XorStr("#define OFFSET_BASEANIMATING_CACHEDBONES 0x%p\n"), Offsets::C_BaseAnimating::m_pCachedBones);
	SDK::Msg(XorStr("#define OFFSET_BASEANIMATING_CACHEDBONECOUNT 0x%p\n"), Offsets::C_BaseAnimating::m_nCachedBoneCount);
	SDK::Msg(XorStr("#define OFFSET_BASEANIMATING_GETSEQUENCEACTIVITY 0x%p\n"), Offsets::C_BaseAnimating::GetSequenceActivity - (uint32_t)GetModuleHandleA(XorStr("client.dll")));

	Offsets::C_BasePlayer::m_vecViewOffset = SDK::CNetworkedVariableManager::GetOffset(XorStr("DT_BasePlayer"), XorStr("m_vecViewOffset[0]"));
	Offsets::C_BasePlayer::m_fFlags = SDK::CNetworkedVariableManager::GetOffset(XorStr("DT_BasePlayer"), XorStr("m_fFlags"));
	Offsets::C_BasePlayer::m_lifeState = SDK::CNetworkedVariableManager::GetOffset(XorStr("DT_BasePlayer"), XorStr("m_lifeState"));
	Offsets::C_BasePlayer::m_vecVelocity = SDK::CNetworkedVariableManager::GetOffset(XorStr("DT_BasePlayer"), XorStr("m_vecVelocity[0]"));
	Offsets::C_BasePlayer::m_hActiveWeapon = SDK::CNetworkedVariableManager::GetOffset(XorStr("DT_BasePlayer"), XorStr("m_hActiveWeapon"));
	Offsets::C_BasePlayer::m_pPredictionPlayer = *(uint32_t*)((uint32_t)Util::Memory::FindPattern(WXorStr(L"client.dll"), XorStr("89 35 ?? ?? ?? ?? F3 0F 10 48")) + 0x02);
	Offsets::C_BasePlayer::m_nPredictionRandomSeed = *(uint32_t*)((uint32_t)Util::Memory::FindPattern(WXorStr(L"client.dll"), XorStr("C7 05 ?? ?? ?? ?? ?? ?? ?? ?? EB 08")) + 0x02);
	Offsets::C_BasePlayer::m_nTickBase = SDK::CNetworkedVariableManager::GetOffset(XorStr("DT_BasePlayer"), XorStr("m_nTickBase"));
	Offsets::C_BasePlayer::m_pCurrentCommand = SDK::CNetworkedVariableManager::GetOffset(XorStr("DT_BasePlayer"), XorStr("m_hConstraintEntity")) - 0x0C;
	Offsets::C_BasePlayer::SimulatePlayerSimulatedEntities = (uint32_t)Util::Memory::FindPattern(WXorStr(L"client.dll"), XorStr("56 8B F1 57 8B BE ?? ?? ?? ?? 83 EF 01 78 72"));
	Offsets::C_BasePlayer::PostThinkVPhysics = (uint32_t)Util::Memory::FindPattern(WXorStr(L"client.dll"), XorStr("55 8B EC 83 E4 F8 81 EC ?? ?? ?? ?? 53 8B D9 56 57 83 BB"));
	Offsets::C_BasePlayer::ButtonOffset = *(uint32_t*)((uint32_t)Util::Memory::FindPattern(WXorStr(L"client.dll"), XorStr("89 86 ?? ?? ?? ?? 8B C1 89 96")) + 0x02);
	Offsets::C_BasePlayer::DisabledButtonOffset = *(uint32_t*)((uint32_t)Util::Memory::FindPattern(WXorStr(L"client.dll"), XorStr("8B 86 ?? ?? ?? ?? 09 47 30")) + 0x02);
	Offsets::C_BasePlayer::m_flFallVelocity = SDK::CNetworkedVariableManager::GetOffset(XorStr("DT_BasePlayer"), XorStr("m_flFallVelocity"));

	SDK::Msg(XorStr("#define OFFSET_BASEPLAYER_VIEWOFFSET 0x%p\n"), Offsets::C_BasePlayer::m_vecViewOffset);
	SDK::Msg(XorStr("#define OFFSET_BASEPLAYER_FLAGS 0x%p\n"), Offsets::C_BasePlayer::m_fFlags);
	SDK::Msg(XorStr("#define OFFSET_BASEPLAYER_LIFESTATE 0x%p\n"), Offsets::C_BasePlayer::m_lifeState);
	SDK::Msg(XorStr("#define OFFSET_BASEPLAYER_VELOCITY 0x%p\n"), Offsets::C_BasePlayer::m_vecVelocity);
	SDK::Msg(XorStr("#define OFFSET_BASEPLAYER_ACTIVEWEAPON 0x%p\n"), Offsets::C_BasePlayer::m_hActiveWeapon);
	SDK::Msg(XorStr("#define OFFSET_BASEPLAYER_PREDICTIONPLAYER 0x%p\n"), Offsets::C_BasePlayer::m_pPredictionPlayer - (uint32_t)GetModuleHandleA(XorStr("client.dll")));
	SDK::Msg(XorStr("#define OFFSET_BASEPLAYER_PREDICTIONSEED 0x%p\n"), Offsets::C_BasePlayer::m_nPredictionRandomSeed - (uint32_t)GetModuleHandleA(XorStr("client.dll")));
	SDK::Msg(XorStr("#define OFFSET_BASEPLAYER_TICKBASE 0x%p\n"), Offsets::C_BasePlayer::m_nTickBase);
	SDK::Msg(XorStr("#define OFFSET_BASEPLAYER_CURRENTCOMMAND 0x%p\n"), Offsets::C_BasePlayer::m_pCurrentCommand);
	SDK::Msg(XorStr("#define OFFSET_BASEPLAYER_SIMULATEPLAYERSIMULATEDENTITIES 0x%p\n"), Offsets::C_BasePlayer::SimulatePlayerSimulatedEntities - (uint32_t)GetModuleHandleA(XorStr("client.dll")));
	SDK::Msg(XorStr("#define OFFSET_BASEPLAYER_POSTTHINKVPHYSICS 0x%p\n"), Offsets::C_BasePlayer::PostThinkVPhysics - (uint32_t)GetModuleHandleA(XorStr("client.dll")));
	SDK::Msg(XorStr("#define OFFSET_BASEPLAYER_BUTTONOFFSET 0x%p\n"), Offsets::C_BasePlayer::ButtonOffset);
	SDK::Msg(XorStr("#define OFFSET_BASEPLAYER_DISABLEDBUTTONOFFSET 0x%p\n"), Offsets::C_BasePlayer::DisabledButtonOffset);
	SDK::Msg(XorStr("#define OFFSET_BASEPLAYER_FALLVELOCITY 0x%p\n"), Offsets::C_BasePlayer::m_flFallVelocity);

	Offsets::C_CSPlayer::m_nLastOcclusionCheckFrameCount = *(uint32_t*)((uint32_t)Util::Memory::FindPattern(WXorStr(L"client.dll"), XorStr("8B B7 ?? ?? ?? ?? 89 75 F8 39 70 04")) + 0x02);
	Offsets::C_CSPlayer::m_fOcclusionFlags = *(uint32_t*)((uint32_t)Util::Memory::FindPattern(WXorStr(L"client.dll"), XorStr("C7 87 ?? ?? ?? ?? ?? ?? ?? ?? 85 F6 0F 84")) + 0x02);
	Offsets::C_CSPlayer::m_pAnimState = *(uint32_t*)((uint32_t)Util::Memory::FindPattern(WXorStr(L"client.dll"), XorStr("89 87 ?? ?? ?? ?? 8B CF 8B 07 8B 80 ?? ?? ?? ?? FF D0 33 F6")) + 0x02);
	Offsets::C_CSPlayer::m_angEyeAngles = SDK::CNetworkedVariableManager::GetOffset(XorStr("DT_CSPlayer"), XorStr("m_angEyeAngles[0]"));
	Offsets::C_CSPlayer::m_AnimStateHelpers = *(uint32_t*)((uint32_t)Util::Memory::FindPattern(WXorStr(L"client.dll"), XorStr("C7 87 ?? ?? ?? ?? ?? ?? ?? ?? C7 87 ?? ?? ?? ?? ?? ?? ?? ?? C7 07 ?? ?? ?? ?? C7 47")) + 0x02);
	Offsets::C_CSPlayer::m_flLowerBodyYawTarget = SDK::CNetworkedVariableManager::GetOffset(XorStr("DT_CSPlayer"), XorStr("m_flLowerBodyYawTarget"));
	Offsets::C_CSPlayer::m_bHasHelmet = SDK::CNetworkedVariableManager::GetOffset(XorStr("DT_CSPlayer"), XorStr("m_bHasHelmet"));
	Offsets::C_CSPlayer::m_bHasHeavyArmor = SDK::CNetworkedVariableManager::GetOffset(XorStr("DT_CSPlayer"), XorStr("m_bHasHeavyArmor"));
	Offsets::C_CSPlayer::m_ArmorValue = SDK::CNetworkedVariableManager::GetOffset(XorStr("DT_CSPlayer"), XorStr("m_ArmorValue"));

	SDK::Msg(XorStr("#define OFFSET_CSPLAYER_LASTOCCLUSIONCHECK 0x%p\n"), Offsets::C_CSPlayer::m_nLastOcclusionCheckFrameCount);
	SDK::Msg(XorStr("#define OFFSET_CSPLAYER_OCCLUSIONFLAGS 0x%p\n"), Offsets::C_CSPlayer::m_fOcclusionFlags);
	SDK::Msg(XorStr("#define OFFSET_CSPLAYER_ANIMSTATE 0x%p\n"), Offsets::C_CSPlayer::m_pAnimState);
	SDK::Msg(XorStr("#define OFFSET_CSPLAYER_EYEANGLES 0x%p\n"), Offsets::C_CSPlayer::m_angEyeAngles);
	SDK::Msg(XorStr("#define OFFSET_CSPLAYER_ANIMSTATEHELPERS 0x%p\n"), Offsets::C_CSPlayer::m_AnimStateHelpers);
	SDK::Msg(XorStr("#define OFFSET_CSPLAYER_LOWERBODYYAWTARGET 0x%p\n"), Offsets::C_CSPlayer::m_flLowerBodyYawTarget);
	SDK::Msg(XorStr("#define OFFSET_CSPLAYER_HASHELMET 0x%p\n"), Offsets::C_CSPlayer::m_bHasHelmet);
	SDK::Msg(XorStr("#define OFFSET_CSPLAYER_HASHEAVYARMOR 0x%p\n"), Offsets::C_CSPlayer::m_bHasHeavyArmor);
	SDK::Msg(XorStr("#define OFFSET_CSPLAYER_ARMORVALUE 0x%p\n"), Offsets::C_CSPlayer::m_ArmorValue);

	Offsets::CCSPlayerAnimState::SetupAnimState = (uint32_t)Util::Memory::FindPattern(WXorStr(L"client.dll"), XorStr("E8 ?? ?? ?? ?? C7 01 ?? ?? ?? ?? C7 41 ?? ?? ?? ?? ?? C6 81"));

	SDK::Msg(XorStr("#define OFFSET_CSANIMSTATE_SETUP 0x%p\n"), Offsets::CCSPlayerAnimState::SetupAnimState - (uint32_t)GetModuleHandleA(XorStr("client.dll")));
#else
	Offsets::C_BaseEntity::SetAbsOrigin = (uint32_t)GetModuleHandle(CLIENT_CRC32) + OFFSET_BASEENTITY_SETABSORIGIN;
	Offsets::C_BaseEntity::SetAbsAngles = (uint32_t)GetModuleHandle(CLIENT_CRC32) + OFFSET_BASEENTITY_SETABSANGLES;
	Offsets::C_BaseEntity::m_flSimulationTime = OFFSET_BASEENTITY_SIMULATIONTIME;
	Offsets::C_BaseEntity::m_flAnimTime = OFFSET_BASEENTITY_ANIMTIME;
	Offsets::C_BaseEntity::m_vecOrigin = OFFSET_BASEENTITY_ORIGIN;
	Offsets::C_BaseEntity::RemoveFromInterpolationList = (uint32_t)GetModuleHandle(CLIENT_CRC32) + OFFSET_BASEENTITY_REMOVEFROMINTERPLIST;
	Offsets::C_BaseEntity::m_iTeamNum = OFFSET_BASEENTITY_TEAMNUM;
	Offsets::C_BaseEntity::IsBreakable = (uint32_t)GetModuleHandle(CLIENT_CRC32) + OFFSET_BASEENTITY_ISBREAKABLE;

	Offsets::C_BaseAnimating::m_fReadableBones = OFFSET_BASEANIMATING_READABLEBONES;
	Offsets::C_BaseAnimating::m_fWriteableBones = OFFSET_BASEANIMATING_WRITEABLEBONES;
	Offsets::C_BaseAnimating::m_flLastBoneSetupTime = OFFSET_BASEANIMATING_LASTBONESETUP;
	Offsets::C_BaseAnimating::m_iMostRecentModelBoneCounter = OFFSET_BASEANIMATING_MOSTRECENTBONECOUNTER;
	Offsets::C_BaseAnimating::m_iPrevBoneMask = OFFSET_BASEANIMATING_PREVBONEMASK;
	Offsets::C_BaseAnimating::m_iAccumulatedBoneMask = OFFSET_BASEANIMATING_ACCUMULATEDBONEMASK;
	Offsets::C_BaseAnimating::m_nAnimOverlayCount = OFFSET_BASEANIMATING_ANIMOVERLAYCOUNT;
	Offsets::C_BaseAnimating::m_pAnimOverlays = OFFSET_BASEANIMATING_ANIMOVERLAYS;
	Offsets::C_BaseAnimating::m_flPoseParameter = OFFSET_BASEANIMATING_POSEPARAMETERS;
	Offsets::C_BaseAnimating::m_flCycle = OFFSET_BASEANIMATING_CYCLE;
	Offsets::C_BaseAnimating::m_nSequence = OFFSET_BASEANIMATING_SEQUENCE;
	Offsets::C_BaseAnimating::m_pCachedBones = OFFSET_BASEANIMATING_CACHEDBONES;
	Offsets::C_BaseAnimating::m_nCachedBoneCount = OFFSET_BASEANIMATING_CACHEDBONECOUNT;
	Offsets::C_BaseAnimating::GetSequenceActivity = (uint32_t)GetModuleHandle(CLIENT_CRC32) + OFFSET_BASEANIMATING_GETSEQUENCEACTIVITY;

	Offsets::C_BasePlayer::m_vecViewOffset = OFFSET_BASEPLAYER_VIEWOFFSET;
	Offsets::C_BasePlayer::m_fFlags = OFFSET_BASEPLAYER_FLAGS;
	Offsets::C_BasePlayer::m_lifeState = OFFSET_BASEPLAYER_LIFESTATE;
	Offsets::C_BasePlayer::m_vecVelocity = OFFSET_BASEPLAYER_VELOCITY;
	Offsets::C_BasePlayer::m_pPredictionPlayer = (uint32_t)GetModuleHandle(CLIENT_CRC32) + OFFSET_BASEPLAYER_PREDICTIONPLAYER;
	Offsets::C_BasePlayer::m_nPredictionRandomSeed = (uint32_t)GetModuleHandle(CLIENT_CRC32) + OFFSET_BASEPLAYER_PREDICTIONSEED;
	Offsets::C_BasePlayer::m_nTickBase = OFFSET_BASEPLAYER_TICKBASE;
	Offsets::C_BasePlayer::m_pCurrentCommand = OFFSET_BASEPLAYER_CURRENTCOMMAND;

	Offsets::C_CSPlayer::m_nLastOcclusionCheckFrameCount = OFFSET_CSPLAYER_LASTOCCLUSIONCHECK;
	Offsets::C_CSPlayer::m_fOcclusionFlags = OFFSET_CSPLAYER_OCCLUSIONFLAGS;
	Offsets::C_CSPlayer::m_pAnimState = OFFSET_CSPLAYER_ANIMSTATE;
	Offsets::C_CSPlayer::m_angEyeAngles = OFFSET_CSPLAYER_EYEANGLES;
	Offsets::C_CSPlayer::m_AnimStateHelpers = OFFSET_CSPLAYER_ANIMSTATEHELPERS;
	Offsets::C_CSPlayer::m_flLowerBodyYawTarget = OFFSET_CSPLAYER_LOWERBODYYAWTARGET;
	Offsets::C_CSPlayer::m_bHasHelmet = OFFSET_CSPLAYER_HASHELMET;
	Offsets::C_CSPlayer::m_bHasHeavyArmor = OFFSET_CSPLAYER_HASHEAVYARMOR;
	Offsets::C_CSPlayer::m_ArmorValue = OFFSET_CSPLAYER_ARMORVALUE;

	Offsets::CCSPlayerAnimState::SetupAnimState = (uint32_t)GetModuleHandle(CLIENT_CRC32) + OFFSET_CSANIMSTATE_SETUP;
#endif

#if defined(_DEBUG)
	Offsets::IMoveHelper::MoveHelperPtr = **(uint32_t**)((uint32_t)Util::Memory::FindPattern(WXorStr(L"client.dll"), XorStr("8B 0D ?? ?? ?? ?? 8B 46 08 68")) + 0x2);

	SDK::Msg("#define OFFSET_MOVEHELPER_PTR 0x%p\n", Offsets::IMoveHelper::MoveHelperPtr - (uint32_t)GetModuleHandleA(XorStr("client.dll")));
#else
	Offsets::IMoveHelper::MoveHelperPtr = (uint32_t)GetModuleHandle(CLIENT_CRC32) + OFFSET_MOVEHELPER_PTR;
#endif

#if defined(_DEBUG)
	Offsets::Other::MD5_PseudoRandom = (uint32_t)Util::Memory::FindPattern(WXorStr(L"client.dll"), XorStr("55 8B EC 83 E4 F8 83 EC 70 6A 58"));
	Offsets::Other::UTIL_ClipTraceToPlayers = (uint32_t)Util::Memory::FindPattern(WXorStr(L"client.dll"), XorStr("53 8B DC 83 EC 08 83 E4 F0 83 C4 04 55 8B 6B 04 89 6C 24 04 8B EC 81 EC ?? ?? ?? ?? 8B 43 10"));

	SDK::Msg("#define OFFSET_OTHER_MD5PSEUDORANDOM 0x%p\n", Offsets::Other::MD5_PseudoRandom - (uint32_t)GetModuleHandleA(XorStr("client.dll")));
	SDK::Msg("#define OFFSET_OTHER_CLIPTRACETOPLAYERS 0x%p\n", Offsets::Other::UTIL_ClipTraceToPlayers - (uint32_t)GetModuleHandleA(XorStr("client.dll")));

	Offsets::Other::Prediction::dword_4A87B1C = (uint32_t)((*(uint32_t*)((uint32_t)Util::Memory::FindPattern(WXorStr(L"client.dll"), XorStr("8B 88 ?? ?? ?? ?? 85 C9 74 33")) + 0x02)) + 0x10000);

	SDK::Msg("#define OFFSET_OTHER_PREDICTION_DW4A87B1C 0x%p\n", Offsets::Other::Prediction::dword_4A87B1C - (uint32_t)GetModuleHandleA(XorStr("client.dll")));
#else
	Offsets::Other::MD5_PseudoRandom = (uint32_t)GetModuleHandle(CLIENT_CRC32) + OFFSET_OTHER_MD5PSEUDORANDOM;
	Offsets::Other::UTIL_ClipTraceToPlayers = (uint32_t)GetModuleHandle(CLIENT_CRC32) + OFFSET_OTHER_CLIPTRACETOPLAYERS;
#endif

#if defined(_DEBUG)
	SDK::Msg(XorStr("//============OFFSETS============//\n"));
#endif
}