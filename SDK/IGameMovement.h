#ifndef __HEADER_IGAMEMOVEMENT__
#define __HEADER_IGAMEMOVEMENT__
#pragma once

namespace SDK
{
	class CMoveData;

	class IGameMovement;
}

class SDK::IGameMovement
{
public:
	CMoveData* GetMoveData()
	{
		return *(CMoveData**)((uint32_t)this + 0x08);
	}

	void ProcessMovement(C_BasePlayer* player, CMoveData* movedata)
	{
		return GET_VFUNC(void(__thiscall*)(IGameMovement*, C_BasePlayer*, CMoveData*), this, 1)(this, player, movedata);
	}
};

#endif