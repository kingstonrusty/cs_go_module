#ifndef __HEADER_CNETCHAN__
#define __HEADER_CNETCHAN__
#pragma once

namespace SDK
{
	enum
	{
		FLOW_OUTGOING,
		FLOW_INCOMING,
		MAX_FLOWS
	};

	class CSVC_TempEntities;
	class NET_Tick;

	class CNetChan;
}

class SDK::CSVC_TempEntities
{
public:
	bool Process(void* msg)
	{
		return GET_VFUNC(bool(__thiscall*)(CSVC_TempEntities*, void*), this, 4)(this, msg);
	}
};

class SDK::CNetChan
{
public:
	float GetLatency(int flow)
	{
		return GET_VFUNC(float(__thiscall*)(CNetChan*, int), this, 9)(this, flow);
	}
};

#endif