#ifndef __HEADER_C_WEAPONCSBASE__
#define __HEADER_C_WEAPONCSBASE__
#pragma once

#include "C_BaseCombatWeapon.h"

namespace SDK
{
	class CCSWeaponInfo;

	class C_WeaponCSBase;
}

class SDK::CCSWeaponInfo
{
public:
	int GetDamage()
	{
		return *(int*)((uint32_t)this + 0xEC);
	}

	float GetArmorRatio()
	{
		return *(float*)((uint32_t)this + 0xF0);
	}

	float GetPenetration()
	{
		return *(float*)((uint32_t)this + 0xF8);
	}

	float GetRange()
	{
		return *(float*)((uint32_t)this + 0x104);
	}

	float GetRangeMod()
	{
		return *(float*)((uint32_t)this + 0x108);
	}
};

class SDK::C_WeaponCSBase : public C_BaseCombatWeapon
{
public:
	CCSWeaponInfo* GetCSWpnData()
	{
		return GET_VFUNC(CCSWeaponInfo*(__thiscall*)(C_WeaponCSBase*), this, 446)(this);
	}
};

#endif