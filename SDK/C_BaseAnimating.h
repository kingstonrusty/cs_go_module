#ifndef __HEADER_CBASEANIMATING__
#define __HEADER_CBASEANIMATING__
#pragma once

#include "C_BaseEntity.h"

namespace SDK
{
	class CAnimationLayer;

	class C_BaseAnimating;
}

class SDK::CAnimationLayer
{
public:
	int& m_nSequence()
	{
		return *(int*)((uint32_t)this + 0x18);
	}

	float& m_flWeight()
	{
		return *(float*)((uint32_t)this + 0x20);
	}

	float& m_flCycle()
	{
		return *(float*)((uint32_t)this + 0x2C);
	}
private:
	uint8_t __pad[0x38];
};

class SDK::C_BaseAnimating : public C_BaseEntity
{
public:
	int GetMostRecentBoneCounter()
	{
		return *(int*)((uint32_t)this + Offsets::C_BaseAnimating::m_iMostRecentModelBoneCounter);
	}

	void SetMostRecentBoneCounter(int counter)
	{
		*(int*)((uint32_t)this + Offsets::C_BaseAnimating::m_iMostRecentModelBoneCounter) = counter;
	}

	int GetReadableBones()
	{
		return *(int*)((uint32_t)this + Offsets::C_BaseAnimating::m_fReadableBones);
	}

	void SetReadableBones(int readablebones)
	{
		*(int*)((uint32_t)this + Offsets::C_BaseAnimating::m_fReadableBones) = readablebones;
	}

	int GetWriteableBones()
	{
		return *(int*)((uint32_t)this + Offsets::C_BaseAnimating::m_fWriteableBones);
	}

	void SetWriteableBones(int writeablebones)
	{
		*(int*)((uint32_t)this + Offsets::C_BaseAnimating::m_fWriteableBones) = writeablebones;
	}

	int GetPreviousBoneMask()
	{
		return *(int*)((uint32_t)this + Offsets::C_BaseAnimating::m_iPrevBoneMask);
	}

	void SetPreviousBoneMask(int prevmask)
	{
		*(int*)((uint32_t)this + Offsets::C_BaseAnimating::m_iPrevBoneMask) = prevmask;
	}

	int GetAccumulatedBoneMask()
	{
		return *(int*)((uint32_t)this + Offsets::C_BaseAnimating::m_iAccumulatedBoneMask);
	}

	void SetAccumulatedBoneMask(int accumulatedmask)
	{
		*(int*)((uint32_t)this + Offsets::C_BaseAnimating::m_iAccumulatedBoneMask) = accumulatedmask;
	}

	float GetLastBoneSetupTime()
	{
		return *(float*)((uint32_t)this + Offsets::C_BaseAnimating::m_flLastBoneSetupTime);
	}

	void SetLastBoneSetupTime(float time)
	{
		*(float*)((uint32_t)this + Offsets::C_BaseAnimating::m_flLastBoneSetupTime) = time;
	}

	void InvalidateBoneCache()
	{
		SetReadableBones(0);
		SetWriteableBones(0);
		SetPreviousBoneMask(GetAccumulatedBoneMask());
		SetAccumulatedBoneMask(0);
		SetLastBoneSetupTime(-3.402823466e+38F/*-FLT_MAX*/);
	}

	int GetAnimOverlayCount()
	{
		return *(int*)((uint32_t)this + Offsets::C_BaseAnimating::m_nAnimOverlayCount);
	}

	CAnimationLayer* GetAnimOverlay(int i)
	{
		if ((GetAnimOverlayCount() - 1) >= 0 && i < (GetAnimOverlayCount() - 1))
		{
			return (CAnimationLayer*)(*(uint32_t*)((uint32_t)this + Offsets::C_BaseAnimating::m_pAnimOverlays) + sizeof(CAnimationLayer) * i);
		}

		return nullptr;
	}

	float GetPoseParameter(int i)
	{
		return ((float*)((uint32_t)this + Offsets::C_BaseAnimating::m_flPoseParameter))[i];
	}

	void SetPoseParameter(int i, float fl)
	{
		((float*)((uint32_t)this + Offsets::C_BaseAnimating::m_flPoseParameter))[i] = fl;
	}

	float GetCycle()
	{
		return *(float*)((uint32_t)this + Offsets::C_BaseAnimating::m_flCycle);
	}

	void SetCycle(float cycle)
	{
		*(float*)((uint32_t)this + Offsets::C_BaseAnimating::m_flCycle) = cycle;
	}

	int GetSequence()
	{
		return *(int*)((uint32_t)this + Offsets::C_BaseAnimating::m_nSequence);
	}

	void SetSequence(int sequence)
	{
		*(int*)((uint32_t)this + Offsets::C_BaseAnimating::m_nSequence) = sequence;
	}

	matrix3x4* GetCachedBone(int idx)
	{
		uint32_t v9 = *(uint32_t*)((uint32_t)this + Offsets::C_BaseAnimating::m_pCachedBones/*10496*/);
		return (matrix3x4*)((uint32_t)v9 + idx * sizeof(SDK::matrix3x4));
	}

	int GetCachedBoneCount()
	{
		return *(int*)((uint32_t)this + Offsets::C_BaseAnimating::m_nCachedBoneCount);
	}

	int GetSequenceActivity(int sequence)
	{
		return ((int(__thiscall*)(C_BaseAnimating*, int))Offsets::C_BaseAnimating::GetSequenceActivity)(this, sequence);
	}
};

#endif