#ifndef __HEADER_ICLIENTNETWORKABLE__
#define __HEADER_ICLIENTNETWORKABLE__
#pragma once

namespace SDK
{
	class IClientNetworkable;
}

class SDK::IClientNetworkable
{
public:
	bool IsDormant()
	{
		return GET_VFUNC(bool(__thiscall*)(IClientNetworkable*), this, 9)(this);
	}

	int EntIndex()
	{
		return GET_VFUNC(int(__thiscall*)(IClientNetworkable*), this, 10)(this);
	}
};

#endif