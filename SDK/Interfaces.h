#ifndef __HEADER_INTERFACES__
#define __HEADER_INTERFACES__
#pragma once

#include "CGlobalVarsBase.h"
#include "IBaseClientDLL.h"
#include "IClientEntityList.h"
#include "IClientMode.h"
#include "ICvar.h"
#include "IEngineTrace.h"
#include "IEngineVGui.h"
#include "IGameMovement.h"
#include "IInput.h"
#include "IPhysicsSurfaceProps.h"
#include "IPrediction.h"
#include "ISurface.h"
#include "IVEngineClient.h"

namespace SDK
{
	namespace Interfaces
	{
		extern CGlobalVarsBase* Globals;
		extern IBaseClientDLL* Client;
		extern IClientEntityList* EntityList;
		extern IClientMode* ClientMode;
		extern ICvar* Cvar;
		extern IEngineTrace* EngineTrace;
		extern IEngineVGui* EngineVGui;
		extern IGameMovement* GameMovement;
		extern IPhysicsSurfaceProps* PhysProps;
		extern IPrediction* Prediction;
		extern ISurface* Surface;
		extern IVEngineClient* Engine;
	}
}

#endif