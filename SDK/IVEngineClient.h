#ifndef __HEADER_IVENGINECLIENT__
#define __HEADER_IVENGINECLIENT__
#pragma once

#include "CNetChan.h"

namespace SDK
{
	struct player_info_s;

	class IVEngineClient;
}

struct SDK::player_info_s
{
	unsigned long long			unknown;
	unsigned long long			xuid;
	char						name[128];
	int							userID;
	char						guid[32 + 1];
	unsigned int				friendsID;
	char						friendsName[128];
	bool						fakeplayer;
	bool						ishltv;
	unsigned int				customFiles[4];
	unsigned char				filesDownloaded;
};

class SDK::IVEngineClient
{
public:
	bool GetPlayerInfo(int index, player_info_s* pInfo)
	{
		return GET_VFUNC(bool(__thiscall*)(IVEngineClient*, int, player_info_s*), this, 8)(this, index, pInfo);
	}

	int GetLocalPlayer()
	{
		return GET_VFUNC(int(__thiscall*)(IVEngineClient*), this, 12)(this);
	}

	bool IsInGame()
	{
		return GET_VFUNC(bool(__thiscall*)(IVEngineClient*), this, 26)(this);
	}

	CNetChan* GetNetChannelInfo()
	{
		return GET_VFUNC(CNetChan*(__thiscall*)(IVEngineClient*), this, 78)(this);
	}

	int GetServerTick()
	{
		return GET_VFUNC(int(__thiscall*)(IVEngineClient*), this, 208)(this);
	}
};

#endif