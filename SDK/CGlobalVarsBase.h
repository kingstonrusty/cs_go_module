#ifndef __HEADER_CGLOBALVARSBASE__
#define __HEADER_CGLOBALVARSBASE__
#pragma once

namespace SDK
{
	class CGlobalVarsBase;
}

class SDK::CGlobalVarsBase
{
public:
	float& absoluteframetime()
	{
		return *(float*)((uint32_t)this + 0x08);
	}

	float& curtime()
	{
		return *(float*)((uint32_t)this + 0x10);
	}

	float& frametime()
	{
		return *(float*)((uint32_t)this + 0x14);
	}

	int& maxClients()
	{
		return *(int*)((uint32_t)this + 0x18);
	}

	int& tickcount()
	{
		return *(int*)((uint32_t)this + 0x1C);
	}

	float& interval_per_tick()
	{
		return *(float*)((uint32_t)this + 0x20);
	}
};

#endif