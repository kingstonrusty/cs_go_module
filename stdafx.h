#ifndef __HEADER_STDAFX__
#define __HEADER_STDAFX__
#pragma once

#if defined(_DEBUG)
#define _CRT_SECURE_NO_WARNINGS

#include <Windows.h>
#include <stdint.h>
#include <stdio.h>
#include <math.h>
#else
#include "wincrt/stdint.h"
#include "wincrt/string.h"
#include "wincrt/va.h"
#include "wincrt/windef.h"
#include "wincrt/winnt.h"
#include "wincrt/internal.h"
#include "wincrt/import_handler.h"
#include "wincrt/ntapi.h"
#include "wincrt/kernel32.h"
#include "wincrt/math.h"
#endif

#include "Util/CRC32.h"
#include "Util/XorStr.h"
#include "Util/Memory.h"

#define NAME XorStr("hvh cheat")
#define NAMEW WXorStr(L"hvh cheat")

#include "Wrappers/Memory.h"
#include "Wrappers/String.h"
#include "Wrappers/Math.h"

#include "SDK/Include.h"

#include "Util/Input.h"

#include "Menu/Vars.h"

#endif